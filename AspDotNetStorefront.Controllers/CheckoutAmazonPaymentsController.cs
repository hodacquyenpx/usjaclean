// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Classes;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefront.Routing;
using AspDotNetStorefrontCore;
using AspDotNetStorefrontGateways.Processors;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutAmazonPaymentsController : Controller
	{
		readonly AmazonPaymentsApiProvider AmazonPaymentsApiProvider;
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;
		readonly AddressSelectListBuilder SelectListBuilder;

		public CheckoutAmazonPaymentsController(
			AmazonPaymentsApiProvider amazonPaymentsApiProvider,
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider,
			AddressSelectListBuilder selectListBuilder)
		{
			AmazonPaymentsApiProvider = amazonPaymentsApiProvider;
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
			SelectListBuilder = selectListBuilder;
		}

		[PageTypeFilter(PageTypes.Checkout)]
		public ActionResult AmazonPayments(bool clearSession = false)
		{
			var customer = HttpContext.GetCustomer();

			if(!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMAmazonPayments, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var model = new AmazonPaymentsViewModel(
				residenceTypeOptions: SelectListBuilder.BuildResidenceTypeSelectList(ResidenceTypes.Residential.ToString()),
				clientId: AmazonPaymentsApiProvider.Configuration.ClientId,
				merchantId: AmazonPaymentsApiProvider.Configuration.MerchantId,
				scriptUrl: AmazonPaymentsApiProvider.Configuration.ScriptUrl);

			if(clearSession)
			{
				var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);
				var updatedCheckoutContext = new PersistedCheckoutContext(
					creditCard: checkoutContext.CreditCard,
					payPalExpress: checkoutContext.PayPalExpress,
					purchaseOrder: checkoutContext.PurchaseOrder,
                    cCWA: checkoutContext.CCWA,
                    synchrony: checkoutContext.Synchrony,
                    cash: checkoutContext.Cash,
                    cOD: checkoutContext.COD,
                    checkByMail: checkoutContext.CheckByMail,
                    customPayment: checkoutContext.CustomPayment,
                    braintree: checkoutContext.Braintree,
					amazonPayments: null,
					termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
					over13Checked: checkoutContext.Over13Checked,
					shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
					offsiteRequiresBillingAddressId: null,
					offsiteRequiresShippingAddressId: null,
					email: checkoutContext.Email,
					selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                    isHoldOrder: checkoutContext.IsHoldOrder);

				PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
				customer.UpdateCustomer(requestedPaymentMethod: string.Empty);
				return Redirect(Url.Action(ActionNames.Index, ControllerNames.Checkout));
			}

			return View(model);
		}

		public ActionResult AmazonPaymentsDetail()
		{
			return PartialView(ViewNames.AmazonPaymentsDetailPartial);
		}

		public ActionResult AmazonPaymentsCallback(string session, string access_token, string token_type, string expires_in, string scope)
		{
			// Get an email back from amazon and update the checkout context with it if we don't already have an email on the checkout context.
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);
			if(string.IsNullOrEmpty(checkoutContext.Email))
			{
				if(string.IsNullOrEmpty(access_token))
					return View(ViewNames.AmazonPayments, new { clearSession = true });

				var userProfile = AmazonPaymentsApiProvider.GetUserProfile(access_token);
				if(userProfile != null && !string.IsNullOrEmpty(userProfile.Email))
				{
					var updatedCheckoutContext = new PersistedCheckoutContext(
						creditCard: checkoutContext.CreditCard,
						payPalExpress: checkoutContext.PayPalExpress,
						purchaseOrder: checkoutContext.PurchaseOrder,
                        cCWA: checkoutContext.CCWA,
                        synchrony: checkoutContext.Synchrony,
                        cash: checkoutContext.Cash,
                        cOD: checkoutContext.COD,
                        checkByMail: checkoutContext.CheckByMail,
                        customPayment: checkoutContext.CustomPayment,
                        braintree: checkoutContext.Braintree,
						amazonPayments: checkoutContext.AmazonPayments,
						termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
						over13Checked: checkoutContext.Over13Checked,
						shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
						offsiteRequiresBillingAddressId: checkoutContext.OffsiteRequiresBillingAddressId,
						offsiteRequiresShippingAddressId: checkoutContext.OffsiteRequiresShippingAddressId,
						email: userProfile.Email,
						selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                        isHoldOrder: checkoutContext.IsHoldOrder);

					PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
				}
			}

			var residenceType = ResidenceTypes.Residential;
			if(customer.PrimaryShippingAddress != null
				&& customer.PrimaryShippingAddress.ResidenceType != ResidenceTypes.Unknown)
				residenceType = customer.PrimaryShippingAddress.ResidenceType;

			var model = new AmazonPaymentsViewModel(
				residenceTypeOptions: SelectListBuilder.BuildResidenceTypeSelectList(residenceType.ToString()),
				clientId: AmazonPaymentsApiProvider.Configuration.ClientId,
				merchantId: AmazonPaymentsApiProvider.Configuration.MerchantId,
				scriptUrl: AmazonPaymentsApiProvider.Configuration.ScriptUrl)
			{
				ResidenceType = residenceType,
				CheckoutStep = AmazonPaymentsCheckoutStep.SelectAddress
			};

			return View(ViewNames.AmazonPayments, model);
		}

		public ActionResult AmazonPaymentsComplete(AmazonPaymentsViewModel model)
		{
			var customer = HttpContext.GetCustomer();

			var orderDetails = AmazonPaymentsApiProvider
				.GetOrderDetails(model.AmazonOrderReferenceId)
				.GetOrderReferenceDetailsResult
				.OrderReferenceDetails;

			var shippingAddress = orderDetails
				.Destination
				.PhysicalDestination;

			var city = shippingAddress.City;
			var countryCode = shippingAddress.CountryCode;
			var countryName = AppLogic.GetCountryNameFromTwoLetterISOCode(countryCode);
			var stateName = shippingAddress.StateOrRegion ?? string.Empty;
			var stateAbbreviation = AppLogic.GetStateAbbreviation(stateName, countryName);
			var postalCode = shippingAddress.PostalCode;

			if (!ModelState.IsValid)
			{
				var newModel = new AmazonPaymentsViewModel(
					residenceTypeOptions: SelectListBuilder.BuildResidenceTypeSelectList(ResidenceTypes.Residential.ToString()),
					clientId: model.ClientId,
					merchantId: model.MerchantId,
					scriptUrl: model.ScriptUrl);

				return View(ViewNames.AmazonPayments, newModel);
			}

			var amazonAddress = Address.FindOrCreateOffSiteAddress(
				customerId: customer.CustomerID,
				city: city,
				stateAbbreviation: string.IsNullOrEmpty(stateAbbreviation)
					? stateName
					: stateAbbreviation,
				postalCode: postalCode,
				countryName: string.IsNullOrEmpty(countryName)
					? countryCode
					: countryName,
				offSiteSource: AppLogic.ro_PMAmazonPayments,
				residenceType: model.ResidenceType
				);

			customer.SetPrimaryAddress(amazonAddress.AddressID, AddressTypes.Billing);
			customer.SetPrimaryAddress(amazonAddress.AddressID, AddressTypes.Shipping);

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);
			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: checkoutContext.CCWA,
                synchrony: checkoutContext.Synchrony,
                cash: checkoutContext.Cash,
                cOD: checkoutContext.COD,
                checkByMail: checkoutContext.CheckByMail,
                customPayment: checkoutContext.CustomPayment,
                braintree: checkoutContext.Braintree,
				amazonPayments: new AmazonPaymentsDetails(model.AmazonOrderReferenceId),
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: amazonAddress.AddressID,
				offsiteRequiresShippingAddressId: amazonAddress.AddressID,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMAmazonPayments);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}
	}
}
