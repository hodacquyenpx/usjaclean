﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AspDotNetStorefrontControls.Listing.MultiFieldStringBranchFilter" Codebehind="MultiFieldStringBranchFilter.ascx.cs" %>

<div class="form-group">
	<asp:Label runat="server" ID="ValueLabel" AssociatedControlID="Value" />
	<asp:TextBox runat="server" ID="Value" custom-filter-name-selected="BranchFilter" CssClass="form-control" style="visibility: hidden;height:0px;margin:0px;padding:0px" />
</div>
<asp:checkboxlist id="chkBranchList" custom-filter-name="BranchFilter" runat="server" ></asp:CheckBoxList>
    <script>
        /* Set checked value to filter*/
        var postData = new Array();
        var combined_data;
        
        $("[custom-filter-name*=BranchFilter]").click(function () {
            postData = [];
            var isAll = false;

            
            $("[id*=chkBranchList] input[type=checkbox]").each(function () {
                if (this.checked) {
                    if ($(this).val() == "All") {
                        isAll = true;
                    } else {
                        postData.push($(this).val());
                    }
                }            
            });
            
            if (isAll) {
                combined_data = "All";
            } else{
                combined_data = postData.join(",");
            }
            
            if (combined_data != null) {
                $("[custom-filter-name-selected*=BranchFilter]").val(combined_data);
            }
        });
    </script>
