// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Classes
{
    public class SynchronyTermSelectListBuilder
    {
        const string SelectListValueField = "Value";
        const string SelectListDataField = "Text";

        public SelectList BuildSynchronyTermNameSelectList()
        {
            return new SelectList(
                items: GetSynchronyTermName()
                    .Select(SynchronyTermName => new SelectListItem
                    {
                        Text = SynchronyTermName,
                        Value = SynchronyTermName
        }),
                dataValueField: SelectListValueField,
                dataTextField: SelectListDataField);
        }

        IEnumerable<string> GetSynchronyTermName()
        {
            var query = @"
				select SynchronyTermName
				from SynchronyTerm with (nolock)
                order by AdminOrder";

            using (var connection = DB.dbConn())
            {
                connection.Open();
                using (var reader = DB.GetRS(query, connection))
                    while (reader.Read())
                        yield return DB.RSField(reader, "SynchronyTermName");
            }
        }
    }
}
