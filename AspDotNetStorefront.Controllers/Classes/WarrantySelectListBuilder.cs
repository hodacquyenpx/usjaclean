// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Classes
{
    public class WarrantySelectListBuilder
    {
        const string SelectListValueField = "Value";
        const string SelectListDataField = "Text";

        public SelectList BuildWarrantySelectList()
        {
            return new SelectList(
                items: GetWarrantyName()
                    .Select(WarrantyName => new SelectListItem
                    {
                        Text = WarrantyName,
                        Value = WarrantyName
                    }),
                dataValueField: SelectListValueField,
                dataTextField: SelectListDataField);
        }

        IEnumerable<string> GetWarrantyName()
        {
            var query = @"
				select WarrantyName
				from Warranty with (nolock)
				where Published = 1
                and Deleted = 0
				order by ShowOrder, WarrantyName";

            using (var connection = DB.dbConn())
            {
                connection.Open();
                using (var reader = DB.GetRS(query, connection))
                    while (reader.Read())
                        yield return DB.RSField(reader, "WarrantyName");
            }
        }
    }
}
