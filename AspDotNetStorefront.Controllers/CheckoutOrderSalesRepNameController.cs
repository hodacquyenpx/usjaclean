// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Caching.ObjectCaching;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using AspDotNetStorefront.Filters;
//using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Classes;
//using AspDotNetStorefront.Controllers.Classes;

namespace AspDotNetStorefront.Controllers
{
    [SecureAccessFilter(forceHttps: true)]
    public class CheckoutOrderSalesRepNameController : Controller
    {
        readonly ICachedShoppingCartProvider CachedShoppingCartProvider;

        public CheckoutOrderSalesRepNameController(
            ICachedShoppingCartProvider cachedShoppingCartProvider
         )
        {
            CachedShoppingCartProvider = cachedShoppingCartProvider;
        }


        [ChildActionOnly]
        public ActionResult OrderSalesRepName()
        {
            var customer = HttpContext.GetCustomer();
            var cart = CachedShoppingCartProvider.Get(customer, CartTypeEnum.ShoppingCart, AppLogic.StoreID());
            var SelectListBuilder = new SalesRepNameSelectListBuilder();

            SelectList salesrepnames = SelectListBuilder.BuildSalesRepNameSelectList();

            var model = new OrderSalesRepNameViewModel();
            model.ListOrderSalesRepNames = new SelectList(salesrepnames,"Value","Text", cart.OrderSalesRepName);
            /*var model = new OrderSalesRepNameViewModel
            (
                listOrderSalesRepName: salesrepnames
            );*/

            return PartialView(ViewNames.OrderSalesRepNamePartial, model);
        }

        [HttpPost]
        //public ActionResult OrderSalesRepName(string SelectedOrderSalesRepName)
        public ActionResult OrderSalesRepName(OrderSalesRepNameViewModel model)
        // public ActionResult PostOrderSalesRepName()
        {

            var customer = HttpContext.GetCustomer();

            if (model.SelectedOrderSalesRepName == null)
                model.SelectedOrderSalesRepName = string.Empty;

            customer.UpdateCustomer(orderSalesRepName: model.SelectedOrderSalesRepName.Trim());

            return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
        }
    }
}
