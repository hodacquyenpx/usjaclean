// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Caching.ObjectCaching;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Classes;
using System.Data.SqlClient;

namespace AspDotNetStorefront.Controllers
{
    [SecureAccessFilter(forceHttps: true)]
    public class CheckoutOrderRecalcTaxController : Controller
    {
        readonly ICachedShoppingCartProvider CachedShoppingCartProvider;

        public CheckoutOrderRecalcTaxController(
            ICachedShoppingCartProvider cachedShoppingCartProvider
         )
        {
            CachedShoppingCartProvider = cachedShoppingCartProvider;
        }


        [ChildActionOnly]
        public ActionResult OrderIgnoreTax()
        {
            var customer = HttpContext.GetCustomer();
            var cart = CachedShoppingCartProvider.Get(customer, CartTypeEnum.ShoppingCart, AppLogic.StoreID());

            var model = new OrderRecalcTaxViewModel();
            model.IsIgnoreTax = customer.IsNoTax;
            return PartialView(ViewNames.OrderRecalcTaxPartial, model);
        }

        [HttpPost]
        public ActionResult OrderIgnoreTax(OrderRecalcTaxViewModel model)
        {
            var customer = HttpContext.GetCustomer();

            if (model.IsIgnoreTax == true) {                             
                customer.UpdateCustomer(appliedTaxRate: 0, submittedAppliedTaxRate: true, isNoTax: true, submittedIsNoTax: true);
            }
            else
            {
                customer.UpdateCustomer(appliedTaxRate: customer.ActualTaxRate, submittedAppliedTaxRate: true, isNoTax: false, submittedIsNoTax: true);           
            }
            return RedirectToAction(ActionNames.OrderSummary, ControllerNames.CheckoutOrderSummary);
        }
    }
}
