// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using AspDotNetStorefrontControls;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefrontAdmin
{
	public partial class Warranty : AspDotNetStorefront.Admin.AdminPageBase
	{
		public const string PublishWarrantyCommand = "warranty:publish";
		public const string UnpublishWarrantyCommand = "warranty:unpublish";
		public const string DeleteWarrantyCommand = "warranty:delete";

		protected void DispatchCommand(object sender, GridViewCommandEventArgs e)
		{
			if(e.CommandName == PublishWarrantyCommand)
				PublishWarrantyCommandHandler(e, true);
			else if(e.CommandName == UnpublishWarrantyCommand)
                PublishWarrantyCommandHandler(e, false);
			else if(e.CommandName == DeleteWarrantyCommand)
                DeleteWarrantyCommandHandler(e);
		}

		void PublishWarrantyCommandHandler(CommandEventArgs e, bool published)
		{
			int eventId;
			if(!Int32.TryParse((string)e.CommandArgument, out eventId))
				return;

			if(SetWarrantyFlags(eventId, published: published))
				ctrlAlertMessage.PushAlertMessage(
					String.Format(
						"Warranty {0}",
						published
							? "published"
							: "unpublished"),
					AlertMessage.AlertType.Success);
		}

		void DeleteWarrantyCommandHandler(CommandEventArgs e)
		{
			int warrantyId;
			if(!Int32.TryParse((string)e.CommandArgument, out warrantyId))
				return;

			if(SetWarrantyFlags(warrantyId, deleted: true))
				ctrlAlertMessage.PushAlertMessage("Warranty deleted", AlertMessage.AlertType.Success);
		}

		bool SetWarrantyFlags(int warrantyId, bool? published = null, bool? deleted = null)
		{
			try
			{
				DB.ExecuteSQL(
                    @"update dbo.Warranty 
					set Published = coalesce(@published, Published), Deleted = coalesce(@deleted, Deleted) 
					where WarrantyId = @warrantyId",
					new[]
					{
						new SqlParameter("published", (object)published ?? DBNull.Value),
						new SqlParameter("deleted", (object)deleted ?? DBNull.Value),
						new SqlParameter("warrantyId", warrantyId),
					});

				return true;
			}
			catch(Exception ex)
			{
				SysLog.LogException(ex, MessageTypeEnum.DatabaseException, MessageSeverityEnum.Error);
				return false;
			}
		}
	}
}
