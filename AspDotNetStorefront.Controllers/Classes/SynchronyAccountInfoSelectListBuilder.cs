// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Classes
{
    public class SynchronyAccountInfoSelectListBuilder
    {
        const string SelectListValueField = "Value";
        const string SelectListDataField = "Text";

        public SelectList BuildSynchronyAccountInfoSelectList()
        {
            return new SelectList(
                items: GetSynchronyAccountInfoName()
                    .Select(SynchronyAccountInfoName => new SelectListItem
                    {
                        Text = SynchronyAccountInfoName,
                        Value = SynchronyAccountInfoName
        }),
                dataValueField: SelectListValueField,
                dataTextField: SelectListDataField);
        }

        IEnumerable<string> GetSynchronyAccountInfoName()
        {
            var query = @"
				select SynchronyAccountInfoName
				from SynchronyAccountInfo with (nolock)
                order by AdminOrder";

            using (var connection = DB.dbConn())
            {
                connection.Open();
                using (var reader = DB.GetRS(query, connection))
                    while (reader.Read())
                        yield return DB.RSField(reader, "SynchronyAccountInfoName");
            }
        }
    }
}
