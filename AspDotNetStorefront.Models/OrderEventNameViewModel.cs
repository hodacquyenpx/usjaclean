// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AspDotNetStorefront.Validation.DataAttribute;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Models
{
    [Bind(Exclude = "ListOrderEventNames")]
    public class OrderEventNameViewModel
	{
        public SelectList ListOrderEventNames;

        [Required(ErrorMessage = "checkout.OrderEventName.required")]
        public string SelectedOrderEventName
        { get; set; }
    }
}
