// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Checkout
{
	public interface IPersistedCheckoutContextProvider
	{
		void ClearCheckoutContext(Customer customer);

		PersistedCheckoutContext LoadCheckoutContext(Customer customer);

		void SaveCheckoutContext(Customer customer, PersistedCheckoutContext checkoutContext);
	}

	public class PersistedCheckoutContext
	{
		public readonly CreditCardDetails CreditCard;
		public readonly PayPalExpressDetails PayPalExpress;
		public readonly PurchaseOrderDetails PurchaseOrder;
        public readonly CCWADetails CCWA;
        public readonly SynchronyDetails Synchrony;
        public readonly CashDetails Cash;
        public readonly CODDetails COD;
        public readonly CheckByMailDetails CheckByMail;
        public readonly CustomPaymentDetails CustomPayment;
        public readonly BraintreeDetails Braintree;
		public readonly AmazonPaymentsDetails AmazonPayments;
		public readonly bool TermsAndConditionsAccepted;
		public readonly bool Over13Checked;
        public readonly bool IsHoldOrder;
        public readonly ShippingEstimateDetails ShippingEstimateDetails;
		public readonly int? OffsiteRequiresBillingAddressId;
		public readonly int? OffsiteRequiresShippingAddressId;
		public readonly string Email;
		public readonly int? SelectedShippingMethodId;

		public PersistedCheckoutContext(CreditCardDetails creditCard,
			PayPalExpressDetails payPalExpress,
			PurchaseOrderDetails purchaseOrder,
            CCWADetails cCWA,
            SynchronyDetails synchrony,
            CashDetails cash,
            CODDetails cOD,
            CheckByMailDetails checkByMail,
            CustomPaymentDetails customPayment,
            BraintreeDetails braintree,
			AmazonPaymentsDetails amazonPayments,
			bool termsAndConditionsAccepted,
			bool over13Checked,
            bool isHoldOrder,
            ShippingEstimateDetails shippingEstimateDetails,
			int? offsiteRequiresBillingAddressId,
			int? offsiteRequiresShippingAddressId,
			string email,
			int? selectedShippingMethodId)
		{
			CreditCard = creditCard;
			PayPalExpress = payPalExpress;
			PurchaseOrder = purchaseOrder;
            CCWA = cCWA;
            Synchrony = synchrony;
            Cash = cash;
            COD = cOD;
            CheckByMail = checkByMail;
            CustomPayment = customPayment;
            Braintree = braintree;
			AmazonPayments = amazonPayments;
			TermsAndConditionsAccepted = termsAndConditionsAccepted;
			Over13Checked = over13Checked;
            IsHoldOrder = isHoldOrder;
			ShippingEstimateDetails = shippingEstimateDetails;
			OffsiteRequiresBillingAddressId = offsiteRequiresBillingAddressId;
			OffsiteRequiresShippingAddressId = offsiteRequiresShippingAddressId;
			Email = email;
			SelectedShippingMethodId = selectedShippingMethodId;
		}
	}

	public class CreditCardDetails
	{
		public readonly string Name;
		public readonly string Number;
		public readonly string IssueNumber;
		public readonly string CardType;
		public readonly DateTime? ExpirationDate;
		public readonly DateTime? StartDate;
		public readonly string Cvv;

		public CreditCardDetails(string name, string number, string issueNumber, string cardType, DateTime? expirationDate, DateTime? startDate, string cvv)
		{
			Name = name;
			Number = number;
			IssueNumber = issueNumber;
			CardType = cardType;
			ExpirationDate = expirationDate;
			StartDate = startDate;
			Cvv = cvv;
		}
	}

	public class PayPalExpressDetails
	{
		public readonly string Token;
		public readonly string PayerId;

		public PayPalExpressDetails(string token, string payerId)
		{
			Token = token;
			PayerId = payerId;
		}
	}

	public class PurchaseOrderDetails
	{
		public readonly string Number;

		public PurchaseOrderDetails(string number)
		{
			Number = number;
		}
	}

    public class CCWADetails
    {
        public readonly string Number;
        public readonly string PrimaryPaymentBalance;
        public readonly string SelectedsecondlyPayment;

        public CCWADetails(string number, string primarypaymentbalance, string selectedsecondlypayment)
        {
            Number = number;
            PrimaryPaymentBalance = primarypaymentbalance;
            SelectedsecondlyPayment = selectedsecondlypayment;
        }
    }

    public class SynchronyDetails
    {
        public readonly string AccountNumber;
        public readonly string ApplicationKey;
        public readonly string AccountInfo;
        public readonly string Term;
        public readonly string PrimaryPaymentBalance;
        public readonly string SelectedsecondlyPayment;

        public SynchronyDetails(string accountNumber, string applicationKey, string accountInfo, string term, string primarypaymentbalance, string selectedsecondlypayment)
        {
            AccountNumber = accountNumber;
            ApplicationKey = applicationKey;
            AccountInfo = accountInfo;
            Term = term;
            PrimaryPaymentBalance = primarypaymentbalance;
            SelectedsecondlyPayment = selectedsecondlypayment;
        }
    }

    public class CustomPaymentDetails
    {
        public readonly string PrimaryAccountNumber;
        public readonly string PrimaryApplicationKey;
        public readonly string PrimaryAccountInfo;
        public readonly string PrimaryTerm;
        public readonly string PrimaryPaymentBalance;
        public readonly string PrimaryAuthorizeNumber;
        public readonly string PrimaryPurchaseOrderNumber;
        public readonly string PrimarySamsSynchronyNumber;
        public readonly string SelectedPrimaryPayment;
        public readonly string SecondlyAccountNumber;
        public readonly string SecondlyApplicationKey;
        public readonly string SecondlyAccountInfo;
        public readonly string SecondlyTerm;
        public readonly string SecondlyPaymentBalance;
        public readonly string SelectedSecondlyPayment;
        public readonly string SecondlyAuthorizeNumber;
        public readonly string SecondlyPurchaseOrderNumber;
        public readonly string SecondlySamsSynchronyNumber;
        public readonly string ThirdlyAccountNumber;
        public readonly string ThirdlyApplicationKey;
        public readonly string ThirdlyAccountInfo;
        public readonly string ThirdlyTerm;
        public readonly string ThirdlyPaymentBalance;
        public readonly string SelectedThirdlyPayment;
        public readonly string ThirdlyAuthorizeNumber;
        public readonly string ThirdlyPurchaseOrderNumber;
        public readonly string ThirdlySamsSynchronyNumber;

        public CustomPaymentDetails(string primaryaccountNumber, string primaryapplicationKey, string primaryaccountInfo, string primaryterm, string primarypaymentbalance, string selectedprimarypayment, string primaryauthorizenumber, string primarypurchaseordernumber, string primarysamssynchronynumber, string secondlyaccountNumber, string secondlyapplicationKey, string secondlyaccountInfo, string secondlyterm, string secondlypaymentbalance, string selectedsecondlypayment, string secondlyauthorizenumber, string secondlypurchaseordernumber, string secondlysamssynchronynumber, string thirdlyaccountNumber, string thirdlyapplicationKey, string thirdlyaccountInfo, string thirdlyterm, string thirdlypaymentbalance, string selectedthirdlypayment, string thirdlyauthorizenumber, string thirdlypurchaseordernumber, string thirdlysamssynchronynumber)
        {
            PrimaryAccountNumber = primaryaccountNumber;
            PrimaryApplicationKey = primaryapplicationKey;
            PrimaryAccountInfo = primaryaccountInfo;
            PrimaryTerm = primaryterm;
            PrimaryPaymentBalance = primarypaymentbalance;
            SelectedPrimaryPayment = selectedprimarypayment;
            PrimaryAuthorizeNumber = primaryauthorizenumber;
            PrimaryPurchaseOrderNumber = primarypurchaseordernumber;
            PrimarySamsSynchronyNumber = primarysamssynchronynumber;

            SecondlyAccountNumber = secondlyaccountNumber;
            SecondlyApplicationKey = secondlyapplicationKey;
            SecondlyAccountInfo = secondlyaccountInfo;
            SecondlyTerm = secondlyterm;
            SecondlyPaymentBalance = secondlypaymentbalance;
            SelectedSecondlyPayment = selectedsecondlypayment;
            SecondlyAuthorizeNumber = secondlyauthorizenumber;
            SecondlyPurchaseOrderNumber = secondlypurchaseordernumber;
            SecondlySamsSynchronyNumber = secondlysamssynchronynumber;

            ThirdlyAccountNumber = thirdlyaccountNumber;
            ThirdlyApplicationKey = thirdlyapplicationKey;
            ThirdlyAccountInfo = thirdlyaccountInfo;
            ThirdlyTerm = thirdlyterm;
            ThirdlyPaymentBalance = thirdlypaymentbalance;
            SelectedThirdlyPayment = selectedthirdlypayment;
            ThirdlyAuthorizeNumber = thirdlyauthorizenumber;
            ThirdlyPurchaseOrderNumber = thirdlypurchaseordernumber;
            ThirdlySamsSynchronyNumber = thirdlysamssynchronynumber;
        }
    }

    public class CashDetails
    {
        public readonly string PrimaryPaymentBalance;
        public readonly string SelectedsecondlyPayment;

        public CashDetails(string primarypaymentbalance, string selectedsecondlypayment)
        {
            PrimaryPaymentBalance = primarypaymentbalance;
            SelectedsecondlyPayment = selectedsecondlypayment;
        }
    }

    public class CODDetails
    {
        public readonly string PrimaryPaymentBalance;
        public readonly string SelectedsecondlyPayment;

        public CODDetails(string primarypaymentbalance, string selectedsecondlypayment)
        {
            PrimaryPaymentBalance = primarypaymentbalance;
            SelectedsecondlyPayment = selectedsecondlypayment;
        }
    }

    public class CheckByMailDetails
    {
        public readonly string PrimaryPaymentBalance;
        public readonly string SelectedsecondlyPayment;

        public CheckByMailDetails(string primarypaymentbalance, string selectedsecondlypayment)
        {
            PrimaryPaymentBalance = primarypaymentbalance;
            SelectedsecondlyPayment = selectedsecondlypayment;
        }
    }

    public class BraintreeDetails
	{
		public readonly string Nonce;
		public readonly string Token;
		public readonly string PaymentMethod;
		public readonly bool ThreeDSecureApproved;

		public BraintreeDetails(string nonce, string token, string paymentMethod, bool threeDSecureApproved)
		{
			Nonce = nonce;
			Token = token;
			PaymentMethod = paymentMethod;
			ThreeDSecureApproved = threeDSecureApproved;
		}
	}

	public class AmazonPaymentsDetails
	{
		public readonly string AmazonOrderReferenceId;

		public AmazonPaymentsDetails(string amazonOrderReferenceId)
		{
			AmazonOrderReferenceId = amazonOrderReferenceId;
		}
	}

	public class ShippingEstimateDetails
	{
		public readonly string Country;
		public readonly string City;
		public readonly string State;
		public readonly string PostalCode;

		public ShippingEstimateDetails(
			string country,
			string city,
			string state,
			string postalCode)
		{
			Country = country;
			City = city;
			State = state;
			PostalCode = postalCode;
		}
	}
}
