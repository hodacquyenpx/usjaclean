// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Classes
{
    public class SalesRepNameSelectListBuilder
    {
        const string SelectListValueField = "Value";
        const string SelectListDataField = "Text";

        public SelectList BuildSalesRepNameSelectList()
        {
            return new SelectList(
                items: GetSalesRepName()
                    .Select(SalesRepName => new SelectListItem
                    {
                        Text = SalesRepName,
                        Value = SalesRepName
                    }),
                dataValueField: SelectListValueField,
                dataTextField: SelectListDataField);
        }

        IEnumerable<string> GetSalesRepName()
        {
            var query = @"
				select concat(firstName,' ',lastname) as SalesRepName
				from Customer with (nolock)
				where CustomerLevelID = 1
                and concat(firstName,' ',lastname) is not null and concat(firstName,' ',lastname) <> ''
				order by SalesRepName";

            using (var connection = DB.dbConn())
            {
                connection.Open();
                using (var reader = DB.GetRS(query, connection))
                    while (reader.Read())
                        yield return DB.RSField(reader, "SalesRepName");
            }
        }
    }
}
