// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AspDotNetStorefront.Validation.DataAttribute;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Models
{
    [Bind(Exclude = "ListSynchronyAccountInfo,ListSynchronyTerm,ListSecondaryPayment")]
    public class SynchronyViewModel
	{
		[Display(Name = "checkout.SynchronyAccountNumber.label", Prompt = "checkout.SynchronyAccountNumber.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyAccountNumber.required")]*/
        [RegularExpression(@"^(\d{16})$", ErrorMessage = "Account Number must be 16 digits")]
        public string AccountNumber
		{ get; set; }

        [Display(Name = "checkout.SynchronyApplicationKey.label", Prompt = "checkout.SynchronyApplicationKey.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        [RegularExpression(@"^(\d{8})$", ErrorMessage = "Application key must be 8 digits")]
        public string ApplicationKey
        { get; set; }

       
        public SelectList ListSynchronyAccountInfo;

        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        public string SelectedSynchronyAccountInfo
        { get; set; }

        public SelectList ListSynchronyTerm;

        /*[Required(ErrorMessage = "checkout.SynchronyTerm.required")]*/
        public string SelectedSynchronyTerm
        { get; set; }

        /*[Display(Name = "checkout.SynchronyAccountInfo.label", Prompt = "checkout.SynchronyAccountInfo.example")]
        [Required(ErrorMessage = "checkout.SynchronyAccountInfo.required")]
        public string AccountInfo
        { get; set; }*/

        [Display(Name = "checkout.PrimaryPaymentBalance.label")]
        [RegularExpression(@"\d{1,20}(\.\d{1,2})?", ErrorMessage = "Invalid Price. Please use the format of XXXX.XX.")]
        public string PrimaryPaymentBalance
        { get; set; }

        [Display(Name = "checkout.SecondaryPayment.label")]
        public SelectList ListSecondaryPayment;

        public string SelectedSecondaryPayment
        { get; set; }
    }
}
