// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Classes
{
    public class PromotionSelectListBuilder
    {
        const string SelectListValueField = "Value";
        const string SelectListDataField = "Text";

        public SelectList BuildPromotionSelectList()
        {
            return new SelectList(
                items: GetPromotion()
                    .Select(Promotion => new SelectListItem
                    {
                        Text = Promotion,
                        Value = Promotion
                    }),
                dataValueField: SelectListValueField,
                dataTextField: SelectListDataField);
        }

        IEnumerable<string> GetPromotion()
        {
            var query = @"
				select Name
				from Promotions with (nolock)
				where Active = 1 and AutoAssigned = 0
                order by case isnumeric(replace(name,'Off','')) 
		                 when 1 then cast(replace(name,'Off','') as int) end";

            using (var connection = DB.dbConn())
            {
                connection.Open();
                using (var reader = DB.GetRS(query, connection))
                    while (reader.Read())
                        yield return DB.RSField(reader, "Name");
            }
        }
    }
}
