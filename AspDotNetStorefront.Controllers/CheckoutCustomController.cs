// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using AspDotNetStorefront.Classes;
using AspDotNetStorefront.Caching.ObjectCaching;
using System.Linq;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutCustomController : Controller
	{
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;
        readonly ICachedShoppingCartProvider CachedShoppingCartProvider;

        public CheckoutCustomController(
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider,
            ICachedShoppingCartProvider cachedShoppingCartProvider)
        {
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
            CachedShoppingCartProvider = cachedShoppingCartProvider;
        }

		[PageTypeFilter(PageTypes.Checkout)]
		[HttpGet, ImportModelStateFromTempData]
		public ActionResult CustomPayment()
		{
			var customer = HttpContext.GetCustomer();
            var cart = CachedShoppingCartProvider.Get(customer, CartTypeEnum.ShoppingCart, AppLogic.StoreID());

            SelectList listoptionalpayment = new SelectList(AppLogic.AppConfig("Checkout.OptionalPaymentList").Split(',').ToList());

            if (!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMCustom, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

            var SelectListBuilder1 = new SynchronyAccountInfoSelectListBuilder();
            SelectList synchronyaccountinfonames = SelectListBuilder1.BuildSynchronyAccountInfoSelectList();

            var SelectListBuilder2 = new SynchronyTermSelectListBuilder();
            SelectList synchronytermnames = SelectListBuilder2.BuildSynchronyTermNameSelectList();

            //add 20190607 JBS
            var SelectListBuilder3 = new SynchronyPrimaryIDSelectListBuilder();
            SelectList synchronyprimaryIDnames = SelectListBuilder3.BuildSynchronyPrimaryIDNameSelectList();

            var SelectListBuilder4 = new IssuedStateSelectListBuilder();
            SelectList IssuedStatenames = SelectListBuilder4.BuildSynchronyIssuedStateNameSelectList();

            System.Collections.Generic.List<string> SelectListExpMonths = new System.Collections.Generic.List<string>();

            for (int i = 1; i < 13; i++)
            {
                SelectListExpMonths.Add(i.ToString().PadLeft(2,'0'));
            }
            //SelectList listexpiretionmonth = new SelectList(AppLogic.AppConfig("Checkout.ExpiretionMonthList").Split(',').ToList());
            SelectList listexpiretionmonth = new SelectList(SelectListExpMonths);

            System.Collections.Generic.List<string> SelectListExpYears = new System.Collections.Generic.List<string>();
            int currentYear = System.DateTime.Now.Year;

            for (int i = 0; i < 10; i++)
            {
                SelectListExpYears.Add((currentYear + i).ToString());
            }
            SelectList listexpiretionyear = new SelectList(SelectListExpYears);

            var model = BuildCustomPaymentViewModel(checkoutContext.CustomPayment);

            /***Create List***/
            /*For Primary Payment*/
            model.ListPrimarySynchronyAccountInfo = new SelectList(synchronyaccountinfonames, "Value", "Text", cart.SynchronyAccountInfo);
            model.ListPrimarySynchronyTerm = new SelectList(synchronytermnames, "Value", "Text", cart.SynchronyTerm);
            model.ListPrimaryCustomPayment = listoptionalpayment;
            model.ListPrimarySynchronyPrimaryID = new SelectList(synchronyprimaryIDnames, "Value", "Text", cart.SynchronyPrimaryID);
            model.ListPrimaryIssuedState = new SelectList(IssuedStatenames, "Value", "Text", cart.IssuedState);
            model.ListPrimaryExpiretionMonth = listexpiretionmonth;
            model.ListPrimaryExpiretionYear = listexpiretionyear;

            /*For Secondly Payment*/
            model.ListSecondlySynchronyAccountInfo = new SelectList(synchronyaccountinfonames, "Value", "Text", cart.SecondlySynchronyAccountInfo);
            model.ListSecondlySynchronyTerm = new SelectList(synchronytermnames, "Value", "Text", cart.SecondlySynchronyTerm);
            model.ListSecondlyCustomPayment = listoptionalpayment;
            model.ListSecondlySynchronyPrimaryID = new SelectList(synchronyprimaryIDnames, "Value", "Text", cart.SecondlySynchronyPrimaryID);

            model.ListSecondlyIssuedState = new SelectList(IssuedStatenames, "Value", "Text", cart.SecondlyIssuedState);
            model.ListSecondlyExpiretionMonth = listexpiretionmonth;
            model.ListSecondlyExpiretionYear = listexpiretionyear;

            /*For Thirdly Payment*/
            model.ListThirdlySynchronyAccountInfo = new SelectList(synchronyaccountinfonames, "Value", "Text", cart.ThirdlySynchronyAccountInfo);
            model.ListThirdlySynchronyTerm = new SelectList(synchronytermnames, "Value", "Text", cart.ThirdlySynchronyTerm);
            model.ListThirdlyCustomPayment = listoptionalpayment;
            model.ListThirdlySynchronyPrimaryID = new SelectList(synchronyprimaryIDnames, "Value", "Text", cart.ThirdlySynchronyPrimaryID);
            model.ListThirdlyIssuedState = new SelectList(IssuedStatenames, "Value", "Text", cart.ThirdlyIssuedState);
            model.ListThirdlyExpiretionMonth = listexpiretionmonth;
            model.ListThirdlyExpiretionYear = listexpiretionyear;

            return View(model);
		}

		[HttpPost, ExportModelStateToTempData]
		public ActionResult CustomPayment(CustomPaymentViewModel model)
		{
			if(!ModelState.IsValid)
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: checkoutContext.CCWA,
                synchrony: checkoutContext.Synchrony,
                cash: checkoutContext.Cash,
                cOD: checkoutContext.COD,
                checkByMail: checkoutContext.CheckByMail,
                customPayment: new CustomPaymentDetails(model.PrimaryAccountNumber, model.PrimaryApplicationKey, model.PrimarySelectedSynchronyAccountInfo, 
                                                        model.PrimarySelectedSynchronyTerm, model.PrimaryPaymentBalance, model.SelectedPrimaryPayment, 
                                                        model.PrimaryCCWA, model.PrimaryPurchaseOrderNumber, model.PrimarySamsSynchronyNumber, 
                                                        model.SecondlyAccountNumber, model.SecondlyApplicationKey, model.SecondlySelectedSynchronyAccountInfo, 
                                                        model.SecondlySelectedSynchronyTerm, model.SecondlyPaymentBalance, model.SelectedSecondlyPayment, 
                                                        model.SecondlyCCWA, model.SecondlyPurchaseOrderNumber, model.SecondlySamsSynchronyNumber, 
                                                        model.ThirdlyAccountNumber, model.ThirdlyApplicationKey, model.ThirdlySelectedSynchronyAccountInfo, 
                                                        model.ThirdlySelectedSynchronyTerm, model.ThirdlyPaymentBalance, model.SelectedThirdlyPayment, 
                                                        model.ThirdlyCCWA, model.ThirdlyPurchaseOrderNumber, model.ThirdlySamsSynchronyNumber,
                                                        model.PrimarySelectedSynchronyPrimaryID, model.SecondlySelectedSynchronyPrimaryID, model.ThirdlySelectedSynchronyPrimaryID,
                                                        model.PrimarySelectedIssuedState, model.SecondlySelectedIssuedState, model.ThirdlySelectedIssuedState,
                                                        model.PrimarySelectedExpiretionMonth, model.SecondlySelectedExpiretionMonth, model.ThirdlySelectedExpiretionMonth,
                                                        model.PrimarySelectedExpiretionYear, model.SecondlySelectedExpiretionYear, model.ThirdlySelectedExpiretionYear),
                braintree: checkoutContext.Braintree,
				amazonPayments: null,
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: null,
				offsiteRequiresShippingAddressId: null,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMCustom);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}

		public ActionResult CustomPaymentDetail()
		{
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCustomPaymentViewModel(checkoutContext.CustomPayment);

			return PartialView(ViewNames.CustomPaymentDetailPartial, model);
		}

		CustomPaymentViewModel BuildCustomPaymentViewModel(CustomPaymentDetails custompayment )
		{
            if (custompayment != null) { 
                return new CustomPaymentViewModel
                {
                    PrimaryAccountNumber = custompayment.PrimaryAccountNumber == null
                        ? string.Empty
                        : custompayment.PrimaryAccountNumber,
                    PrimaryApplicationKey = custompayment.PrimaryApplicationKey == null
                        ? string.Empty
                        : custompayment.PrimaryApplicationKey,
                    PrimarySelectedSynchronyAccountInfo = custompayment.PrimaryAccountInfo == null
                        ? string.Empty
                        : custompayment.PrimaryAccountInfo,
                    PrimarySelectedSynchronyTerm = custompayment.PrimaryTerm == null
                        ? string.Empty
                        : custompayment.PrimaryTerm,
                    PrimarySelectedSynchronyPrimaryID = custompayment.PrimaryPrimaryID == null
                        ? string.Empty
                        : custompayment.PrimaryPrimaryID,
                    PrimarySelectedIssuedState = custompayment.PrimaryIssuedState == null
                        ? string.Empty
                        : custompayment.PrimaryIssuedState,
                    PrimarySelectedExpiretionMonth = custompayment.PrimaryExpiretionMonth == null
                        ? string.Empty
                        : custompayment.PrimaryExpiretionMonth,
                    PrimarySelectedExpiretionYear = custompayment.PrimaryExpiretionYear == null
                        ? string.Empty
                        : custompayment.PrimaryExpiretionYear,
                    PrimaryPaymentBalance = custompayment.PrimaryPaymentBalance == null
                        ? string.Empty
                        : custompayment.PrimaryPaymentBalance,
                    SelectedPrimaryPayment = custompayment.SelectedPrimaryPayment == null
                        ? string.Empty
                        : custompayment.SelectedPrimaryPayment,
                    PrimaryCCWA = custompayment.PrimaryAuthorizeNumber == null
                        ? string.Empty
                        : custompayment.PrimaryAuthorizeNumber,
                    PrimaryPurchaseOrderNumber = custompayment.PrimaryPurchaseOrderNumber == null
                        ? string.Empty
                        : custompayment.PrimaryPurchaseOrderNumber,
                    PrimarySamsSynchronyNumber = custompayment.PrimarySamsSynchronyNumber == null
                        ? string.Empty
                        : custompayment.PrimarySamsSynchronyNumber,

                    SecondlyAccountNumber = custompayment.SecondlyAccountNumber == null
                        ? string.Empty
                        : custompayment.SecondlyAccountNumber,
                    SecondlyApplicationKey = custompayment.SecondlyApplicationKey == null
                        ? string.Empty
                        : custompayment.SecondlyApplicationKey,
                    SecondlySelectedSynchronyAccountInfo = custompayment.SecondlyAccountInfo == null
                        ? string.Empty
                        : custompayment.SecondlyAccountInfo,
                    SecondlySelectedSynchronyTerm = custompayment.SecondlyTerm == null
                        ? string.Empty
                        : custompayment.SecondlyTerm,
                    SecondlySelectedSynchronyPrimaryID = custompayment.SecondlyPrimaryID == null
                        ? string.Empty
                        : custompayment.SecondlyPrimaryID,
                    SecondlySelectedIssuedState = custompayment.SecondlyIssuedState == null
                        ? string.Empty
                        : custompayment.SecondlyIssuedState,
                    SecondlySelectedExpiretionMonth = custompayment.SecondlyExpiretionMonth == null
                        ? string.Empty
                        : custompayment.SecondlyExpiretionMonth,
                    SecondlySelectedExpiretionYear = custompayment.SecondlyExpiretionYear == null
                        ? string.Empty
                        : custompayment.SecondlyExpiretionYear,
                    SecondlyPaymentBalance = custompayment.SecondlyPaymentBalance == null
                        ? string.Empty
                        : custompayment.SecondlyPaymentBalance,
                    SelectedSecondlyPayment = custompayment.SelectedSecondlyPayment == null
                        ? string.Empty
                        : custompayment.SelectedSecondlyPayment,
                    SecondlyCCWA = custompayment.SecondlyAuthorizeNumber == null
                        ? string.Empty
                        : custompayment.SecondlyAuthorizeNumber,
                    SecondlyPurchaseOrderNumber = custompayment.SecondlyPurchaseOrderNumber == null
                        ? string.Empty
                        : custompayment.SecondlyPurchaseOrderNumber,
                    SecondlySamsSynchronyNumber = custompayment.SecondlySamsSynchronyNumber == null
                        ? string.Empty
                        : custompayment.SecondlySamsSynchronyNumber,

                    ThirdlyAccountNumber = custompayment.ThirdlyAccountNumber == null
                        ? string.Empty
                        : custompayment.ThirdlyAccountNumber,
                    ThirdlyApplicationKey = custompayment.ThirdlyApplicationKey == null
                        ? string.Empty
                        : custompayment.ThirdlyApplicationKey,
                    ThirdlySelectedSynchronyAccountInfo = custompayment.ThirdlyAccountInfo == null
                        ? string.Empty
                        : custompayment.ThirdlyAccountInfo,
                    ThirdlySelectedSynchronyTerm = custompayment.ThirdlyTerm == null
                        ? string.Empty
                        : custompayment.ThirdlyTerm,
                    ThirdlySelectedSynchronyPrimaryID = custompayment.ThirdlyPrimaryID == null
                        ? string.Empty
                        : custompayment.ThirdlyPrimaryID,
                    ThirdlySelectedIssuedState = custompayment.ThirdlyIssuedState == null
                        ? string.Empty
                        : custompayment.ThirdlyIssuedState,
                    ThirdlySelectedExpiretionMonth = custompayment.ThirdlyExpiretionMonth == null
                        ? string.Empty
                        : custompayment.ThirdlyExpiretionMonth,
                    ThirdlySelectedExpiretionYear = custompayment.ThirdlyExpiretionYear == null
                        ? string.Empty
                        : custompayment.ThirdlyExpiretionYear,
                    ThirdlyPaymentBalance = custompayment.ThirdlyPaymentBalance == null
                        ? string.Empty
                        : custompayment.ThirdlyPaymentBalance,
                    SelectedThirdlyPayment = custompayment.SelectedThirdlyPayment == null
                        ? string.Empty
                        : custompayment.SelectedThirdlyPayment,
                    ThirdlyCCWA = custompayment.ThirdlyAuthorizeNumber == null
                        ? string.Empty
                        : custompayment.ThirdlyAuthorizeNumber,
                    ThirdlyPurchaseOrderNumber = custompayment.ThirdlyPurchaseOrderNumber == null
                        ? string.Empty
                        : custompayment.ThirdlyPurchaseOrderNumber,
                    ThirdlySamsSynchronyNumber = custompayment.ThirdlySamsSynchronyNumber == null
                        ? string.Empty
                        : custompayment.ThirdlySamsSynchronyNumber
                };
            }
            else
            {
                return new CustomPaymentViewModel
                {
                    PrimaryAccountNumber = string.Empty,
                    PrimaryApplicationKey = string.Empty,
                    PrimarySelectedSynchronyAccountInfo = string.Empty,
                    PrimarySelectedSynchronyTerm = string.Empty,
                    PrimarySelectedSynchronyPrimaryID = string.Empty,
                    PrimarySelectedIssuedState = string.Empty,
                    PrimarySelectedExpiretionMonth = string.Empty,
                    PrimarySelectedExpiretionYear = string.Empty,
                    PrimaryPaymentBalance = string.Empty,
                    SelectedPrimaryPayment = string.Empty,
                    PrimaryCCWA = string.Empty,
                    PrimaryPurchaseOrderNumber = string.Empty,
                    PrimarySamsSynchronyNumber = string.Empty,

                    SecondlyAccountNumber = string.Empty,
                    SecondlyApplicationKey = string.Empty,
                    SecondlySelectedSynchronyAccountInfo = string.Empty,
                    SecondlySelectedSynchronyTerm = string.Empty,
                    SecondlySelectedSynchronyPrimaryID = string.Empty,
                    SecondlySelectedIssuedState = string.Empty,
                    SecondlySelectedExpiretionMonth = string.Empty,
                    SecondlySelectedExpiretionYear = string.Empty,
                    SecondlyPaymentBalance = string.Empty,
                    SelectedSecondlyPayment = string.Empty,
                    SecondlyCCWA = string.Empty,
                    SecondlyPurchaseOrderNumber = string.Empty,
                    SecondlySamsSynchronyNumber = string.Empty,

                    ThirdlyAccountNumber = string.Empty,
                    ThirdlyApplicationKey = string.Empty,
                    ThirdlySelectedSynchronyAccountInfo = string.Empty,
                    ThirdlySelectedSynchronyTerm = string.Empty,
                    ThirdlySelectedSynchronyPrimaryID = string.Empty,
                    ThirdlySelectedIssuedState = string.Empty,
                    ThirdlySelectedExpiretionMonth = string.Empty,
                    ThirdlySelectedExpiretionYear = string.Empty,
                    ThirdlyPaymentBalance = string.Empty,
                    SelectedThirdlyPayment = string.Empty,
                    ThirdlyCCWA = string.Empty,
                    ThirdlyPurchaseOrderNumber = string.Empty,
                    ThirdlySamsSynchronyNumber = string.Empty
                };
            }
        }
	}
}
