// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Web.UI.WebControls;
using AspDotNetStorefrontControls;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefrontAdmin
{
	public partial class EventName : AspDotNetStorefront.Admin.AdminPageBase
	{
		public const string PublishEventNameCommand = "eventname:publish";
		public const string UnpublishEventNameCommand = "eventname:unpublish";
		public const string DeleteEventNameCommand = "eventname:delete";

		protected void DispatchCommand(object sender, GridViewCommandEventArgs e)
		{
			if(e.CommandName == PublishEventNameCommand)
				PublishEventNameCommandHandler(e, true);
			else if(e.CommandName == UnpublishEventNameCommand)
				PublishEventNameCommandHandler(e, false);
			else if(e.CommandName == DeleteEventNameCommand)
				DeleteEventNameCommandHandler(e);
		}

		void PublishEventNameCommandHandler(CommandEventArgs e, bool published)
		{
			int eventId;
			if(!Int32.TryParse((string)e.CommandArgument, out eventId))
				return;

			if(SetEventNameFlags(eventId, published: published))
				ctrlAlertMessage.PushAlertMessage(
					String.Format(
						"Event {0}",
						published
							? "published"
							: "unpublished"),
					AlertMessage.AlertType.Success);
		}

		void DeleteEventNameCommandHandler(CommandEventArgs e)
		{
			int eventId;
			if(!Int32.TryParse((string)e.CommandArgument, out eventId))
				return;

			if(SetEventNameFlags(eventId, deleted: true))
				ctrlAlertMessage.PushAlertMessage("Event deleted", AlertMessage.AlertType.Success);
		}

		bool SetEventNameFlags(int eventId, bool? published = null, bool? deleted = null)
		{
			try
			{
				DB.ExecuteSQL(
					@"update dbo.EventName 
					set Published = coalesce(@published, Published), Deleted = coalesce(@deleted, Deleted) 
					where EventId = @eventId",
					new[]
					{
						new SqlParameter("published", (object)published ?? DBNull.Value),
						new SqlParameter("deleted", (object)deleted ?? DBNull.Value),
						new SqlParameter("eventId", eventId),
					});

				return true;
			}
			catch(Exception ex)
			{
				SysLog.LogException(ex, MessageTypeEnum.DatabaseException, MessageSeverityEnum.Error);
				return false;
			}
		}
	}
}
