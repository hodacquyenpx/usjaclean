// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using System.Linq;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutCashController : Controller
	{
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;

		public CheckoutCashController(
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider)
		{
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
		}

		[PageTypeFilter(PageTypes.Checkout)]
		[HttpGet, ImportModelStateFromTempData]
		public ActionResult Cash()
		{
			var customer = HttpContext.GetCustomer();

            SelectList listsecondlypayment = new SelectList(AppLogic.AppConfig("Checkout.secondlyPaymentListForCash").Split(',').ToList());


            if (!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMCash, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCashViewModel(checkoutContext.Cash);

            model.ListsecondlyPayment = listsecondlypayment;

            return View(model);
		}

		[HttpPost, ExportModelStateToTempData]
		public ActionResult Cash(CashViewModel model)
		{
			if(!ModelState.IsValid)
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: checkoutContext.CCWA,   
                synchrony: checkoutContext.Synchrony,
                cash: new CashDetails(model.PrimaryPaymentBalance, model.SelectedsecondlyPayment),
                cOD: checkoutContext.COD,
                checkByMail: checkoutContext.CheckByMail,
                customPayment: checkoutContext.CustomPayment,
                braintree: checkoutContext.Braintree,
				amazonPayments: null,
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: null,
				offsiteRequiresShippingAddressId: null,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMCash);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}

		public ActionResult CashDetail()
		{
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCashViewModel(checkoutContext.Cash);

			return PartialView(ViewNames.CashDetailPartial, model);
		}

		CashViewModel BuildCashViewModel(CashDetails cash)
		{
            if (cash != null)
            {
                return new CashViewModel
                {
                    PrimaryPaymentBalance = cash.PrimaryPaymentBalance == null
                        ? string.Empty
                        : cash.PrimaryPaymentBalance,
                    SelectedsecondlyPayment = cash.SelectedsecondlyPayment == null
                        ? string.Empty
                        : cash.SelectedsecondlyPayment
                };
            }
            else
            {
                return new CashViewModel
                {
                    PrimaryPaymentBalance = string.Empty,
                    SelectedsecondlyPayment = string.Empty,
                };
            }
            }
	}
}
