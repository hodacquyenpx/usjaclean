// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using AspDotNetStorefront.Classes;
using AspDotNetStorefront.Caching.ObjectCaching;
using System.Linq;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutSynchronyController : Controller
	{
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;
        readonly ICachedShoppingCartProvider CachedShoppingCartProvider;

        public CheckoutSynchronyController(
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider,
            ICachedShoppingCartProvider cachedShoppingCartProvider)
        {
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
            CachedShoppingCartProvider = cachedShoppingCartProvider;
        }

		[PageTypeFilter(PageTypes.Checkout)]
		[HttpGet, ImportModelStateFromTempData]
		public ActionResult Synchrony()
		{
			var customer = HttpContext.GetCustomer();
            var cart = CachedShoppingCartProvider.Get(customer, CartTypeEnum.ShoppingCart, AppLogic.StoreID());

            SelectList listsecondlypayment = new SelectList(AppLogic.AppConfig("Checkout.secondlyPaymentListForSynchrony").Split(',').ToList());


            if (!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMSynchrony, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

            var SelectListBuilder1 = new SynchronyAccountInfoSelectListBuilder();
            SelectList synchronyaccountinfonames = SelectListBuilder1.BuildSynchronyAccountInfoSelectList();

            var SelectListBuilder2 = new SynchronyTermSelectListBuilder();
            SelectList synchronytermnames = SelectListBuilder2.BuildSynchronyTermNameSelectList();

            var model = BuildSynchronyViewModel(checkoutContext.Synchrony);
            model.ListSynchronyAccountInfo = new SelectList(synchronyaccountinfonames, "Value", "Text", cart.SynchronyAccountInfo);
            model.ListSynchronyTerm = new SelectList(synchronytermnames, "Value", "Text", cart.SynchronyTerm);

            model.ListsecondlyPayment = listsecondlypayment;

            return View(model);
		}

		[HttpPost, ExportModelStateToTempData]
		public ActionResult Synchrony(SynchronyViewModel model)
		{
			if(!ModelState.IsValid)
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: checkoutContext.CCWA,
                synchrony: new SynchronyDetails(model.AccountNumber, model.ApplicationKey, model.SelectedSynchronyAccountInfo, model.SelectedSynchronyTerm, model.PrimaryPaymentBalance, model.SelectedsecondlyPayment),
                cash: checkoutContext.Cash,
                cOD: checkoutContext.COD,
                checkByMail: checkoutContext.CheckByMail,
                customPayment: checkoutContext.CustomPayment,
                braintree: checkoutContext.Braintree,
				amazonPayments: null,
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: null,
				offsiteRequiresShippingAddressId: null,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMSynchrony);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}

		public ActionResult SynchronyDetail()
		{
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildSynchronyViewModel(checkoutContext.Synchrony);

			return PartialView(ViewNames.SynchronyDetailPartial, model);
		}

		SynchronyViewModel BuildSynchronyViewModel(SynchronyDetails synchrony )
		{
            if (synchrony !=null) { 
                return new SynchronyViewModel
                {
                    AccountNumber = synchrony.AccountNumber == null
                        ? string.Empty
                        : synchrony.AccountNumber,
                    ApplicationKey = synchrony.ApplicationKey == null
                        ? string.Empty
                        : synchrony.ApplicationKey,
                    SelectedSynchronyAccountInfo = synchrony.AccountInfo == null
                        ? string.Empty
                        : synchrony.AccountInfo,
                    SelectedSynchronyTerm = synchrony.Term == null
                        ? string.Empty
                        : synchrony.Term,
                    PrimaryPaymentBalance = synchrony.PrimaryPaymentBalance == null
                        ? string.Empty
                        : synchrony.PrimaryPaymentBalance,
                    SelectedsecondlyPayment = synchrony.SelectedsecondlyPayment == null
                        ? string.Empty
                        : synchrony.SelectedsecondlyPayment
                };
            }
            else
            {
                return new SynchronyViewModel
                {
                    AccountNumber = string.Empty,
                    ApplicationKey = string.Empty,
                    SelectedSynchronyAccountInfo = string.Empty,
                    SelectedSynchronyTerm = string.Empty,
                    PrimaryPaymentBalance = string.Empty,
                    SelectedsecondlyPayment = string.Empty,
                };
            }
        }
	}
}
