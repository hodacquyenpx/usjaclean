<%@ Page Language="C#" %>
<script runat="server">
    void Login_Click(Object sender, EventArgs e)
    {
        if (SystemPassword.Text == "usj#1")
        {
            Session["BaseAuth"] = "success";
            if(Request.QueryString["systemreturnurl"] != null) {
                Response.Redirect(Request.QueryString["systemreturnurl"]);
            }
            else
            {
                Response.Redirect("/");
            }
        }
        else
        {
            ErrorMSG.Text = "Invalid Password";
        }
    }
    void ClearLogin_Click(Object sender, EventArgs e)
    {
        Session.Remove("BaseAuth");
    }
</script>
<html>
<head>
  <title>Daiwa.space system login</title>
    <link rel="stylesheet" type="text/css" href="login.css" />
</head>
<body>
  <form runat="server">
    <div class="loginbox">
       <asp:Label id="ErrorMSG" runat="server" Text=""></asp:Label>
       <br>
       <img class="toppage_logo" src="/skins/default/images/daiwa-logo.png" />
       <br>
       <asp:TextBox id="SystemPassword" runat="server" placeholder="Enter system password here"></asp:TextBox>
       <br>
       <asp:Button id="Login" runat="server" onclick="Login_Click" Text="Login"></asp:Button>
       <!--<br>
       <asp:Button id="ClearLoginSession" runat="server" onclick="ClearLogin_Click" Text="Clear Login"></asp:Button>-->
    </div>
  </form>
</body>
</html>