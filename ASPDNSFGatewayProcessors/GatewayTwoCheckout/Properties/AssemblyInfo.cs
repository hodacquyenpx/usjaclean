// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GatewayTwoCheckout")]
[assembly: AssemblyDescription("AspDotNetStorefront 2Checkout Gateway Library")]
[assembly: AssemblyCompany("AspDotNetStorefront.com")]
[assembly: AssemblyProduct("AspDotNetStorefront MultiStore")]
[assembly: AssemblyCopyright("Copyright AspDotNetStorefront")]

[assembly: AssemblyVersion("10.0.2")]
[assembly: AssemblyFileVersion("10.0.2.3645")]
[assembly: AssemblyInformationalVersion("7f0e5f765ee41caccbe8a1e0f7ad81996711eeb5")]

[assembly: ComVisible(false)]
[assembly: CLSCompliant(false)]
