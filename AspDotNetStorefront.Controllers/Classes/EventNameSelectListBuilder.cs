// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Classes
{
    public class EventNameSelectListBuilder
    {
        const string SelectListValueField = "Value";
        const string SelectListDataField = "Text";

        public SelectList BuildEventNameSelectList()
        {
            return new SelectList(
                items: GetEventName()
                    .Select(ConvinedEventName => new SelectListItem
                    {
                        Text = ConvinedEventName,
                        Value = ConvinedEventName.Split(':')[0]
        }),
                dataValueField: SelectListValueField,
                dataTextField: SelectListDataField);
        }

        IEnumerable<string> GetEventName()
        {
            var query = @"
				select concat(EventName,':(TaxRate:',TaxRate,'%)') ConvinedEventName
				from EventName with (nolock)
				where Published = 1
                and Deleted = 0
                order by EventName";

            using (var connection = DB.dbConn())
            {
                connection.Open();
                using (var reader = DB.GetRS(query, connection))
                    while (reader.Read())
                        yield return DB.RSField(reader, "ConvinedEventName");
            }
        }
    }
}
