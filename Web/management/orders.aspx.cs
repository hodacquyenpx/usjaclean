// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI.WebControls;
using AspDotNetStorefrontCore;
using System.Collections.Generic;

namespace AspDotNetStorefrontAdmin
{
	public partial class Orders : AspDotNetStorefront.Admin.AdminPageBase
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
			Response.CacheControl = "private";
			Response.Expires = 0;
			Response.AddHeader("pragma", "no-cache");

            //Set custom navigation by JBS
            String LoginBranch = "";
            String CustomerLevelId = "";
            using (var conn = new SqlConnection(DB.GetDBConn()))
            {
                conn.Open();
                using (var rs = DB.GetRS("select branch,CustomerLevelID from customer where customerid = " + ThisCustomer.CustomerID, conn))
                {
                    while (rs.Read())
                    {
                        LoginBranch = rs["branch"].ToString();
                        CustomerLevelId = rs["CustomerLevelID"].ToString();
                    }
                }
            }

            string order_cnt_new = "";
            string order_cnt_paid = "";
            string order_cnt_assigned = "";
            string order_cnt_scheduled= "";
            string order_cnt_called = "";
            string order_cnt_delivered = "";
            string order_cnt_cancel = "";
            string order_cnt_hold = "";
            string order_branches = "";

            /*Set login user's branch for the default. Show all when the user is dispatcher*/
            if (String.IsNullOrEmpty(Request.QueryString["filter.7.0"]))
            {
                if (CustomerLevelId == "3") { 
                    order_branches = "All";
                    order_cnt_new = GetOrderCount(order_branches, "New").ToString();
                    order_cnt_paid = GetOrderCount(order_branches, "Printed").ToString();
                    order_cnt_assigned = GetOrderCount(order_branches, "Assigned").ToString();
                    order_cnt_scheduled = GetOrderCount(order_branches, "Scheduled").ToString();
                    order_cnt_called = GetOrderCount(order_branches, "Called").ToString();
                    order_cnt_delivered = GetOrderCount(order_branches, "Delivered").ToString();
                    order_cnt_cancel = GetOrderCount(order_branches, "Cancelled").ToString();
                    order_cnt_hold = GetOrderCount(order_branches, "Hold").ToString();
                }
                else
                {
                    order_branches = LoginBranch;
                    order_cnt_new = GetOrderCount(order_branches,"New").ToString();
                    order_cnt_paid = GetOrderCount(order_branches, "Printed").ToString();
                    order_cnt_assigned = GetOrderCount(order_branches, "Assigned").ToString();
                    order_cnt_scheduled = GetOrderCount(order_branches, "Scheduled").ToString();
                    order_cnt_called = GetOrderCount(order_branches, "Called").ToString();
                    order_cnt_delivered = GetOrderCount(order_branches, "Delivered").ToString();
                    order_cnt_cancel = GetOrderCount(order_branches, "Cancelled").ToString();
                    order_cnt_hold = GetOrderCount(order_branches, "Hold").ToString();
                }
            }
            else
            {
                order_branches = Request.QueryString["filter.7.0"].ToString();
                order_cnt_new = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "New").ToString();
                order_cnt_paid = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Printed").ToString();
                order_cnt_assigned = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Assigned").ToString();      //add chung 20190511
                order_cnt_scheduled = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Scheduled").ToString();
                order_cnt_called = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Called").ToString();
                order_cnt_delivered = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Delivered").ToString();
                order_cnt_cancel = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Cancelled").ToString();
                order_cnt_hold = GetOrderCount(Request.QueryString["filter.7.0"].ToString(), "Hold").ToString();
            }

            string seleced_new_status = "";
            string seleced_paymentok_status = "";
            string seleced_assignok_status = "";
            string seleced_scheduledok_status = "";
            string seleced_shipok_status = "";
            string seleced_shipped_status = "";
            string seleced_cancel_status = "";
            string seleced_hold_status = "";

            if (!String.IsNullOrEmpty(Request.QueryString["filter.8.0"])) { 
                switch (Request.QueryString["filter.8.0"].ToString())
                {
                    case "New":
                        seleced_new_status = "font-weight:bold";
                        break;
                    case "Assigned":
                        seleced_assignok_status = "font-weight:bold";
                        break;
                    case "Printed":
                        seleced_paymentok_status = "font-weight:bold";
                        break;
                    case "Scheduled":
                        seleced_scheduledok_status = "font-weight:bold";
                        break;
                    //case "Dispatched / Shipped":
                    case "Called":
                        seleced_shipok_status = "font-weight:bold";
                        break;
                    case "Delivered":
                        seleced_shipped_status = "font-weight:bold";
                        break;
                    case "Cancelled":
                        seleced_cancel_status = "font-weight:bold";
                        break;
                    case "Hold":
                        seleced_hold_status = "font-weight:bold";
                        break;
                }
            }
            NewOrderLink.Text = "<span style='"+ seleced_new_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=New> New </a> (" + order_cnt_new + ")</span><span><img src='/images/arrow.png'></span>";
            PaidOrderLink.Text = "<span style='" + seleced_paymentok_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Printed> Printed </a> (" + order_cnt_paid + ")</span><span><img src='/images/arrow.png'></span>";
            AssigndOrderLink.Text = "<span style='" + seleced_assignok_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Assigned> Assigned </a> (" + order_cnt_assigned + ")</span><span><img src='/images/arrow.png'></span>";
            ScheduledOrderLink.Text = "<span style='" + seleced_scheduledok_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Scheduled> Scheduled </a> (" + order_cnt_scheduled + ")</span><span><img src='/images/arrow.png'></span>";
            CalledOrderLink.Text = "<span style='" + seleced_shipok_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Called> Called </a> (" + order_cnt_called + ")</span><span><img src='/images/arrow.png'></span>";
            DeliveredOrderLink.Text = "<span style='" + seleced_shipped_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Delivered> Delivered </a> (" + order_cnt_delivered + ")</span><span>|</span>";
            CancelOrderLink.Text = "<span style='" + seleced_cancel_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Cancelled> Cancelled </a> (" + order_cnt_cancel + ")</span>";
            HoldOrderLink.Text = "<span style='" + seleced_hold_status + "'><a href = /management/orders.aspx?filter.7.0=" + order_branches + "&filter.8.0=Hold> Hold </a> (" + order_cnt_hold + ")</span>";
        }

        protected string BuildOrderItems(int orderNumber)
		{
			var order = new Order(orderNumber, LocaleSetting);

			// Create lines for the first two items
			var firstTwoItems = order.CartItems
				.Take(2)
				.Select(item => String.Format(
					"<div class=\"order-item\">{0}</div>",
					AppLogic.MakeProperObjectName(
						item.ProductName,
						item.VariantName,
						LocaleSetting)));

			// If there are more than two items, show a link to the order details
			var moreLink = order.CartItems
				.Skip(2)
				.Select(item => String.Format(
					"<div class=\"order-item\"><a href=\"order.aspx?ordernumber={0}\">{1}</a></div>",
					orderNumber,
					AppLogic.GetString("admin.orderdetails.More", LocaleSetting)));

			// Combine them together
			var displayedItems = firstTwoItems
				.Concat(moreLink)
				.Take(3);

			return String.Join(Environment.NewLine, displayedItems);
		}

		protected void btnPrint_Click(object sender, EventArgs e)
		{
			// Get the list of order numbers from the grid
			var orderIdsToPrint = grdOrders
				.DataKeys
				.Cast<System.Web.UI.WebControls.DataKey>()
				.Where(dataKey => dataKey != null)
				.Where(dataKey => dataKey.Value != null)
				.Select(dataKey => dataKey.Value);

			if(!orderIdsToPrint.Any())
				return;

			// Pop up a window
			var popupUrl = String.Format("printreceipts.aspx?ordernumbers={0}", String.Join(",", orderIdsToPrint));
			var popupScript = String.Format("window.open('{0}', 'popup_window', 'height=600,width=800,top=0,left=0,status=yes,toolbar=yes,menubar=yes,scrollbars=yes,location=yes');", popupUrl);
			ClientScript.RegisterStartupScript(this.GetType(), "printReceiptsPopup", popupScript, true);
		}

		protected void btnBulkSaveIsNew_Click(object sender, EventArgs e)
		{
			//var isNewSql = @"UPDATE Orders SET IsNew = @isNew, IsHoldOrder = @isHoldOrder WHERE OrderNumber = @orderNumber";
            var isNewSql = @"UPDATE Orders SET IsHoldOrder = @isHoldOrder, OrderStatus = 'New' WHERE OrderNumber = @orderNumber";
            var isHoldSql = @"UPDATE Orders SET IsHoldOrder = @isHoldOrder, OrderStatus = 'Hold' WHERE OrderNumber = @orderNumber";

            using (var connection = new SqlConnection(DB.GetDBConn()))
			{
				connection.Open();

				foreach(GridViewRow row in grdOrders.Rows)
				{
					if(row.RowType != DataControlRowType.DataRow)
						continue;

					var orderNumber = (int)grdOrders.DataKeys[row.DataItemIndex].Values["OrderNumber"];

					var chkNew = (CheckBox)row.FindControl("chkNew");

                    var chkIsHoldOrder = (CheckBox)row.FindControl("chkIsHoldOrder");

                    if (!chkIsHoldOrder.Checked){ 
                        DB.ExecuteSQL(isNewSql, 
						    connection,
						    //new SqlParameter("@isNew", chkNew.Checked),
                            new SqlParameter("@isHoldOrder", chkIsHoldOrder.Checked),
                            new SqlParameter("@orderNumber", orderNumber));
                    }
                    else
                    {
                        DB.ExecuteSQL(isHoldSql,
                            connection,
                            //new SqlParameter("@isNew", chkNew.Checked),
                            new SqlParameter("@isHoldOrder", chkIsHoldOrder.Checked),
                            new SqlParameter("@orderNumber", orderNumber));
                    }
                }
			}
		}

        int GetOrderCount(string branches,string orderstatus)
        {
            string sql = "";

            sql = "SELECT count(*) as N from orders";

            if(!String.IsNullOrEmpty(orderstatus)) { 
                sql += " where OrderStatus = '"+ orderstatus + "'";

                if (orderstatus == "NEW")
                    sql += " or OrderStatus is Null";
            }

            

            if (!String.IsNullOrEmpty(branches) && branches != "All")
            {
                string joined = "'" +  branches.Replace(",","','") + "'";
                sql += " and branch in (" + joined + ")";
            }
            return DB.GetSqlN(sql);
        }
    }
}
