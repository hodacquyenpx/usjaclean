<?xml version="1.0" standalone="yes" ?>
<!-- ##################################################################################	-->
<!-- Copyright AspDotNetStorefront.com. All Rights Reserved.							-->
<!-- http://www.aspdotnetstorefront.com													-->
<!-- For details on this license please visit the product homepage at the URL above.	-->
<!-- THE ABOVE NOTICE MUST REMAIN INTACT.												-->
<!-- ##################################################################################	-->
<package version="2.1" displayname="Order Shipped" debug="false">
	<query name="Order" rowElementName="OrderInfo">
		<sql>
			<![CDATA[
            select o.*
            From orders o with (NOLOCK) 
            where o.ordernumber = @ordernum
            ]]>
		</sql>
		<queryparam paramname="@ordernum" paramtype="runtime" requestparamname="ordernumber" defvalue="0" sqlDataType="int" validationpattern="^\d{1,9}$"/>
	</query>
	<PackageTransform>
		<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:aspdnsf="urn:aspdnsf" exclude-result-prefixes="aspdnsf">
			<xsl:output method="html" omit-xml-declaration="yes"  encoding="ISO-8859-1" />

			<xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
			<xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
			<xsl:param name="ShowCustomerServiceNotesInReceipts" select="aspdnsf:AppConfigBool('ShowCustomerServiceNotesInReceipts')" />
			<xsl:param name="StoreURL">
				<xsl:value-of select="/root/Runtime/StoreUrl" />
			</xsl:param>

			<xsl:variable name="StoreId" select="root/Order/OrderInfo/StoreID" />
			<xsl:variable name="lower" select="'abcdefghijklmnopqrstuvwxyz'" />
			<xsl:variable name="upper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
			<xsl:variable name="carrier" select="translate(/root/Order/OrderInfo/ShippedVIA, $upper, $lower)" />
			<xsl:variable name="trackingNumber" select="/root/Order/OrderInfo/ShippingTrackingNumber" />

			<xsl:template match="/">
				<html xmlns="http://www.w3.org/1999/xhtml">
					<head>
						<title>
							<xsl:value-of select="aspdnsf:AppConfig($StoreId, 'StoreName')" disable-output-escaping="yes" /> - <xsl:value-of select="aspdnsf:StringResource('notification.shipped.7')" disable-output-escaping="yes" />
						</title>
					</head>
					<body>
						<xsl:choose>
							<xsl:when test="count(/root/Order/OrderInfo) > 0">
								<div style="font:normal 12pt Arial;color:;#000080;">
									<p>
										<xsl:value-of select="aspdnsf:StringResource('notification.shipped.1')" disable-output-escaping="yes" />&#0160;<xsl:value-of select="/root/Order/OrderInfo/OrderNumber" />&#0160;<xsl:value-of select="aspdnsf:StringResource('notification.shipped.2')" disable-output-escaping="yes" />.<br />
                    <xsl:value-of select="aspdnsf:StringResource('notification.shipped.5')" disable-output-escaping="yes" />
                  </p>
                  <xsl:choose>
                    <xsl:when test="/root/Order/OrderInfo/ShippedVIA!=''">
                      <p>
                        Carrier: <xsl:value-of select="/root/Order/OrderInfo/ShippedVIA" />
                      </p>
                      <p>
                        <!-- If the Carrier field contains none of the above we just show the tracking number without a link. -->
                        Tracking Number: <xsl:value-of select="$trackingNumber" />
                      </p>
                    </xsl:when>
                  </xsl:choose>
										<!--<xsl:value-of select="aspdnsf:StringResource('notification.shipped.3')" disable-output-escaping="yes" />&#0160;-->
										<xsl:choose>
											<xsl:when test="contains($carrier, 'fedex')">
                        <p>
												<!-- If the Carrier field contains 'fedex' build a FedEx tracking link. -->
												  <a href="{concat(aspdnsf:AppConfig($StoreId, 'Shipping.Tracking.Fedex'), $trackingNumber)}" target="_blank">
													  <xsl:value-of select="$trackingNumber" />
												  </a>
                        </p>
											</xsl:when>
											<xsl:when test="contains($carrier, 'ups')">
                        <p>
												<!-- If the Carrier field contains 'ups' build a UPS tracking link. -->
												<a href="{concat(aspdnsf:AppConfig($StoreId, 'Shipping.Tracking.Ups'), $trackingNumber)}" target="_blank">
													<xsl:value-of select="$trackingNumber" />
												</a>
                        </p>
											</xsl:when>
											<xsl:when test="contains($carrier, 'usps')">
                        <p>
												<!-- If the Carrier field contains 'usps' build a USPS tracking link. -->
												<a href="{concat(aspdnsf:AppConfig($StoreId, 'Shipping.Tracking.Usps'), $trackingNumber)}" target="_blank">
													<xsl:value-of select="$trackingNumber" />
												</a>
                        </p>
											</xsl:when>
											<xsl:otherwise>
                        
											</xsl:otherwise>
										</xsl:choose>

                  <!--<p>
                    <xsl:value-of select="aspdnsf:StringResource('notification.shipped.4')" disable-output-escaping="yes" />&#0160;<a href="{$StoreURL}">
											<xsl:value-of select="aspdnsf:AppConfig($StoreId, 'StoreName')" disable-output-escaping="yes" />
										</a>.
									</p>-->
								</div>
								<div style="font:normal 12pt Arial;color:;#000080;">
                  <!--<a href="{$StoreURL}">
											<xsl:value-of select="aspdnsf:AppConfig($StoreId, 'StoreName')" disable-output-escaping="yes" />-->
                  <!--</a>&#0160;-->
                  <!--<xsl:value-of select="aspdnsf:StringResource('notification.shipped.5')" disable-output-escaping="yes" />  -->              
								</div>
                
                <!--<div style="font:normal 12pt Arial;color:;#000080;">
                  <xsl:value-of select="aspdnsf:StringResource('notification.shipped.8')" disable-output-escaping="yes" /><br/>
                  <xsl:value-of select="aspdnsf:StringResource('notification.shipped.9')" disable-output-escaping="yes" />
                </div>-->

                <xsl:value-of select="aspdnsf:Topic('ShippedFooter')" disable-output-escaping="yes" />
                
                <!-- <p>
									<font face="Arial" size="1">
										<xsl:value-of select="aspdnsf:StringResource('notification.shipped.6')" disable-output-escaping="yes" />
									</font>
								</p>
								<p>&#0160;</p>-->
							</xsl:when>
							<xsl:otherwise>
								<div style="font:normal 10pt Arial;color:;#000080;">
									<xsl:value-of select="aspdnsf:StringResource('notification.shipped.1')" disable-output-escaping="yes" />&#0160;9999999&#0160;<xsl:value-of select="aspdnsf:StringResource('notification.shipped.2')" disable-output-escaping="yes" />&#0160;GROUND.
									<xsl:value-of select="aspdnsf:StringResource('notification.shipped.3')" disable-output-escaping="yes" />&#0160;1234567
									<xsl:value-of select="aspdnsf:StringResource('mailingtest.aspx.10')"/>:&#160;<xsl:value-of select="/root/System/Date"/>&#160;<xsl:value-of select="/root/System/Time"/>
								</div>
								<div style="font:normal 10pt Arial;color:;#000080;">
									<strong>
										<a href="{$StoreURL}">
											<xsl:value-of select="aspdnsf:AppConfig($StoreId, 'StoreName')" disable-output-escaping="yes" />
										</a>&#0160;<xsl:value-of select="aspdnsf:StringResource('notification.shipped.5')" disable-output-escaping="yes" />
									</strong>
								</div>
								<p>
									<font face="Arial" size="1">
										<xsl:value-of select="aspdnsf:StringResource('notification.shipped.6')" disable-output-escaping="yes" />
									</font>
								</p>
								<p>&#0160;</p>
							</xsl:otherwise>
						</xsl:choose>
					</body>
				</html>
			</xsl:template>
		</xsl:stylesheet>
	</PackageTransform>
</package>
