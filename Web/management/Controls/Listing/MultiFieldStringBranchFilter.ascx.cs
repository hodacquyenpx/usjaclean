// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using AspDotNetStorefrontCore;


namespace AspDotNetStorefrontControls.Listing
{
	public partial class MultiFieldStringBranchFilter : FilterControl
	{
		public string Label
		{ get; set; }

		[TypeConverter(typeof(StringArrayConverter))]
		public string[] Fields
		{ get; set; }

        public string Customerid
        { get; set; }

        private string LoginBranch
        { get; set; }

        private string CustomerLevelId
        { get; set; }

        protected override void OnInit(EventArgs e)
        {
            using (var conn = new SqlConnection(DB.GetDBConn()))
            {
                conn.Open();

                using (var rs = DB.GetRS("exec [dbo].[aspdnsf_getBranches]", conn))
                {
                    ListItem item_all = new ListItem();
                    item_all.Text = "All";
                    item_all.Value = "All";
                    chkBranchList.Items.Add(item_all);

                    while (rs.Read())
                    {
                        ListItem item = new ListItem();
                        item.Text = rs["branchname"].ToString();
                        item.Value = rs["branchname"].ToString();
                        if(item.Value == Value.Text)
                        {
                            item.Selected = true;
                        }
                        //item.Selected = Convert.ToBoolean(sdr["IsSelected"]);
                        chkBranchList.Items.Add(item);
                    }
                    
                }

                using (var rs = DB.GetRS("select branch,CustomerLevelID from customer where customerid = " + Customerid, conn))
                {
                    while (rs.Read())
                    {
                        LoginBranch = rs["branch"].ToString();
                        CustomerLevelId = rs["CustomerLevelID"].ToString();
                    }
                }
            }
        }

        protected override void OnPreRender(EventArgs e)
		{
			ValueLabel.Visible = !String.IsNullOrEmpty(Label);
			ValueLabel.Text = Label;

			var filterNames = GenerateFilterNames();
			Value.Attributes.Add("data-filter-name", filterNames.First());
        }

		protected override void SetValues(IEnumerable<KeyValuePair<string, string>> values)
		{
            if (String.IsNullOrWhiteSpace(values.First().Value)) {
                //Value.Text = null;

                /*Set login user's branch for the default. Show all when the user is dispatcher*/
                if (String.IsNullOrEmpty(Value.Text) && CustomerLevelId == "3")
                {
                    Value.Text = "All";
                }
                else if (String.IsNullOrEmpty(Value.Text) && LoginBranch != null)
                {
                    Value.Text = LoginBranch;
                }
            }
            else { 
				Value.Text = values.First().Value;   
            }

            string[] branch_values = Value.Text.Split(',');
            foreach (ListItem ListItem in chkBranchList.Items)
            {
                for (var i = 0; i < branch_values.Count(); i++)
                {
                    if (ListItem.Value == branch_values[i])
                    {
                        ListItem.Selected = true;
                        break;
                    }
                }

            }
        }

		protected override FilterClause GetFilterClause(IEnumerable<string> parameterNames)
		{
			var selectedValueParameterName = parameterNames.First();

            /*Set checked branch*/
            string selected_value = "'" + Value.Text.Replace(",","','") + "'";
            if (Value.Text == "All")
            {
                return new FilterClause(
               (Fields == null || !Fields.Any())
                   ? null
                   : String.Format(
                       "",
                       selectedValueParameterName,
                       Fields.Aggregate(
                           String.Empty,
                           (a, field) => String.Format("{0}", a, field, selectedValueParameterName))),
               new[] { new ControlParameter(selectedValueParameterName, System.Data.DbType.String, Value.UniqueID, "Text") });

            }
            else { 
            return new FilterClause(
				(Fields == null || !Fields.Any())
					? null
					: String.Format(
						"(@{0} is null {1})",
						selectedValueParameterName,
						Fields.Aggregate(
							String.Empty,
							(a, field) => String.Format("{0} or {1} in (" + selected_value + ")", a, field, selectedValueParameterName))),
				new[] { new ControlParameter(selectedValueParameterName, System.Data.DbType.String, Value.UniqueID, "Text") });

            }
        }
    }
}
