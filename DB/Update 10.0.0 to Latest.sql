-- ------------------------------------------------------------------------------------------
-- Copyright AspDotNetStorefront.com.  All Rights Reserved.
-- http://www.aspdotnetstorefront.com
-- For details on this license please visit our homepage at the URL above.
-- THE ABOVE NOTICE MUST REMAIN INTACT.
-- ------------------------------------------------------------------------------------------

-- ------------------------------------------------------------------------------------------
-- Database Upgrade Script:
-- AspDotNetStorefront Version 10.0.0 to Latest Microsoft SQL Server 2008 or higher
-- ------------------------------------------------------------------------------------------

/*********** ASPDOTNETSTOREFRONT v10.0.0 to Latest *******************/
/*                                                                */
/*                                                                */
/* BACKUP YOUR EXISTING DATABASE BEFORE RUNNING THIS SCRIPT!!     */
/* BACKUP YOUR EXISTING DATABASE BEFORE RUNNING THIS SCRIPT!!     */
/* BACKUP YOUR EXISTING DATABASE BEFORE RUNNING THIS SCRIPT!!     */
/* BACKUP YOUR EXISTING DATABASE BEFORE RUNNING THIS SCRIPT!!     */
/*                                                                */
/*                                                                */
/* ************************************************************** */

IF NOT EXISTS(SELECT Name from AppConfig WHERE Name = 'RelatedProducts.NumberDisplayed')
BEGIN
	INSERT INTO [dbo].[AppConfig] ([StoreID], [Name], [Description], [ConfigValue], [ValueType], [AllowableValues], [GroupName], [SuperOnly], [Hidden], [CreatedOn], [UpdatedOn]) VALUES (0, N'RelatedProducts.NumberDisplayed', N'The maximum number of related products displayed at the bottom of product pages.', N'4', N'integer', NULL, N'DISPLAY', 0, 0, GETDATE(), GETDATE())
END
GO

--Cleanup
DELETE FROM AppConfig WHERE Name = 'Bongo.Extend.Enabled'
DELETE FROM AppConfig WHERE Name = 'Bongo.Extend.Script'
DELETE FROM AppConfig WHERE Name = 'PayPal.Express.UseIntegratedCheckout'
DELETE FROM AppConfig WHERE Name = 'PayPal.Express.IntegratedCheckout.SandboxURL'
DELETE FROM AppConfig WHERE Name = 'PayPal.Express.IntegratedCheckout.LiveURL'

--Recreate PayPal URL AppConfigs with their new URLs
DELETE FROM AppConfig WHERE Name = 'PayPal.Express.LiveURL'
INSERT INTO [dbo].[AppConfig] ([StoreID], [Name], [Description], [ConfigValue], [ValueType], [AllowableValues], [GroupName], [SuperOnly], [Hidden], [CreatedOn], [UpdatedOn])
VALUES (0, N'PayPal.Express.LiveURL', N'PayPal Express In-Context Checkout Live Site URL. Do not change this value without consulting PayPal support.', N'https://www.paypal.com/checkoutnow', NULL, NULL, N'GATEWAY', 1, 0, GETDATE(), GETDATE())

DELETE FROM AppConfig WHERE Name = 'PayPal.Express.SandboxURL'
INSERT INTO [dbo].[AppConfig] ([StoreID], [Name], [Description], [ConfigValue], [ValueType], [AllowableValues], [GroupName], [SuperOnly], [Hidden], [CreatedOn], [UpdatedOn])
VALUES (0, N'PayPal.Express.SandboxURL', N'PayPal Express In-Context Checkout Sandbox Site URL. Do not change this value without consulting PayPal support.', N'https://www.sandbox.paypal.com/checkoutnow', NULL, NULL, N'GATEWAY', 1, 0, GETDATE(), GETDATE())

--Update the PayPal callback url for IPN
UPDATE AppConfig
SET ConfigValue = 'paypalnotifications'
WHERE Name = 'PayPal.NotificationURL'
AND ConfigValue = 'paypalnotification.aspx'

UPDATE Store
SET StagingURI = ''
WHERE StagingURI = 'staging.mystore.com'

UPDATE Store
SET ProductionURI = 'www.samplesitename.com'
WHERE ProductionURI = 'www.mystore.com'
GO
/*********** End 10.0.1 Changes *********************/


/*********** Begin 10.0.2 Changes *********************/
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[aspdnsf_updCustomer]'))
    DROP PROC [dbo].[aspdnsf_updCustomer]
GO
create proc [dbo].[aspdnsf_updCustomer]
	@CustomerID int,
	@CustomerLevelID int = null,
	@Email nvarchar(100) = null,
	@Password nvarchar(250) = null,
	@SaltKey int = null,
	@Gender nvarchar(1) = null,
	@FirstName nvarchar(100) = null,
	@LastName nvarchar(100) = null,
	@Notes nvarchar(max) = null,
	@SkinID int = null,
	@Phone nvarchar(25) = null,
	@AffiliateID int = null,
	@Referrer nvarchar(max) = null,
	@CouponCode nvarchar(50) = null,
	@OkToEmail tinyint = null,
	@IsAdmin tinyint = null,
	@BillingEqualsShipping tinyint = null,
	@LastIPAddress varchar(40) = null,
	@OrderNotes nvarchar(max) = null,
	@RTShipRequest nvarchar(max) = null,
	@RTShipResponse nvarchar(max) = null,
	@OrderOptions nvarchar(max) = null,
	@LocaleSetting nvarchar(10) = null,
	@MicroPayBalance money = null,
	@RecurringShippingMethodID int = null,
	@RecurringShippingMethod nvarchar(100) = null,
	@BillingAddressID int = null,
	@ShippingAddressID int = null,
	@ExtensionData nvarchar(max) = null,
	@FinalizationData nvarchar(max) = null,
	@Deleted tinyint = null,
	@Over13Checked tinyint = null,
	@CurrencySetting nvarchar(10) = null,
	@VATSetting int = null,
	@VATRegistrationID nvarchar(100) = null,
	@StoreCCInDB tinyint = null,
	@IsRegistered tinyint = null,
	@LockedUntil datetime = null,
	@AdminCanViewCC tinyint = null,
	@BadLogin smallint = 0, --only pass -1 = null, 0 = null, or 1: -1 clears the field = null, 0 does nothing = null, 1 increments the field by one
	@Active tinyint = null,
	@PwdChangeRequired tinyint = null,
	@RegisterDate datetime = null,
	@RequestedPaymentMethod  nvarchar(100) = null,
	@ClearSavedCCNumbers bit = 0,
	@StoreID	int = null
AS
SET NOCOUNT ON

DECLARE @OldPwd nvarchar(100), @OldSaltKey int
SELECT @OldPwd = Password, @OldSaltKey = SaltKey FROM dbo.Customer WHERE CustomerID = @CustomerID

UPDATE dbo.Customer
SET
	CustomerLevelID = COALESCE(@CustomerLevelID, CustomerLevelID),
	RegisterDate = COALESCE(@RegisterDate, RegisterDate),
	Email = COALESCE(@Email, Email),
	Password = COALESCE(@Password, Password),
	SaltKey = COALESCE(@SaltKey, SaltKey),
	Gender = COALESCE(@Gender, Gender),
	FirstName = COALESCE(@FirstName, FirstName),
	LastName = COALESCE(@LastName, LastName),
	Notes = COALESCE(@Notes, Notes),
	SkinID = COALESCE(@SkinID, SkinID),
	Phone = COALESCE(@Phone, Phone),
	AffiliateID = COALESCE(@AffiliateID, AffiliateID),
	Referrer = COALESCE(@Referrer, Referrer),
	CouponCode = COALESCE(@CouponCode, CouponCode),
	OkToEmail = COALESCE(@OkToEmail, OkToEmail),
	IsAdmin = COALESCE(@IsAdmin, IsAdmin),
	BillingEqualsShipping = COALESCE(@BillingEqualsShipping, BillingEqualsShipping),
	LastIPAddress = COALESCE(@LastIPAddress, LastIPAddress),
	OrderNotes = COALESCE(@OrderNotes, OrderNotes),
	RTShipRequest = COALESCE(@RTShipRequest, RTShipRequest),
	RTShipResponse = COALESCE(@RTShipResponse, RTShipResponse),
	OrderOptions = COALESCE(@OrderOptions, OrderOptions),
	LocaleSetting = COALESCE(@LocaleSetting, LocaleSetting),
	MicroPayBalance = COALESCE(@MicroPayBalance, MicroPayBalance),
	RecurringShippingMethodID = COALESCE(@RecurringShippingMethodID, RecurringShippingMethodID),
	RecurringShippingMethod = COALESCE(@RecurringShippingMethod, RecurringShippingMethod),
	BillingAddressID = COALESCE(@BillingAddressID, BillingAddressID),
	ShippingAddressID = COALESCE(@ShippingAddressID, ShippingAddressID),
	ExtensionData = COALESCE(@ExtensionData, ExtensionData),
	FinalizationData = COALESCE(@FinalizationData, FinalizationData),
	Deleted = COALESCE(@Deleted, Deleted),
	Over13Checked = COALESCE(@Over13Checked, Over13Checked),
	CurrencySetting = COALESCE(@CurrencySetting, CurrencySetting),
	VATSetting = COALESCE(@VATSetting, VATSetting),
	VATRegistrationID = COALESCE(@VATRegistrationID, VATRegistrationID),
	StoreCCInDB = COALESCE(@StoreCCInDB, StoreCCInDB),
	IsRegistered = COALESCE(@IsRegistered, IsRegistered),
	LockedUntil = COALESCE(@LockedUntil, LockedUntil),
	AdminCanViewCC = COALESCE(@AdminCanViewCC, AdminCanViewCC),
	PwdChanged = CASE
		WHEN @OldPwd <> @Password AND @Password IS NOT NULL THEN getdate()
		ELSE PwdChanged END,
	BadLoginCount = CASE @BadLogin
		WHEN -1 THEN 0
		ELSE BadLoginCount + isnull(@BadLogin, 0) END,
	LastBadLogin = CASE @BadLogin
		WHEN -1 THEN NULL
		WHEN 1 THEN getdate()
		ELSE LastBadLogin END,
	Active = COALESCE(@Active, Active),
	PwdChangeRequired = COALESCE(@PwdChangeRequired, PwdChangeRequired),
	RequestedPaymentMethod = COALESCE(@RequestedPaymentMethod, RequestedPaymentMethod),
	StoreID = COALESCE(@StoreID, StoreID)
WHERE
	CustomerID = @CustomerID
	-- Only update the row if any fields will actually change
	AND (
		@CustomerLevelID is not null and (CustomerLevelID is null or CustomerLevelID != @CustomerLevelID)
		OR @RegisterDate is not null and (RegisterDate is null or RegisterDate != @RegisterDate)
		OR @Email is not null and (Email is null or Email != @Email)
		OR @Password is not null and ([Password] is null or [Password] != @Password)
		OR @SaltKey is not null and (SaltKey is null or SaltKey != @SaltKey)
		OR @Gender is not null and (Gender is null or Gender != @Gender)
		OR @FirstName is not null and (FirstName is null or FirstName != @FirstName)
		OR @LastName is not null and (LastName is null or LastName != @LastName)
		OR @Notes is not null and (Notes is null or Notes != @Notes)
		OR @SkinID is not null and (SkinID is null or SkinID != @SkinID)
		OR @Phone is not null and (Phone is null or Phone != @Phone)
		OR @AffiliateID is not null and (AffiliateID is null or AffiliateID != @AffiliateID)
		OR @Referrer is not null and (Referrer is null or Referrer != @Referrer)
		OR @CouponCode is not null and (CouponCode is null or CouponCode != @CouponCode)
		OR @OkToEmail is not null and (OkToEmail is null or OkToEmail != @OkToEmail)
		OR @IsAdmin is not null and (IsAdmin is null or IsAdmin != @IsAdmin)
		OR @BillingEqualsShipping is not null and (BillingEqualsShipping is null or BillingEqualsShipping != @BillingEqualsShipping)
		OR @LastIPAddress is not null and (LastIPAddress is null or LastIPAddress != @LastIPAddress)
		OR @OrderNotes is not null and (OrderNotes is null or OrderNotes != @OrderNotes)
		OR @RTShipRequest is not null and (RTShipRequest is null or RTShipRequest != @RTShipRequest)
		OR @RTShipResponse is not null and (RTShipResponse is null or RTShipResponse != @RTShipResponse)
		OR @OrderOptions is not null and (OrderOptions is null or OrderOptions != @OrderOptions)
		OR @LocaleSetting is not null and (LocaleSetting is null or LocaleSetting != @LocaleSetting)
		OR @MicroPayBalance is not null and (MicroPayBalance is null or MicroPayBalance != @MicroPayBalance)
		OR @RecurringShippingMethodID is not null and (RecurringShippingMethodID is null or RecurringShippingMethodID != @RecurringShippingMethodID)
		OR @RecurringShippingMethod is not null and (RecurringShippingMethod is null or RecurringShippingMethod != @RecurringShippingMethod)
		OR @BillingAddressID is not null and (BillingAddressID is null or BillingAddressID != @BillingAddressID)
		OR @ShippingAddressID is not null and (ShippingAddressID is null or ShippingAddressID != @ShippingAddressID)
		OR @ExtensionData is not null and (ExtensionData is null or ExtensionData != @ExtensionData)
		OR @FinalizationData is not null and (FinalizationData is null or FinalizationData != @FinalizationData)
		OR @Deleted is not null and (Deleted is null or Deleted != @Deleted)
		OR @Over13Checked is not null and (Over13Checked is null or Over13Checked != @Over13Checked)
		OR @CurrencySetting is not null and (CurrencySetting is null or CurrencySetting != @CurrencySetting)
		OR @VATSetting is not null and (VATSetting is null or VATSetting != @VATSetting)
		OR @VATRegistrationID is not null and (VATRegistrationID is null or VATRegistrationID != @VATRegistrationID)
		OR @StoreCCInDB is not null and (StoreCCInDB is null or StoreCCInDB != @StoreCCInDB)
		OR @IsRegistered is not null and (IsRegistered is null or IsRegistered != @IsRegistered)
		OR @LockedUntil is not null and (LockedUntil is null or LockedUntil != @LockedUntil)
		OR @AdminCanViewCC is not null and (AdminCanViewCC is null or AdminCanViewCC != @AdminCanViewCC)
		OR @Password is not null and (@Password != @OldPwd)
		OR @BadLogin != 0
		OR @PwdChangeRequired is not null and (PwdChangeRequired is null or PwdChangeRequired != @PwdChangeRequired)
		OR @RequestedPaymentMethod is not null and (RequestedPaymentMethod is null or RequestedPaymentMethod != @RequestedPaymentMethod)
		OR @StoreID is not null and (StoreID is null or StoreID != @StoreID)
	)

IF @OldPwd <> @Password and @OldSaltKey <> 0
	INSERT dbo.PasswordLog (CustomerID, OldPwd, SaltKey, ChangeDt)
	VALUES (@CustomerID, @OldPwd, @OldSaltKey, getdate())

IF NULLIF(@ClearSavedCCNumbers, 0) = 1
BEGIN
	UPDATE Address
	SET	CardNumber = NULL,
		CardExpirationMonth = NULL,
		CardExpirationYear = NULL,
		CardStartDate = NULL,
		CardIssueNumber = NULL
	WHERE CustomerID = @CustomerID

	UPDATE Orders
	SET	CardNumber = NULL,
		CardExpirationMonth = NULL,
		CardExpirationYear = NULL,
		CardStartDate = NULL,
		CardIssueNumber = NULL
	WHERE CustomerID = @CustomerID
END
GO
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[aspdnsf_updCustomerByEmail]'))
    DROP PROC [dbo].[aspdnsf_updCustomerByEmail]
GO
create proc [dbo].[aspdnsf_updCustomerByEmail]
    @Email nvarchar(100),
    @CustomerLevelID int = null,
    @Password nvarchar(250) = null,
    @SaltKey int = null,
    @Gender nvarchar(1) = null,
    @FirstName nvarchar(100) = null,
    @LastName nvarchar(100) = null,
    @Notes nvarchar(max) = null,
    @SkinID int = null,
    @Phone nvarchar(25) = null,
    @AffiliateID int = null,
    @Referrer nvarchar(max) = null,
    @CouponCode nvarchar(50) = null,
    @OkToEmail tinyint = null,
    @IsAdmin tinyint = null,
    @BillingEqualsShipping tinyint = null,
    @LastIPAddress varchar(40) = null,
    @OrderNotes nvarchar(max) = null,
    @RTShipRequest nvarchar(max) = null,
    @RTShipResponse nvarchar(max) = null,
    @OrderOptions nvarchar(max) = null,
    @LocaleSetting nvarchar(10) = null,
    @MicroPayBalance money = null,
    @RecurringShippingMethodID int = null,
    @RecurringShippingMethod nvarchar(100) = null,
    @BillingAddressID int = null,
    @ShippingAddressID int = null,
    @ExtensionData nvarchar(max) = null,
    @FinalizationData nvarchar(max) = null,
    @Deleted tinyint = null,
    @Over13Checked tinyint = null,
    @CurrencySetting nvarchar(10) = null,
    @VATSetting int = null,
    @VATRegistrationID nvarchar(100) = null,
    @StoreCCInDB tinyint = null,
    @IsRegistered tinyint = null,
    @LockedUntil datetime = null,
    @AdminCanViewCC tinyint = null,
    @BadLogin smallint = 0 , --only pass -1 = null, 0 = null, or 1: -1 clears the field = null, 0 does nothing = null, 1 increments the field by one
    @Active tinyint = null,
    @PwdChangeRequired tinyint = null,
    @RequestedPaymentMethod  nvarchar(100) = null

AS
SET NOCOUNT ON

DECLARE @CustomerID int, @OldPwd nvarchar(100), @IsAdminCust tinyint, @OldSaltKey int

SELECT @CustomerID = CustomerID , @OldPwd = Password, @IsAdminCust = IsAdmin, @OldSaltKey = Saltkey FROM dbo.Customer WHERE Email = @Email


UPDATE dbo.Customer
SET
    CustomerLevelID = COALESCE(@CustomerLevelID, CustomerLevelID),
    Email = COALESCE(@Email, Email),
    Password = COALESCE(@Password, Password),
    SaltKey = COALESCE(@SaltKey, SaltKey),
    Gender = COALESCE(@Gender, Gender),
    FirstName = COALESCE(@FirstName, FirstName),
    LastName = COALESCE(@LastName, LastName),
    Notes = COALESCE(@Notes, Notes),
    SkinID = COALESCE(@SkinID, SkinID),
    Phone = COALESCE(@Phone, Phone),
    AffiliateID = COALESCE(@AffiliateID, AffiliateID),
    Referrer = COALESCE(@Referrer, Referrer),
    CouponCode = COALESCE(@CouponCode, CouponCode),
    OkToEmail = COALESCE(@OkToEmail, OkToEmail),
    IsAdmin = COALESCE(@IsAdmin, IsAdmin),
    BillingEqualsShipping = COALESCE(@BillingEqualsShipping, BillingEqualsShipping),
    LastIPAddress = COALESCE(@LastIPAddress, LastIPAddress),
    OrderNotes = COALESCE(@OrderNotes, OrderNotes),
    RTShipRequest = COALESCE(@RTShipRequest, RTShipRequest),
    RTShipResponse = COALESCE(@RTShipResponse, RTShipResponse),
    OrderOptions = COALESCE(@OrderOptions, OrderOptions),
    LocaleSetting = COALESCE(@LocaleSetting, LocaleSetting),
    MicroPayBalance = COALESCE(@MicroPayBalance, MicroPayBalance),
    RecurringShippingMethodID = COALESCE(@RecurringShippingMethodID, RecurringShippingMethodID),
    RecurringShippingMethod = COALESCE(@RecurringShippingMethod, RecurringShippingMethod),
    BillingAddressID = COALESCE(@BillingAddressID, BillingAddressID),
    ShippingAddressID = COALESCE(@ShippingAddressID, ShippingAddressID),
    ExtensionData = COALESCE(@ExtensionData, ExtensionData),
    FinalizationData = COALESCE(@FinalizationData, FinalizationData),
    Deleted = COALESCE(@Deleted, Deleted),
    Over13Checked = COALESCE(@Over13Checked, Over13Checked),
    CurrencySetting = COALESCE(@CurrencySetting, CurrencySetting),
    VATSetting = COALESCE(@VATSetting, VATSetting),
    VATRegistrationID = COALESCE(@VATRegistrationID, VATRegistrationID),
    StoreCCInDB = COALESCE(@StoreCCInDB, StoreCCInDB),
    IsRegistered = COALESCE(@IsRegistered, IsRegistered),
    LockedUntil = COALESCE(@LockedUntil, LockedUntil),
    AdminCanViewCC = COALESCE(@AdminCanViewCC, AdminCanViewCC),
    PwdChanged = case when @OldPwd <> @Password and @Password is not null then getdate() else PwdChanged end,
    BadLoginCount = case @BadLogin when -1 then 0 else BadLoginCount + @BadLogin end,
    LastBadLogin = case @BadLogin when -1 then null when 1 then getdate() else LastBadLogin end,
    Active = COALESCE(@Active, Active),
    PwdChangeRequired = COALESCE(@PwdChangeRequired, PwdChangeRequired),
    RequestedPaymentMethod = COALESCE(@RequestedPaymentMethod, RequestedPaymentMethod)
WHERE Email = @Email

IF @OldPwd <> @Password and @OldSaltKey <> 0
    INSERT dbo.PasswordLog (CustomerID, OldPwd, SaltKey, ChangeDt)
    VALUES (@CustomerID, @OldPwd, @OldSaltKey, getdate())
GO

-- Update state from ID list to abbreviation list in AppConfig setting 'RTShipping.LocalpickupRestrictionStates'
declare @IDs nvarchar(max)
declare @storeID int
declare @states nvarchar(max)

IF EXISTS (SELECT * FROM AppConfig WHERE Name = 'RTShipping.LocalpickupRestrictionStates')
BEGIN
	DECLARE stateid_cursor CURSOR FOR SELECT ConfigValue, StoreID FROM AppConfig WHERE Name = 'RTShipping.LocalpickupRestrictionStates'
	OPEN stateid_cursor
	FETCH NEXT FROM stateid_cursor INTO @IDs, @storeID

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF ISNUMERIC((SELECT TOP 1 Items from dbo.Split(@IDs, ','))) > 0 -- check if first element is numeric; if it succeeds, conversion needed, if not, state abbreviation is already used
		BEGIN
			SELECT @states = COALESCE(@states + ',', '') + Abbreviation from State join dbo.Split(@IDs, ',') IDs on IDs.Items = State.StateID
			UPDATE AppConfig SET ConfigValue = @states where Name = 'RTShipping.LocalpickupRestrictionStates' and StoreID = @storeID
			set @states = null
		END
		FETCH NEXT FROM stateid_cursor INTO @IDs, @storeID
	END
END
CLOSE stateid_cursor
DEALLOCATE stateid_cursor

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[aspdnsf_Get404Suggestions]'))
    DROP PROC [dbo].[aspdnsf_Get404Suggestions]
GO
CREATE PROC [dbo].[aspdnsf_Get404Suggestions]
	@storeId INT = 0
AS
BEGIN
    SET NOCOUNT ON

	DECLARE @suggestionTypes VARCHAR(100) = (SELECT TOP 1 CASE ConfigValue WHEN '' THEN 'product, category, manufacturer, section, topic' ELSE ConfigValue END FROM [dbo].[AppConfig] WITH(NOLOCK) WHERE [Name] = '404.VisibleSuggestions' AND (StoreID=@storeId OR StoreID=0) ORDER BY StoreID desc)
	DECLARE @hideInventoryLevel INT = (SELECT TOP 1 ConfigValue FROM [dbo].[AppConfig] WITH(NOLOCK) WHERE [Name] = 'HideProductsWithLessThanThisInventoryLevel' AND (StoreID=@storeId OR StoreID=0) ORDER BY StoreID desc)
	
	DECLARE @filterProducts BIT = (SELECT ConfigValue FROM [dbo].[GlobalConfig] WITH(NOLOCK) WHERE [Name] = 'AllowProductFiltering')
	DECLARE @filterTopics BIT = (SELECT ConfigValue FROM [dbo].[GlobalConfig] WITH(NOLOCK) WHERE [Name] = 'AllowTopicFiltering')
	DECLARE @filterEntities BIT = (SELECT ConfigValue FROM [dbo].[GlobalConfig] WITH(NOLOCK) WHERE [Name] = 'AllowEntityFiltering')

	CREATE TABLE #UnfilteredEntities 
	(
        Id INT NOT NULL,
		ObjectType VARCHAR(100) NOT NULL,
		Name NVARCHAR(400) NOT NULL,
		[Description] NVARCHAR(MAX)
	)

	--Products
	IF @suggestionTypes LIKE '%product%'
	BEGIN
		INSERT INTO #UnfilteredEntities(Id, ObjectType, Name, [Description])
		SELECT p.ProductID as id,
			'product', 
			p.Name, 
			p.[Description]
		FROM Product p WITH (NOLOCK)
			INNER JOIN ProductVariant pv ON p.ProductID = pv.ProductID AND pv.IsDefault = 1
			LEFT JOIN (SELECT VariantID, SUM(Quan) AS Inventory
						FROM Inventory
						GROUP BY VariantID) i on pv.VariantID = i.VariantID
			LEFT JOIN ProductStore ps ON p.ProductID = ps.ProductID AND (@filterProducts = 0 OR ps.StoreID = @storeId)
		WHERE p.Deleted = 0
			AND p.Published = 1
			AND pv.Deleted = 0
			AND pv.Published = 1
			AND (CASE p.TrackInventoryBySizeAndColor 
					WHEN 1 THEN ISNULL(i.Inventory, 0) 
					ELSE pv.Inventory 
				END >= @hideInventoryLevel or @hideInventoryLevel = -1)
	END

	--Topics
	IF @suggestionTypes LIKE '%topic%'
	BEGIN
		INSERT INTO #UnfilteredEntities(Id, ObjectType, Name, [Description])
		SELECT t.TopicID as id,
			'topic', 
			t.Name, 
			t.Title
		FROM Topic t
		WHERE t.Published = 1
			AND t.ShowInSiteMap = 1
			AND (@filterTopics = 0 OR t.StoreID = @storeId)
	END

	--Categories
	IF @suggestionTypes LIKE '%category%'
	BEGIN
        INSERT INTO #UnfilteredEntities(Id, ObjectType, Name, [Description])
            SELECT c.CategoryID,
            'category', 
			c.Name,
			c.[Description]
		FROM Category c
		WHERE c.Published = 1
			AND c.Deleted = 0
			AND (@filterEntities = 0 OR (c.CategoryID IN (SELECT DISTINCT EntityID FROM EntityStore WHERE EntityType = 'category' AND StoreID = @storeId)))
	END
	
	--Manufacturers
	IF @suggestionTypes LIKE '%manufacturer%'
	BEGIN
        INSERT INTO #UnfilteredEntities(Id, ObjectType, Name, [Description])
            SELECT m.ManufacturerID,
            'manufacturer', 
			m.Name,
			m.[Description]
		FROM Manufacturer m
		WHERE m.Published = 1
			AND m.Deleted = 0
			AND (@filterEntities = 0 OR (m.ManufacturerID IN (SELECT DISTINCT EntityID FROM EntityStore WHERE EntityType = 'manufacturer' AND StoreID = @storeId)))
	END
	
	--Manufacturers
	IF @suggestionTypes LIKE '%section%'
	BEGIN
        INSERT INTO #UnfilteredEntities(Id, ObjectType, Name, [Description])
            SELECT s.SectionID,
            'section', 
			s.Name,
			s.[Description]
		FROM Section s
		WHERE s.Published = 1
			AND s.Deleted = 0
			AND (@filterEntities = 0 OR (s.SectionID IN (SELECT DISTINCT EntityID FROM EntityStore WHERE EntityType = 'section' AND StoreID = @storeId)))
	END

	SELECT * FROM #UnfilteredEntities
END
GO

-- Add new client resource management AppConfig
if(not exists (select Name from AppConfig where Name='ClientResources.Script.DeferredRenderingEnabled'))
begin
	insert into AppConfig(Name, ConfigValue, Description, GroupName, ValueType)
	values('ClientResources.Script.DeferredRenderingEnabled', 'false', 'Set to true to defer rendering of opted-in scripts to the end of the HTML body tag' , 'CLIENT RESOURCES', 'boolean')
end

-- Insert ShippingCalculationStore records for each missing Store, and only if there is a single selected record in ShippingCalculation
insert into ShippingCalculationStore(StoreID, ShippingCalculationId)
select StoreID, 
	(select ShippingCalculationID from ShippingCalculation where selected = 1) 
from Store where not(StoreID in (select StoreID from ShippingCalculationStore)) 
	and (select count(*) from ShippingCalculation where Selected = 1) = 1
go

-- Fix upsell products sproc not handling ML-encoded size/color
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[aspdnsf_GetUpsellProducts]'))
	DROP PROC [dbo].[aspdnsf_GetUpsellProducts]
GO

CREATE PROCEDURE [dbo].[aspdnsf_GetUpsellProducts]
	@productID			INT,
	@customerlevelID	INT,
	@invFilter			INT,
	@storeID			INT = 1,
	@filterProduct		BIT = 0
AS
BEGIN
	SET NOCOUNT ON;

   DECLARE @UpsellProducts VARCHAR(8000),
		   @UpsellProductDiscPct MONEY

    SELECT @UpsellProducts = REPLACE(CAST(UpsellProducts AS VARCHAR(8000)), ' ', '')
	     , @UpsellProductDiscPct = UpsellProductDiscountPercentage
	  FROM dbo.product WITH (NOLOCK) WHERE productid = @productid

	SELECT 1-(@UpsellProductDiscPct/100) UpsellDiscMultiplier
		 , p.*
		 , pv.VariantID
		 , pv.Price
		 , ISNULL(pv.SalePrice, 0) SalePrice
		 , ISNULL(pv.SkuSuffix, '') AS SkuSuffix
		 , ISNULL(pv.ManufacturerPartNumber, '') AS VariantManufacturerPartNumber
		 , ISNULL(pv.Dimensions, '') AS Dimensions
		 , pv.Weight
		 , ISNULL(pv.GTIN, '') AS GTIN
		 , pv.Condition
		 , ISNULL(pv.Points, 0) Points
		 , sp.Name SalesPromptName
		 , ISNULL(ep.price, 0) ExtendedPrice
		 , ProductManufacturer.ManufacturerID AS ProductManufacturerId
		 , Manufacturer.Name AS ProductManufacturerName
		 , Manufacturer.SEName AS ProductManufacturerSEName
      FROM dbo.product p WITH (NOLOCK)
      JOIN dbo.split(@UpsellProducts, ',') up ON p.productid = CAST(up.items AS INT)
 LEFT JOIN dbo.SalesPrompt sp WITH (NOLOCK) ON sp.SalesPromptID = p.SalesPromptID
      JOIN dbo.productvariant pv WITH (NOLOCK) ON pv.productid = CAST(up.items AS INT) AND pv.isdefault = 1 AND pv.Published = 1 AND pv.Deleted = 0
 LEFT JOIN dbo.ExtendedPrice ep WITH (NOLOCK) ON ep.VariantID = pv.VariantID AND ep.CustomerLevelID = @CustomerLevelID
      JOIN (SELECT p.ProductID
              FROM dbo.product p WITH (NOLOCK)
              JOIN dbo.split(@UpsellProducts, ',') rp ON p.productid = CAST(rp.items AS INT)
              JOIN (SELECT ProductID, SUM(Inventory) Inventory
			          FROM dbo.productvariant WITH (NOLOCK)
				  GROUP BY ProductID) pv ON p.ProductID = pv.ProductID
         LEFT JOIN (SELECT ProductID
				         , SUM(quan) inventory
				      FROM dbo.inventory i1 WITH (NOLOCK)
				      JOIN dbo.productvariant pv1 WITH (NOLOCK) ON pv1.variantid = i1.variantid
				      JOIN dbo.split(@UpsellProducts, ',') rp1 ON pv1.productid = CAST(rp1.items AS INT)
			      GROUP BY pv1.productid) i ON i.productid = p.productid
                WHERE CASE p.TrackInventoryBySizeAndColor WHEN 1 THEN ISNULL(i.inventory, 0) ELSE pv.inventory END >= @InvFilter
                   ) tp ON p.productid = tp.productid
		INNER JOIN (SELECT DISTINCT a.ProductID
		              FROM Product a WITH (NOLOCK)
				 LEFT JOIN ProductStore b WITH (NOLOCK) ON a.ProductID = b.ProductID WHERE (@filterProduct = 0 OR StoreID = @storeID)
				   ) ps ON p.ProductID = ps.ProductID
LEFT JOIN dbo.ProductManufacturer WITH (NOLOCK) ON tp.ProductID = ProductManufacturer.ProductID
LEFT JOIN dbo.Manufacturer WITH (NOLOCK) ON ProductManufacturer.ManufacturerID = Manufacturer.ManufacturerID
	WHERE p.Published = 1
	  AND p.Deleted = 0
	  AND p.IsCallToOrder = 0
	  AND pv.CustomerEntersPrice = 0
	  AND p.IsAKit = 0
	  AND ( NULLIF(pv.Sizes, '') IS NULL
			OR NOT EXISTS (
				SELECT *
				FROM dbo.ParseMlLocales(pv.Sizes) Sizes
				INNER JOIN LocaleSetting ON Sizes.Locale = LocaleSetting.Name
				WHERE [Value] IS NOT NULL))
	  AND ( NULLIF(pv.Colors, '') IS NULL
			OR NOT EXISTS (
				SELECT *
				FROM dbo.ParseMlLocales(pv.Colors) Colors
				INNER JOIN LocaleSetting ON Colors.Locale = LocaleSetting.Name
				WHERE [Value] IS NOT NULL))
	  AND p.productid != @productid
END
GO

-- Update aspdnsf_getOrder to remove Notes field collision
IF EXISTS (select * from dbo.sysobjects where id = object_id(N'[dbo].[aspdnsf_getOrder]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    drop proc [dbo].[aspdnsf_getOrder]
GO

CREATE proc [dbo].[aspdnsf_getOrder]
	@ordernumber int
as
set nocount on
select
	o.OrderNumber,
	o.OrderGUID,
	o.ParentOrderNumber,
	o.StoreVersion,
	o.QuoteCheckout,
	o.IsNew,
	o.ShippedOn,
	o.CustomerID,
	o.CustomerGUID,
	o.Referrer,
	o.SkinID,
	o.LastName,
	o.FirstName,
	o.Email,
	o.Notes,
	o.BillingEqualsShipping,
	o.BillingLastName,
	o.BillingFirstName,
	o.BillingCompany,
	o.BillingAddress1,
	o.BillingAddress2,
	o.BillingSuite,
	o.BillingCity,
	o.BillingState,
	o.BillingZip,
	o.BillingCountry,
	o.BillingPhone,
	o.ShippingLastName,
	o.ShippingFirstName,
	o.ShippingCompany,
	o.ShippingResidenceType,
	o.ShippingAddress1,
	o.ShippingAddress2,
	o.ShippingSuite,
	o.ShippingCity,
	o.ShippingState,
	o.ShippingZip,
	o.ShippingCountry,
	o.ShippingMethodID,
	o.ShippingMethod,
	o.ShippingPhone,
	o.ShippingCalculationID,
	o.Phone,
	o.RegisterDate,
	o.AffiliateID,
	o.CouponCode,
	o.CouponType,
	o.CouponDescription,
	o.CouponDiscountAmount,
	o.CouponDiscountPercent,
	o.CouponIncludesFreeShipping,
	o.OkToEmail,
	o.Deleted,
	o.CardType,
	o.CardName,
	o.CardNumber,
	o.CardExpirationMonth,
	o.CardExpirationYear,
	o.OrderSubtotal,
	o.OrderTax,
	o.OrderShippingCosts,
	o.OrderTotal,
	o.PaymentGateway,
	o.AuthorizationCode,
	o.AuthorizationResult,
	o.AuthorizationPNREF,
	o.TransactionCommand,
	o.OrderDate,
	o.LevelID,
	o.LevelName,
	o.LevelDiscountPercent,
	o.LevelDiscountAmount,
	o.LevelHasFreeShipping,
	o.LevelAllowsQuantityDiscounts,
	o.LevelHasNoTax,
	o.LevelAllowsCoupons,
	o.LevelDiscountsApplyToExtendedPrices,
	o.LastIPAddress,
	o.PaymentMethod,
	o.OrderNotes,
	o.PONumber,
	o.DownloadEmailSentOn,
	o.ReceiptEmailSentOn,
	o.DistributorEmailSentOn,
	o.ShippingTrackingNumber,
	o.ShippedVIA,
	o.CustomerServiceNotes,
	o.RTShipRequest,
	o.RTShipResponse,
	o.TransactionState,
	o.AVSResult,
	o.CaptureTXCommand,
	o.CaptureTXResult,
	o.VoidTXCommand,
	o.VoidTXResult,
	o.RefundTXCommand,
	o.RefundTXResult,
	o.CardinalLookupResult,
	o.CardinalAuthenticateResult,
	o.CardinalGatewayParms,
	o.AffiliateCommissionRecorded,
	o.OrderOptions,
	o.OrderWeight,
	o.CarrierReportedRate,
	o.CarrierReportedWeight,
	o.LocaleSetting,
	o.FinalizationData,
	o.ExtensionData,
	o.AlreadyConfirmed,
	o.CartType,
	o.THUB_POSTED_TO_ACCOUNTING,
	o.THUB_POSTED_DATE,
	o.THUB_ACCOUNTING_REF,
	o.Last4,
	o.ReadyToShip,
	o.IsPrinted,
	o.AuthorizedOn,
	o.CapturedOn,
	o.RefundedOn,
	o.VoidedOn,
	o.EditedOn,
	o.InventoryWasReduced,
	o.MaxMindFraudScore,
	o.MaxMindDetails,
	o.CardStartDate,
	o.CardIssueNumber,
	o.TransactionType,
	o.Crypt,
	o.VATRegistrationID,
	o.FraudedOn,
	o.RefundReason,
	o.AuthorizationPNREF as TransactionID,
	o.RecurringSubscriptionID,
	o.RelatedOrderNumber,
	o.ReceiptHtml,
	os.ShoppingCartRecID,
	os.IsTaxable,
	os.IsShipSeparately,
	os.IsDownload,
	os.DownloadLocation,
	os.FreeShipping,
	os.DistributorID,
	os.ShippingDetail,
	os.TaxClassID,
	os.TaxRate,
	os.Notes as CartNotes,
	os.CustomerEntersPrice,
	os.ProductID,
	os.VariantID,
	os.Quantity,
	os.ChosenColor,
	os.ChosenColorSKUModifier,
	os.ChosenSize,
	os.ChosenSizeSKUModifier,
	os.TextOption,
	os.SizeOptionPrompt,
	os.ColorOptionPrompt,
	os.TextOptionPrompt,
	os.CustomerEntersPricePrompt,
	os.OrderedProductQuantityDiscountID,
	os.OrderedProductQuantityDiscountName,
	os.OrderedProductQuantityDiscountPercent,
	os.OrderedProductName,
	os.OrderedProductVariantName,
	os.OrderedProductSKU,
	os.OrderedProductManufacturerPartNumber,
	os.OrderedProductPrice,
	os.OrderedProductWeight,
	os.OrderedProductPrice,
	os.ShippingMethodID,
	os.ShippingMethodID CartItemShippingMethodID,
	os.ShippingMethod CartItemShippingMethod,
	os.ShippingAddressID,
	os.IsAKit
from Orders o with (nolock)
	left join orders_ShoppingCart os with (nolock) ON os.OrderNumber = o.OrderNumber
where o.OrderNumber = @ordernumber
order by os.ShippingAddressID
GO

-- Moves Profile cleanup to monthly maintenance sproc
IF EXISTS (select * from dbo.sysobjects where id = object_id(N'[dbo].[aspdnsf_MonthlyMaintenance]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
    drop proc [dbo].[aspdnsf_MonthlyMaintenance]
GO

create proc [dbo].[aspdnsf_MonthlyMaintenance]
	@purgeAnonCustomers tinyint = 1,
	@cleanShoppingCartsOlderThan smallint = 30, -- set to 0 to disable erasing
	@cleanWishListsOlderThan smallint = 30, -- set to 0 to disable erasing
	@eraseCCFromAddresses tinyint = 1, -- except those used for recurring billing items!
	@clearProductViewsOrderThan smallint = 180,
	@eraseCCFromOrdersOlderThan smallint = 30, -- set to 0 to disable erasing
	@defragIndexes tinyint = 0,
	@updateStats tinyint = 0,
	@purgeDeletedRecords tinyint = 0,-- Purges records in all tables with a deleted flag set to 1
	@removeRTShippingDataOlderThan smallint = 30, -- set to 0 to disable erasing
	@clearSearchLogOlderThan smallint = 30,-- set to 0 to disable erasing
	@cleanOrphanedLocalizedNames tinyint = 0,
	@cleanupSecurityLog tinyint = 0,
	@clearProfilesOlderThan smallint = 0
	as
begin
	set nocount on

	-- Clear out failed transactions older than 2 months:
	delete from FailedTransaction where OrderDate < dateadd(mm,-2,getdate());

	-- Clear out old tx info, not longer needed:
	update orders set TransactionCommand=NULL, AuthorizationResult=NULL, VoidTXCommand=NULL, VoidTXResult=NULL, CaptureTXCommand=NULL, CaptureTXResult=NULL, RefundTXCommand=NULL, RefundTXResult=NULL where orderdate < dateadd(mm,-2,getdate());

	-- Clean up Security Log entries that are more than 1 year old
	if @cleanupSecurityLog = 1
	begin
		delete from SecurityLog where ActionDate < dateadd(year,-1,getdate());
	end

	-- Clean up data in the LocalizedObjectName table for locales that no longer exist
	if @cleanOrphanedLocalizedNames = 1
	begin
		delete from LocalizedObjectName where LocaleId not in (select LocaleSettingID from LocaleSetting);
	end

	-- clean up all carts (don't touch recurring items or wishlist items however):
	if @cleanShoppingCartsOlderThan <> 0
	begin
		delete dbo.kitcart where (CartType=0 or CartType=101) and CreatedOn < dateadd(d,-@cleanShoppingCartsOlderThan,getdate());
		delete dbo.ShoppingCart where (CartType=0 or CartType=101) and CreatedOn < dateadd(d,-@cleanShoppingCartsOlderThan,getdate());
	end

	if @cleanWishListsOlderThan <> 0
	begin
		delete dbo.kitcart where CartType=1 and CreatedOn < dateadd(d,-@cleanWishListsOlderThan,getdate());
		delete dbo.ShoppingCart where CartType=1 and CreatedOn < dateadd(d,-@cleanWishListsOlderThan,getdate());
	end

	-- purge anon customers:
	if @purgeAnonCustomers = 1
	begin
		-- clean out CIM profiles for orders that were not completed
		delete dbo.CIM_AddressPaymentProfileMap where customerid not in (select customerid from dbo.customer with (NOLOCK) where IsRegistered=1)

		delete dbo.customer where 
			IsRegistered=0 and IsAdmin = 0
			and customerid not in (select customerid from dbo.ShoppingCart with (NOLOCK))
			and customerid not in (select customerid from dbo.kitcart with (NOLOCK))
			and customerid not in (select customerid from dbo.orders with (NOLOCK))
			and customerid not in (select customerid from dbo.rating with (NOLOCK))
			and customerid not in (select ratingcustomerid from dbo.ratingcommenthelpfulness with (NOLOCK))
			and customerid not in (select votingcustomerid from dbo.ratingcommenthelpfulness with (NOLOCK))
	end

	-- clean addresses, except for those that have recurring orders
	if @eraseCCFromAddresses = 1
		update [dbo].address set 
			CardNumber=NULL,
			CardStartDate=NULL,
			CardIssueNumber=NULL,
			CardExpirationMonth=NULL,
			CardExpirationYear=NULL
		where CustomerID not in (select CustomerID from ShoppingCart where CartType=2)

	-- erase credit cards from all orders older than N days:
	if @eraseCCFromOrdersOlderThan <> 0
		update [dbo].orders set CardNumber=NULL
		where
			OrderDate < dateadd(d,-@eraseCCFromOrdersOlderThan,getdate())

	-- erase product views both for dynamic
	if @clearProductViewsOrderThan <> 0
	begin
		delete dbo.ProductView where ViewDate < dateadd(d,-@clearProductViewsOrderThan,getdate())
	end

	-- Nuke deleted stores
	declare @storeId int
	select top 1 @storeId = StoreID from Store where Deleted = 1
	while @@rowcount > 0 begin
		exec aspdnsf_NukeStore @storeId, 0
		select top 1 @storeId = StoreID from Store where Deleted = 1
	end

	if @purgeDeletedRecords = 1 begin
		delete dbo.Address where deleted = 1
		delete dbo.Coupon where deleted = 1
		delete dbo.Customer where deleted = 1
		delete dbo.Document where deleted = 1
		delete dbo.News where deleted = 1
		delete dbo.Product where deleted = 1
		delete dbo.ProductVariant where deleted = 1 or not exists (select * from dbo.Product where productid = ProductVariant.productid)
		delete dbo.SalesPrompt where deleted = 1
		delete dbo.ShippingZone where deleted = 1
		delete dbo.Topic where deleted = 1
		delete dbo.Affiliate where deleted = 1
		delete dbo.Category where deleted = 1
		delete dbo.CustomerLevel where deleted = 1
		delete dbo.Distributor where deleted = 1
		delete dbo.Genre where deleted = 1
		delete dbo.Library where deleted = 1
		delete dbo.Manufacturer where deleted = 1
		delete dbo.Section where deleted = 1
		delete dbo.Vector where deleted = 1
		delete dbo.ProductVector where not exists (select * from dbo.product where productid = ProductVector.productid) or not exists (select * from dbo.vector where vectorid = ProductVector.vectorid)
		delete dbo.ProductAffiliate where not exists (select * from dbo.product where productid = ProductAffiliate.productid) or not exists (select * from dbo.Affiliate where Affiliateid = ProductAffiliate.Affiliateid)
		delete dbo.ProductCategory where not exists (select * from dbo.product where productid = ProductCategory.productid) or not exists (select * from dbo.Category where Categoryid = ProductCategory.Categoryid)
		delete dbo.ProductCustomerLevel where not exists (select * from dbo.product where productid = ProductCustomerLevel.productid) or not exists (select * from dbo.CustomerLevel where CustomerLevelid = ProductCustomerLevel.CustomerLevelid)
		delete dbo.ProductDistributor where not exists (select * from dbo.product where productid = ProductDistributor.productid) or not exists (select * from dbo.Distributor where Distributorid = ProductDistributor.Distributorid)
		delete dbo.ProductGenre where not exists (select * from dbo.product where productid = ProductGenre.productid) or not exists (select * from dbo.Genre where Genreid = ProductGenre.Genreid)
		delete dbo.ProductLocaleSetting where not exists (select * from dbo.product where productid = ProductLocaleSetting.productid) or not exists (select * from dbo.LocaleSetting where LocaleSettingid = ProductLocaleSetting.LocaleSettingid)
		delete dbo.ProductManufacturer where not exists (select * from dbo.product where productid = ProductManufacturer.productid) or not exists (select * from dbo.Manufacturer where Manufacturerid = ProductManufacturer.Manufacturerid)
		delete dbo.ProductSection where not exists (select * from dbo.product where productid = ProductSection.productid) or not exists (select * from dbo.Section where Sectionid = ProductSection.Sectionid)
	end

	-- Clear out all customer sessions
	truncate table CustomerSession

	-- Clean up abandon records tied to customers that no longer exist
	delete from dbo.ShoppingCart where CustomerID not in (select distinct CustomerID from Customer);
	delete from dbo.KitCart where ShoppingCartRecID not in (select distinct ShoppingCartRecID from ShoppingCart);
	delete from dbo.CustomerSession where CustomerID not in (select distinct CustomerID from Customer);
	delete from dbo.RatingCommentHelpfulness where RatingCustomerID not in (select distinct CustomerID from Customer);
	delete from dbo.RatingCommentHelpfulness where VotingCustomerID not in (select distinct CustomerID from Customer);
	delete from dbo.PromotionUsage where CustomerID not in (select distinct CustomerID from Customer);
	delete from dbo.Address where CustomerID not in (select distinct CustomerID from Customer);
	delete from dbo.Rating where CustomerID not in (select distinct CustomerID from Customer);

	-- Remove old rtshipping requests and responses
	if @removeRTShippingDataOlderThan <> 0
	begin
		update dbo.Customer set RTShipRequest = '', RTShipResponse = ''
		where CreatedOn < dateadd(d,-@removeRTShippingDataOlderThan,getdate())

		update dbo.Orders set RTShipRequest = '', RTShipResponse = ''
		where OrderDate < dateadd(d,-@removeRTShippingDataOlderThan,getdate())
	end

	-- Search log
	if @clearSearchLogOlderThan <> 0
	begin
		delete from dbo.SearchLog where CreatedOn < dateadd(d,-@clearSearchLogOlderThan,getdate())
	end

	-- Profile table
	if @clearProfilesOlderThan != -1
	begin
		if @clearProfilesOlderThan = 0
		begin
			truncate table [dbo].[profile]
		end
		else
		begin
			delete from [dbo].[Profile] where UpdatedOn < dateadd(d,-@clearProfilesOlderThan,getdate());
		end
	end

	-- Defrag indexes
	DECLARE @indexesUpdated BIT = 0
	IF @defragIndexes = 1
	BEGIN
		DECLARE @cmd NVARCHAR(MAX), @tableName VARCHAR(128), @indexName VARCHAR(128)
		CREATE TABLE #INDEXESTOUPDATE(
			TableName VARCHAR(128),
			IndexName VARCHAR(128))

		INSERT INTO #IndexesToUpdate
		SELECT  o.name AS TableName, i.name AS IndexName
		FROM sys.dm_db_index_physical_stats(DB_ID(), NULL, NULL, NULL, NULL) s
			JOIN sys.sysobjects o ON s.object_id = o.id
			LEFT JOIN sys.indexes i ON s.index_id = i.index_id AND o.id = i.object_id
		WHERE s.index_id != 0
			AND s.avg_fragmentation_in_percent > 10
			AND s.page_count > 500
		ORDER BY o.name

		DECLARE indexCursor CURSOR
			FOR SELECT TableName, IndexName FROM #IndexesToUpdate
		OPEN indexCursor
		FETCH NEXT FROM indexCursor
			INTO @tableName, @indexName
		WHILE @@FETCH_STATUS = 0
			BEGIN
				SET @cmd = 'ALTER INDEX [' + @indexName + '] ON [' + @tableName  + '] REBUILD'
				EXEC (@cmd)

			FETCH NEXT FROM indexCursor
				INTO @tableName, @indexName
			END
		CLOSE indexCursor
		DEALLOCATE indexCursor

		-- ALTER INDEX doesn't update statistics, have to trigger it manually
		EXEC [dbo].[aspdnsf_UpdateStatistics]
		SET @indexesUpdated = 1
	END

	--Update statistics, including ones that updating indexes misses
	IF @updateStats = 1 AND @indexesUpdated = 0 --This may have already been done above
	BEGIN
		EXEC [dbo].[aspdnsf_UpdateStatistics]
	END
end
go

-- Clean payment methods of PayPal (Standard) and CheckoutByAmazon, for ConfigValue and AllowableValues
-- Parse out all allowable values for all stores into one homogenzied list
-- Explicitly exclude payment methods deprecated in this version
;with Allowable as (
    select distinct rtrim(ltrim(Items)) [Value]
    from AppConfig
        cross apply dbo.Split(AppConfig.AllowableValues, ',')
    where
        Name = 'PaymentMethods'
        and rtrim(ltrim(Items)) not in ('', 'PayPal', 'CheckoutByAmazon')),


-- Parse out all configured values with their associated store ID
Configured as (
    select StoreID, rtrim(ltrim(Items)) [Value]
    from AppConfig
        cross apply dbo.Split(AppConfig.ConfigValue, ',')
    where
        Name = 'PaymentMethods'),

-- Merge all allowable values for all stores into a single comma-separated list
MergedAllowable as (
    select top 1
        stuff(
            (select ',' + [Value]
            from Allowable
            order by [Value]
            for xml path('')),
            1, 1, '') as [Values]
    from Allowable
    group by [Value]),

-- Merge each store's configured values into a comma-separated list per-store,
-- using the names from the Allowable values and filtering out any configured
-- values that are not in the allowable values
MergedConfigured as (
    select distinct
        StoreID,
        coalesce(stuff(
            (select ',' + Allowable.[Value]
            from Configured
            inner join Allowable on Allowable.Value = Configured.Value
            where Configured.StoreID = StoreWrapper.StoreID
            order by Configured.[Value]
            for xml path('')),
            1, 1, ''), '') as [Values]
    from Configured StoreWrapper
    group by StoreID)

-- Update each store's PaymentMethods AppConfig with the common allowable values and
-- that store's configured values
update AppConfig
set ConfigValue = MergedConfigured.[Values], AllowableValues = MergedAllowable.[Values]
from AppConfig
inner join MergedConfigured on AppConfig.StoreID = MergedConfigured.StoreID
cross join MergedAllowable
where AppConfig.Name = 'PaymentMethods'

/*********** End 10.0.2 Changes *********************/

PRINT CHAR(10)
PRINT '*****Finalizing database upgrade*****'
-- Update database indexes
PRINT 'Updating database indexes...'
EXEC [dbo].[aspdnsf_UpdateIndexes]
-- Update store version
PRINT 'Updating Store Version...'
UPDATE [dbo].[AppConfig] SET [ConfigValue] = '10.0.2' WHERE [Name] = 'StoreVersion'
print '*****Database Upgrade Completed*****'

SET NOEXEC OFF
GO
