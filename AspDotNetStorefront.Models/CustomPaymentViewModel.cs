// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AspDotNetStorefront.Validation.DataAttribute;
using AspDotNetStorefrontCore;
using Foolproof;


namespace AspDotNetStorefront.Models
{
    [Bind(Exclude = "ListPrimarySynchronyAccountInfo,ListPrimarySynchronyTerm,ListPrimarySynchronyPrimaryID,ListPrimaryIssuedState,ListPrimaryExpiretionMonth,ListPrimaryExpiretionYear,ListPrimaryCustomPayment,ListSecondlySynchronyAccountInfo,ListSecondlySynchronyTerm,ListSecondlySynchronyPrimaryID,ListSecondlyIssuedState,ListSecondlyExpiretionMonth,ListSecondlyExpiretionYear,ListSecondlyCustomPayment,ListThirdlySynchronyAccountInfo,ListThirdlySynchronyTerm,ListThirdlySynchronyPrimaryID,ListThirdlyIssuedState, ListThirdlyExpiretionMonth, ListThirdlyExpiretionYear, ListThirdlyCustomPayment")]
    public class CustomPaymentViewModel
	{
        /****** Primary Payment ******/
        public SelectList ListPrimarySynchronyAccountInfo;
        public SelectList ListPrimarySynchronyTerm;
        //add 20190607 JBS
        public SelectList ListPrimarySynchronyPrimaryID;
        public SelectList ListPrimaryIssuedState;
        public SelectList ListPrimaryExpiretionMonth;
        public SelectList ListPrimaryExpiretionYear;

        [Display(Name = "checkout.CustomPayment.label")]
        public SelectList ListPrimaryCustomPayment;

        [Display(Name = "checkout.SynchronyAccountNumber.label", Prompt = "checkout.SynchronyAccountNumber.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyAccountNumber.required")]*/
        [RegularExpression(@"^(\d{16})$", ErrorMessage = "Account Number must be 16 digits")]
        public string PrimaryAccountNumber
		{ get; set; }

        [Display(Name = "checkout.SynchronyApplicationKey.label", Prompt = "checkout.SynchronyApplicationKey.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        [RegularExpression(@"^(\d{8})$", ErrorMessage = "Application key must be 8 digits")]
        public string PrimaryApplicationKey
        { get; set; }

        /*[Required(ErrorMessage = "checkout.SynchronyAccountInfo.required")]*/
        public string PrimarySelectedSynchronyAccountInfo
        { get; set; }

        /*[Required(ErrorMessage = "checkout.SynchronyTerm.required")]*/
        public string PrimarySelectedSynchronyTerm
        { get; set; }

        //add 20190607 JBS
        /*[Required(ErrorMessage = "checkout.SynchronyPrimaryID.required")]*/
        public string PrimarySelectedSynchronyPrimaryID
        { get; set; }

        /*[Required(ErrorMessage = "checkout.IssuedState.required")]*/
        public string PrimarySelectedIssuedState
        { get; set; }

        /*[Required(ErrorMessage = "checkout.ExpiretionMonth.required")]*/
        public string PrimarySelectedExpiretionMonth
        { get; set; }

        /*[Required(ErrorMessage = "checkout.ExpiretionYear.required")]*/
        public string PrimarySelectedExpiretionYear
        { get; set; }

        [Display(Name = "checkout.CCWAnumber.label", Prompt = "checkout.CCWAnumber.example")]
        /*[Required(ErrorMessage = "checkout.CCWA.required")]*/
        public string PrimaryCCWA
        { get; set; }

        [Display(Name = "checkout.PurchaseOrderNumber.label", Prompt = "checkout.PurchaseOrderNumber.example")]
        public string PrimaryPurchaseOrderNumber
        { get; set; }

        [Display(Name = "checkout.SamsSynchronyNumber.label", Prompt = "checkout.SamsSynchronyNumber.example")]
        public string PrimarySamsSynchronyNumber
        { get; set; }

        [Display(Name = "checkout.PrimaryPaymentBalance.label")]
        [RegularExpression(@"\d{1,20}(\.\d{1,2})?", ErrorMessage = "Invalid Price. Please use the format of XXXX.XX.")]
        public string PrimaryPaymentBalance
        { get; set; }

        [Required(ErrorMessage = "checkout.PrimaryPayment.required")]
        public string SelectedPrimaryPayment
        { get; set; }


        /****** Secondly Payment ******/
        public SelectList ListSecondlySynchronyAccountInfo;
        public SelectList ListSecondlySynchronyTerm;
        //add 20190607 JBS
        public SelectList ListSecondlySynchronyPrimaryID;
        public SelectList ListSecondlyIssuedState;
        public SelectList ListSecondlyExpiretionMonth;
        public SelectList ListSecondlyExpiretionYear;

        [Display(Name = "checkout.CustomPayment.label")]
        public SelectList ListSecondlyCustomPayment;

        [Display(Name = "checkout.SynchronyAccountNumber.label", Prompt = "checkout.SynchronyAccountNumber.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyAccountNumber.required")]*/
        [RegularExpression(@"^(\d{16})$", ErrorMessage = "Account Number must be 16 digits")]
        public string SecondlyAccountNumber
        { get; set; }

        [Display(Name = "checkout.SynchronyApplicationKey.label", Prompt = "checkout.SynchronyApplicationKey.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        [RegularExpression(@"^(\d{8})$", ErrorMessage = "Application key must be 8 digits")]
        public string SecondlyApplicationKey
        { get; set; }

        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        public string SecondlySelectedSynchronyAccountInfo
        { get; set; }

        /*[Required(ErrorMessage = "checkout.SynchronyTerm.required")]*/
        public string SecondlySelectedSynchronyTerm
        { get; set; }

        //add 20190607 JBS
        /*[Required(ErrorMessage = "checkout.SynchronyPrimaryID.required")]*/
        public string SecondlySelectedSynchronyPrimaryID
        { get; set; }

        /*[Required(ErrorMessage = "checkout.IssuedState.required")]*/
        public string SecondlySelectedIssuedState
        { get; set; }

        /*[Required(ErrorMessage = "checkout.ExpiretionMonth.required")]*/
        public string SecondlySelectedExpiretionMonth
        { get; set; }

        /*[Required(ErrorMessage = "checkout.ExpiretionYear.required")]*/
        public string SecondlySelectedExpiretionYear
        { get; set; }

        [Display(Name = "checkout.CCWAnumber.label", Prompt = "checkout.CCWAnumber.example")]
        /*[Required(ErrorMessage = "checkout.CCWA.required")]*/
        public string SecondlyCCWA
        { get; set; }

        [Display(Name = "checkout.PurchaseOrderNumber.label", Prompt = "checkout.PurchaseOrderNumber.example")]
        public string SecondlyPurchaseOrderNumber
        { get; set; }

        [Display(Name = "checkout.SamsSynchronyNumber.label", Prompt = "checkout.SamsSynchronyNumber.example")]
        public string SecondlySamsSynchronyNumber
        { get; set; }

        [Display(Name = "checkout.SecondlyPaymentBalance.label")]
        [RegularExpression(@"\d{1,20}(\.\d{1,2})?", ErrorMessage = "Invalid Price. Please use the format of XXXX.XX.")]
        public string SecondlyPaymentBalance
        { get; set; }

        [RequiredIfNotEmpty("SecondlyPaymentBalance")]
        public string SelectedSecondlyPayment
        { get; set; }


        /****** Thirdly Payment ******/
        public SelectList ListThirdlySynchronyAccountInfo;
        public SelectList ListThirdlySynchronyTerm;
        //add 20190607 JBS
        public SelectList ListThirdlySynchronyPrimaryID;
        public SelectList ListThirdlyIssuedState;
        public SelectList ListThirdlyExpiretionMonth;
        public SelectList ListThirdlyExpiretionYear;

        [Display(Name = "checkout.CustomPayment.label")]
        public SelectList ListThirdlyCustomPayment;

        [Display(Name = "checkout.SynchronyAccountNumber.label", Prompt = "checkout.SynchronyAccountNumber.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyAccountNumber.required")]*/
        [RegularExpression(@"^(\d{16})$", ErrorMessage = "Account Number must be 16 digits")]
        public string ThirdlyAccountNumber
        { get; set; }

        [Display(Name = "checkout.SynchronyApplicationKey.label", Prompt = "checkout.SynchronyApplicationKey.example")]
        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        [RegularExpression(@"^(\d{8})$", ErrorMessage = "Application key must be 8 digits")]
        public string ThirdlyApplicationKey
        { get; set; }

        /*[Required(ErrorMessage = "checkout.SynchronyApplicationKey.required")]*/
        public string ThirdlySelectedSynchronyAccountInfo
        { get; set; }

        /*[Required(ErrorMessage = "checkout.SynchronyTerm.required")]*/
        public string ThirdlySelectedSynchronyTerm
        { get; set; }

        [Display(Name = "checkout.CCWAnumber.label", Prompt = "checkout.CCWAnumber.example")]
        /*[Required(ErrorMessage = "checkout.CCWA.required")]*/
        public string ThirdlyCCWA
        { get; set; }

        //add 20190607 JBS
        /*[Required(ErrorMessage = "checkout.SynchronyPrimaryID.required")]*/
        public string ThirdlySelectedSynchronyPrimaryID
        { get; set; }

        /*[Required(ErrorMessage = "checkout.IssuedState.required")]*/
        public string ThirdlySelectedIssuedState
        { get; set; }

        /*[Required(ErrorMessage = "checkout.ExpiretionMonth.required")]*/
        public string ThirdlySelectedExpiretionMonth
        { get; set; }

        /*[Required(ErrorMessage = "checkout.ExpiretionYear.required")]*/
        public string ThirdlySelectedExpiretionYear
        { get; set; }

        [Display(Name = "checkout.PurchaseOrderNumber.label", Prompt = "checkout.PurchaseOrderNumber.example")]
        public string ThirdlyPurchaseOrderNumber
        { get; set; }

        [Display(Name = "checkout.SamsSynchronyNumber.label", Prompt = "checkout.SamsSynchronyNumber.example")]
        public string ThirdlySamsSynchronyNumber
        { get; set; }

        [Display(Name = "checkout.ThirdlyPaymentBalance.label")]
        [RegularExpression(@"\d{1,20}(\.\d{1,2})?", ErrorMessage = "Invalid Price. Please use the format of XXXX.XX.")]
        public string ThirdlyPaymentBalance
        { get; set; }

        [RequiredIfNotEmpty("ThirdlyPaymentBalance")]
        public string SelectedThirdlyPayment
        { get; set; }

    }
}
