<%@ Page Language="C#" AutoEventWireup="true" Inherits="AspDotNetStorefrontAdmin.orderdetail" MasterPageFile="~/App_Templates/Admin_Default/AdminMaster.master" ValidateRequest="false" MaintainScrollPositionOnPostback="true" Codebehind="order.aspx.cs" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="aspdnsf" TagName="DataQueryFilter" Src="Controls/Listing/DataQueryFilter.ascx" %>

<asp:Content ContentPlaceHolderID="bodyContentPlaceholder" runat="server">
	<aspdnsf:ReturnUrlTracker runat="server" ID="ReturnUrlTracker" DefaultReturnUrl="orders.aspx" />
	<h1>
		<i class="fa fa-list"></i>
		<asp:Literal ID="litHeader" runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.OrderInfo %>" />
	</h1>
	<div>
		<aspdnsf:AlertMessage runat="server" ID="ctrlAlertMessage" />
	</div>
	<div class="main-content-wrapper" id="editOrderWrap">
		<div id="divMessage" runat="server" visible="false">
			<asp:Label runat="server" ID="lblMessage" />
		</div>

		<asp:Panel ID="pnlOrderDetails" runat="server" Visible="true">
			<div class="item-action-bar">
				<asp:HyperLink runat="server" ID="btnCloseTop" CssClass="btn btn-default" NavigateUrl="<%# ReturnUrlTracker.GetHyperlinkReturnUrl() %>" Text="<%$Tokens:StringResource, admin.common.close %>" />
				<asp:Button ID="btnViewReceipt" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderframe.OpenNewReceiptWindowHere %>" runat="server" OnClick="btnViewReceipt_Click" />
				<asp:Button ID="btnViewDriverInstruction" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderframe.OpenNewDriverInstructionWindowHere %>" runat="server" OnClick="btnViewDriverInstruction_Click" />
			
            </div>

			<div id="divOrderContents" runat="server" class="white-ui-box" visible="true">
				<div class="row form-inline">
					<div class="col-md-6">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.OrderInfo %>" />
						</div>
						<div class="row">
							<div class="col-md-4">
							 Order Number: 
							</div>
							<div class="col-md-8">
								<asp:Label runat="server" ID="lblOrderNumber" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.OrderDate %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litOrderDate" runat="server" />
							</div>
						</div>
						<div id="divStore" class="row" runat="server" visible="false">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.reports.forstore %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litStoreName" runat="server" />
								(<asp:Literal ID="litStoreId" runat="server" />)
							</div>
						</div>
						
						<div id="divLocale" class="row" runat="server" visible="false">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.LocaleSetting %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litCustomerLocale" runat="server" />
							</div>
						</div>
						<!--<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.OrderIsNew %>" />
							</div>
							<div class="col-md-8">
								<asp:RadioButton runat="server" ID="radOrderNew" GroupName="IsNew" AutoPostBack="true" Text="<%$Tokens:StringResource, admin.common.Yes %>" OnCheckedChanged="radOrderNew_CheckedChanged" />
								<asp:RadioButton runat="server" ID="radOrderNotNew" GroupName="IsNew" AutoPostBack="true" Text="<%$Tokens:StringResource, admin.common.No %>" OnCheckedChanged="radOrderNew_CheckedChanged" />
							</div>
						</div>-->
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.OrderSalesRepName %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litSalesRepName" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SalesPersonPhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litSalesPersonPhone" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.OrderEventName %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litEventName" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.IsHoldOrder %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litIsHoldOrder" />
							</div>
						</div>
                        
                        <br>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerBillingHomePhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litOrderBillingHomePhone" runat="server" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerBillingCellPhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litOrderBillingCellPhone" runat="server" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerShippingHomePhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litOrderShippingHomePhone" runat="server" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerShippingCellPhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litOrderShippingCellPhone" runat="server" />
							</div>
						</div>

                        <!--Summary-->
                        <div class="row">
                            <div class="col-md-12"><hr /></div>
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.cs.90 %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litSubTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.cs.200 %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litDiscountsTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.aspx.12 %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litShippingTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.aspx.14 %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litTaxTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.OrderTotal %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litOrderTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.IsNoTax %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litIsNoTax" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ActualTaxRate %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litActualTaxRate" />%
							</div>
						</div>
                        <div class="row">
                            <div class="col-md-6"><hr /></div>
                        </div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.cs.primarypaymentbalance %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litPrimaryPaymentTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.cs.secondlypaymentbalance %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litSecondlyPaymentTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.cs.thirdlypaymentbalance %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litThirdlyPaymentTotal" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, shoppingcart.cs.remainingbalance %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal runat="server" ID="litRemainingBalance" />
							</div>
						</div>
                        
                        <!--End of Summary-->
					</div>
					<div class="col-md-6">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.CustomerInfo %>" />
						</div>
						<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.customer.CustomerID %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litCustomerID" runat="server" />
								<asp:HyperLink runat="server" ID="lnkOrderHistory" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.CustomerName %>" />
							</div>
							<div class="col-md-8">
								<asp:HyperLink ID="lnkCustomerName" runat="server" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerPhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litCustomerPhone" runat="server" />
							</div>
						</div>
                        <div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerCellPhone %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litCustomerCellPhone" runat="server" />
							</div>
						</div>
                        
						<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.CustomerEmail %>" />
							</div>
							<div class="col-md-8">
								<asp:TextBox CssClass="form-control" ID="txtCustomerEmail" runat="server" />
								<aspdnsf:EmailValidator ID="valCustomerEmail" CssClass="text-danger" ControlToValidate="txtCustomerEmail" Display="Dynamic" Text="<%$Tokens:StringResource, admin.customer.EmailValidationFailed %>" runat="server" />
								<asp:Button ID="btnChangeEmail" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderframe.ChangeOrderEmail %>" runat="server" OnClick="btnChangeEmail_Click" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.RegisteredOn %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litCustomerRegisterDate" runat="server" />
							</div>
						</div>
						<!--<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.AffiliateID %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litAffiliateID" runat="server" />
							</div>
						</div>-->
						<!--<div class="row">
							<div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.Referrer %>" />
							</div>
							<div class="col-md-8">
								<asp:Literal ID="litReferrer" runat="server" />
							</div>
						</div>-->
					</div>
				</div>
			</div>

			<div class="row">
				<div id="divLeftColumn" runat="server" class="col-md-6" visible="true">
					<div id="divAddresses" class="white-ui-box">
						<div class="row">
							<div class="col-md-6">
								<div class="white-box-heading">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.BillingAddress %>" />
								</div>
							</div>
							<div class="col-md-6">
								<div class="white-box-heading">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.ShippingAddress %>" />
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<asp:Literal ID="litBillingAddress" runat="server" />
							</div>
							<div class="col-md-6">
								<asp:Literal ID="litShippingAddress" runat="server" />
							</div>
						</div>
					</div>

					<div id="divDeliveryInfo" class="white-ui-box">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.DeliveryInfo %>" />
						</div>
						<div id="divShippingDeliveryInfo" visible="false" runat="server">
							<div class="row" style="display:none">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.OrderWeight %>" />
								</div>
								<div class="col-md-8">
									<asp:TextBox CssClass="text-sm" runat="server" ID="txtOrderWeight" />
									<asp:CompareValidator ID="valOrderWeight" runat="server" CssClass="text-danger" ControlToValidate="txtOrderWeight" Type="Currency" Text="<%$Tokens:StringResource, admin.orderdetails.ValidDecimal %>" Operator="DataTypeCheck" Display="Dynamic" />
									<asp:Button ID="btnAdjustOrderWeight" runat="server" CssClass="btn btn-primary" OnClick="btnAdjustOrderWeight_Click" Text="<%$Tokens:StringResource, admin.orderframe.AdjustWeight %>" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ShippingMethod %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litShippingMethod" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ShippingPricePaid %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litShippingPricePaid" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ShippedOn %>" />
								</div>
								<div class="col-md-8">
									<telerik:RadDatePicker Width="200px" InputMode="DateTimePicker" SelectedDate='<%# System.DateTime.Now %>' ID="dpShippedOn" runat="server" MaxDate="9999-12-31" MinDate="0001-01-01">
										<Calendar runat="server" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x" />
									</telerik:RadDatePicker>
									<asp:Button ID="btnMarkAsShipped" ValidationGroup="markAsShipped" Enabled="false" runat="server" CssClass="btn btn-primary" OnClick="btnMarkAsShipped_Click" Text="<%$Tokens:StringResource, admin.orderframe.MarkAsShipped %>" />
									<asp:RequiredFieldValidator CssClass="text-danger" ErrorMessage="<%$Tokens:StringResource, admin.common.FillinShippedDate %>" Display="Dynamic" ControlToValidate="dpShippedOn" ID="rfvDpShippedOn" SetFocusOnError="true" runat="server" ValidationGroup="markAsShipped" />
								</div>
							</div>
                            
                            <div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.DelieredOn %>" />
								</div>
								<div class="col-md-8">
									<telerik:RadDatePicker Width="200px" InputMode="DateTimePicker" SelectedDate='<%# System.DateTime.Now %>' ID="dpDeliveredOn" runat="server" MaxDate="9999-12-31" MinDate="0001-01-01">
										<Calendar runat="server" UseColumnHeadersAsSelectors="False" UseRowHeadersAsSelectors="False" ViewSelectorText="x" />
									</telerik:RadDatePicker>
									<asp:Button ID="btnMarkAsDelivered" ValidationGroup="markAsDelivered" Enabled="false" runat="server" CssClass="btn btn-primary" OnClick="btnMarkAsDelivered_Click" Text="<%$Tokens:StringResource, admin.orderframe.MarkAsDelivered %>" />
									<asp:RequiredFieldValidator CssClass="text-danger" ErrorMessage="<%$Tokens:StringResource, admin.common.FillinDeliveredDate %>" Display="Dynamic" ControlToValidate="dpDeliveredOn" ID="rfvDpDeliveredOn" SetFocusOnError="true" runat="server" ValidationGroup="markAsDelivered" />
								</div>
							</div>

                            <div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.SerialNumber %>" />
								</div>
								<div class="col-md-8">
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtSerialNumber" />
								</div>
							</div>

                            <div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ChairType %>" />
								</div>
								<div class="col-md-8">
                                   <asp:DropDownList CssClass="form-control" ID="ddlChairType" runat="server" Enabled="false" ClientIDMode="Static" />
						    
								</div>
							</div>

                            

							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.Carrier %>" />
								</div>
								<div class="col-md-8">
									<asp:TextBox CssClass="form-control" runat="server" ID="txtShippedVia" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.TrackingNumber %>" />
								</div>
								<div class="col-md-8">
									<asp:TextBox CssClass="form-control" runat="server" ID="txtTrackingNumber" />
								</div>
							</div>
							<div id="divMultiShip" runat="server" class="row form-group" visible="false">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.HasMultipleShippingAddresses %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litMultipleShippingAddresses" />
								</div>
							</div>
							<!--<div class="row">
								<div class="col-md-8 col-md-offset-3">
									<asp:Button ID="btnSendToShipManager" Enabled="false" runat="server" CssClass="btn btn-primary" OnClick="btnSendToShipManager_Click" />
								</div>
							</div>-->
						</div>
						<div id="divDownloadDeliveryInfo" visible="false" runat="server">
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.HasDownloadItems %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal ID="litHasDownloadItems" runat="server" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.AllDownloadItems %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal ID="litAllDownloaditems" runat="server" />
								</div>
							</div>
							<div>
								<div class="admin-table-wrap">
									<div class="table-row-inner">
										<asp:GridView ID="grdDownloadItems" CssClass="table table-detail" ShowHeader="true" PageIndex="0" AllowPaging="true" PageSize="3" OnRowDataBound="grdDownloadItems_OnRowDataBound"
											runat="server" GridLines="None" CellSpacing="-1" OnPageIndexChanging="grdDownloadItems_OnPageIndexChanging" AutoGenerateColumns="false">
											<Columns>
												<asp:TemplateField>
													<HeaderTemplate>
														<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.DownloadValidDates %>" />
													</HeaderTemplate>
													<ItemTemplate>
														<div>
															<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.DownloadReleasedOn %>" />
															<asp:Literal runat="server" ID="litDownloadReleasedOn" />
														</div>
														<div>
															<asp:Literal runat="server" ID="litDownloadExpirationLabel" />
															<asp:Literal runat="server" ID="litDownloadExpiresOn" />
														</div>
													</ItemTemplate>
												</asp:TemplateField>
												<asp:TemplateField>
													<HeaderTemplate>
														<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.common.DownloadFile %>" />
													</HeaderTemplate>
													<ItemTemplate>
														<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.DownloadFileLocation %>" />
														<asp:TextBox runat="server" CssClass="form-control" ID="txtDownloadLocation" />
														<asp:Button ID="btnReleaseDownload" CssClass="btn btn-primary" runat="server" CommandArgument="<%# (Container.DataItem as AspDotNetStorefrontCore.DownloadItem).ShoppingCartRecordId %>" OnClick="btnReleaseDownload_Click" Text="<%$Tokens:StringResource, admin.orderframe.DownloadReleaseItem %>" />
													</ItemTemplate>
												</asp:TemplateField>
											</Columns>
										</asp:GridView>
									</div>
									<div runat="server" visible="false" id="divDelayedDownloadWarning" class="alert alert-danger">
										<asp:Literal runat="server" Visible="false" ID="litDelayedDownloadWarning" Text="<%$Tokens:StringResource, download.aspx.17 %>" />
									</div>
								</div>
							</div>
						</div>
						<div id="divDistributorDeliveryInfo" visible="false" runat="server">
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.HasDistributorDropShipItems %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal ID="litHasDistributorItems" runat="server" />
								</div>
							</div>
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.DistributorEmailSentOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal ID="litDistributorEmailSentOn" runat="server" />
									<asp:Button ID="btnSendDistributorEmail" runat="server" CssClass="btn btn-primary" OnClick="btnSendDistributorEmail_Click" Text="<%$Tokens:StringResource, admin.orderframe.SendTrackingNumber %>" />
								</div>
							</div>
							<div>
								<a href="javascript:void(0);" class="btn btn-primary" onclick="toggleDistributor();">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.DistributorInfo %>"></asp:Literal></a>
							</div>
							<div id="divDistributorInfo" style="display: none;">
								<div>
									<asp:Literal ID="litDistributorNotifications" runat="server" />
								</div>
							</div>
						</div>
					</div>

					<div id="divNotes" class="white-ui-box">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.Notes %>" />
						</div>
						<div class="form-group">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.OrderNotes %>" />
							<asp:TextBox TextMode="MultiLine" Rows="2" runat="server" ID="txtOrderNotes" CssClass="form-control disabled" />
						</div>
						<div class="form-group">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.PrivateNotes %>" />
							<asp:TextBox TextMode="MultiLine" Rows="2" runat="server" ID="txtAdminNotes" CssClass="form-control" />
						</div>
						<div class="form-group">
							<!--<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerServiceNotes %>" />&nbsp;<asp:Literal runat="server" ID="litCustomerServiceVisible" />-->
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CustomerServiceNotes %>" />
                            <asp:TextBox TextMode="MultiLine" Rows="2" runat="server" ID="txtCustomerServiceNotes" CssClass="form-control" />
						</div>
						<!--<div class="form-group">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.FinalizationData %>" />
							<asp:TextBox TextMode="MultiLine" Rows="2" runat="server" ID="txtFinalizationData" CssClass="form-control disabled" />
						</div>-->
                        <div class="form-group">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.SynchronyNotes %>" />&nbsp;<asp:Literal runat="server" ID="Literal1" />
							<asp:TextBox TextMode="MultiLine" Rows="4" runat="server" ID="txtSynchronyNotes" PlaceHolder="Account Number (16 digits) &#10;Application Key (8 digits)&#10;Account Info(Sandiego, Lasvegas or Houston)&#10;Term(12month, 24month, 36month or 48month)" CssClass="form-control" />
						</div>
                        <div class="form-group">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ActualShippingCost %>" />
							<asp:TextBox runat="server" ID="txtActualShippingCost" CssClass="form-control disabled" />
                            <asp:RegularExpressionValidator ValidationExpression="^[0-9]\d*(\.\d+)?$"  ID="DecimalValidator" CssClass="text-danger" ControlToValidate="txtActualShippingCost" Display="Dynamic" Text="<%$Tokens:StringResource, admin.customer.DecimalValidationFailed %>" runat="server" />								
						</div>

                        <div class="form-group">
                            <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.BranchName %>" />
                            <asp:DropDownList CssClass="form-control" ID="ddlBranchNames" runat="server" Enabled="false" ClientIDMode="Static" />
						    
                        </div>			

                        <div class="form-group">
                            <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.OrderStatusName %>" />
                            <asp:DropDownList CssClass="form-control" ID="ddlOrderStatus" runat="server" Enabled="false" ClientIDMode="Static" />
						    
                        </div>

						<asp:Button ID="btnUpdateNotes" CssClass="btn btn-primary" runat="server" OnClick="btnUpdateNotes_Click" Text="<%$Tokens:StringResource, admin.common.UpdateNotes %>" />
					</div>
				</div>

				<div id="divRightColumn" runat="server" class="col-md-6" visible="true">
					<div id="divItemInfo" class="white-ui-box">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.ProductDetails %>" />
						</div>
						<div>
							<div>
								<div class="table-row-inner">
									<asp:GridView ID="grdProducts" CssClass="table table-detail" ShowHeader="true" PageIndex="0" AllowPaging="true" PageSize="5"
										runat="server" GridLines="None" CellSpacing="-1" OnPageIndexChanging="grdProducts_OnPageIndexChanging" AutoGenerateColumns="false">
										<Columns>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.common.ID %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("ProductID") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.common.SKU %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("SKU") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.common.ProductName %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("ProductName") + " - " + Eval("VariantName") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.common.Size %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# AspDotNetStorefrontCore.AppLogic.CleanSizeColorOption(Eval("ChosenSize").ToString()) %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.common.Color %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# AspDotNetStorefrontCore.AppLogic.CleanSizeColorOption(Eval("ChosenColor").ToString()) %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.Qty %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("Quantity") %>' />
												</ItemTemplate>
											</asp:TemplateField>
                                            <asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.WarrantyName %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("WarrantyName") %>' />
												</ItemTemplate>
											</asp:TemplateField>
                                            <asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.WarrantyPrice %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("WarrantyPrice") %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</div>
							</div>
						</div>
					</div>

					<div id="divPaymentInfo" class="white-ui-box">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.PaymentInfo %>" />
						</div>
						<div>
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.ReturnActions %>" />
								</div>
								<div class="col-md-8">
									<asp:DropDownList CssClass="form-control" ID="ddlCancelActions" runat="server" Enabled="false" ClientIDMode="Static" />
									<asp:Button ID="btnCancel" Enabled="false" runat="server" CssClass="btn btn-primary" OnClick="btnCancel_Click" Text="<%$Tokens:StringResource, admin.common.Submit %>" />
								</div>
							</div>-->
							<div runat="server" id="divMaxMind" visible="false" class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.MaxMindFraudScore %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litMaxMindScore" Text="--" />&nbsp;<asp:HyperLink ID="lnkMaxMindDetails" runat="server" Target="_blank" Text="<%$Tokens:StringResource, admin.orderframe.MaxMindFraudExplanation %>" />
									<asp:Button ID="btnGetMaxMind" Enabled="false" runat="server" CssClass="btn btn-primary" OnClick="btnGetMaxMind_Click" Text="<%$Tokens:StringResource, admin.orderframe.GetMaxMind %>" />
								</div>
							</div>
							<div id="divParentOrder" visible="false" runat="server" class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ParentOrder %>" />
								</div>
								<div class="col-md-8">
									<asp:HyperLink runat="server" ID="lnkParentOrder" />
								</div>
							</div>
							<div id="divRelatedOrder" visible="false" runat="server" class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RelatedOrder %>" />
								</div>
								<div class="col-md-8">
									<asp:HyperLink runat="server" ID="lnkRelatedOrder" />
								</div>
							</div>
							<div id="divChildOrders" visible="false" runat="server" class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.ChildOrders %>" />
								</div>
								<div class="col-md-8">
									<asp:Repeater ID="rptChildOrders" runat="server" OnItemDataBound="rptChildOrders_ItemDataBound">
										<ItemTemplate>
											<div><asp:HyperLink ID="childLink" runat="server" Text='<%# Eval("OrderNumber") %>' Target="_blank" /></div>
										</ItemTemplate>
									</asp:Repeater>
								</div>
							</div>


                            <!----- Custom payment: Added by JBS 11/17/2017 ----->
                            <div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PrimaryPaymentMethod %>" />
								</div>
                                <div class="col-md-8">
                                    <asp:DropDownList CssClass="form-control" ID="ddlPrimaryPaymentMethod" runat="server" Enabled="false" ClientIDMode="Static" />						    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">              
                                </div>
                                <div class="col-md-4 positionAdjustment">
                                    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PaymentBalance %>" />
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtPrimaryPaymentBalance" onchange="checkNumDigit(this,0)"/>
                                    <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.PaymentBalanceErrMessage %>" CssClass="pm-errormsg" />
                                </div>
                            </div>
                            <div class="row">
                                <div id="PrimaryCreditCardForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.AuthorizeNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtPrimaryAuthorizeNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="PrimaryPurchaseOrderNumberForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PurchaseOrderNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtPrimaryPurchaseOrderNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="PrimarySamsSynchronyForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SamsSynchronyNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtPrimarySamsSynchronyNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div id="PrimarySynchronyForm">
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumber %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtPrimarySynchronyAccountNumber" onchange="checkNumDigit(this,16)"/>
                                        <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumberErrMessage %>" CssClass="pm-errormsg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKey %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtPrimarySynchronyApplicationKey" onchange="checkNumDigit(this,8)"/>
                                        <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKeyErrMessage %>" CssClass="pm-errormsg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountInfo %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlPrimarySynchronyAccountInfo" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountTerm %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlPrimarySynchronyTerm" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyPrimaryID %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlPrimarySynchronyPrimaryID" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyIssuedState %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlPrimaryIssuedState" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyExpiretionDate %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlPrimaryExpiretionMonth" runat="server" Enabled="false" ClientIDMode="Static" />
                                        <asp:DropDownList CssClass="form-control" ID="ddlPrimaryExpiretionYear" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    if ($("#ddlPrimaryPaymentMethod").find('option:selected').text() == 'Synchrony') {
                                        $("#PrimarySynchronyForm").fadeIn(500);
                                        $("#PrimaryCreditCardForm").fadeOut(500);
                                        $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                        $("#PrimarySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlPrimaryPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'CreditCard') {
                                        $("#PrimarySynchronyForm").fadeOut(500);
                                        $("#PrimaryCreditCardForm").fadeIn(500);
                                        $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                        $("#PrimarySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlPrimaryPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'PurchaseOrder') {
                                        $("#PrimarySynchronyForm").fadeOut(500);
                                        $("#PrimaryCreditCardForm").fadeOut(500);
                                        $("#PrimaryPurchaseOrderNumberForm").fadeIn(500);
                                        $("#PrimarySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlPrimaryPaymentMethod").find('option:selected').text().replace(/\s/g, '').replace('�', '') == 'SamsClubSynchrony') {
                                        $("#PrimarySynchronyForm").fadeOut(500);
                                        $("#PrimaryCreditCardForm").fadeOut(500);
                                        $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                        $("#PrimarySamsSynchronyForm").fadeIn(500);
                                    } else {
                                        $("#PrimarySynchronyForm").fadeOut(500);
                                        $("#PrimaryCreditCardForm").fadeOut(500);
                                        $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                        $("#PrimarySamsSynchronyForm").fadeOut(500);
                                    }
                                    $("#ddlPrimaryPaymentMethod").change(function () {
                                        if ($("#ddlPrimaryPaymentMethod").find('option:selected').text() == 'Synchrony') {
                                            $("#PrimarySynchronyForm").fadeIn(500);
                                            $("#PrimaryCreditCardForm").fadeOut(500);
                                            $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                            $("#PrimarySamsSynchronyForm").fadeOut(500);
                                        } else if ($("#ddlPrimaryPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'CreditCard') {
                                            $("#PrimarySynchronyForm").fadeOut(500);
                                            $("#PrimaryCreditCardForm").fadeIn(500);
                                            $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                            $("#PrimarySamsSynchronyForm").fadeOut(500);
                                        } else if ($("#ddlPrimaryPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'PurchaseOrder') {
                                            $("#PrimarySynchronyForm").fadeOut(500);
                                            $("#PrimaryCreditCardForm").fadeOut(500);
                                            $("#PrimaryPurchaseOrderNumberForm").fadeIn(500);
                                            $("#PrimarySamsSynchronyForm").fadeOut(500);
                                        } else if ($("#ddlPrimaryPaymentMethod").find('option:selected').text().replace(/\s/g, '').replace('�', '') == 'SamsClubSynchrony') {
                                            $("#PrimarySynchronyForm").fadeOut(500);
                                            $("#PrimaryCreditCardForm").fadeOut(500);
                                            $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                            $("#PrimarySamsSynchronyForm").fadeIn(500);
                                        } else {
                                            $("#PrimarySynchronyForm").fadeOut(500);
                                            $("#PrimaryCreditCardForm").fadeOut(500);
                                            $("#PrimaryPurchaseOrderNumberForm").fadeOut(500);
                                            $("#PrimarySamsSynchronyForm").fadeOut(500);
                                        }
                                    });                                   
                                </script>
							</div>
                            <br />
                            <div class="row">

                                <div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SecondlyPaymentMethod %>" />
								</div>
                                <div class="col-md-8">
                                    <asp:DropDownList CssClass="form-control" ID="ddlSecondlyPaymentMethod" runat="server" Enabled="false" ClientIDMode="Static" />								    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">              
                                </div>
                                <div class="col-md-4 positionAdjustment">
                                    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PaymentBalance %>" />
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtSecondlyPaymentBalance" onchange="checkNumDigit(this,0)"/> 
                                    <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.PaymentBalanceErrMessage %>" CssClass="pm-errormsg" />
                                </div>
                            </div>
                            <div class="row">
                                <div id="SecondlyCreditCardForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.AuthorizeNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtSecondlyAuthorizeNumber" />                                    
                                    </div>
                                </div>
							</div>
                            <div class="row">
                                <div id="SecondlyPurchaseOrderNumberForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PurchaseOrderNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtSecondlyPurchaseOrderNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="SecondlySamsSynchronyForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SamsSynchronyNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtSecondlySamsSynchronyNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div id="SecondlySynchronyForm">
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumber %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtSecondlySynchronyAccountNumber"  onchange="checkNumDigit(this,16)"/>
                                        <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumberErrMessage %>" CssClass="pm-errormsg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKey %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtSecondlySynchronyApplicationKey"  onchange="checkNumDigit(this,8)"/>
                                        <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKeyErrMessage %>" CssClass="pm-errormsg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountInfo %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlSecondlySynchronyAccountInfo" runat="server" Enabled="false" ClientIDMode="Static" />	
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountTerm %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlSecondlySynchronyTerm" runat="server" Enabled="false" ClientIDMode="Static" />	
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyPrimaryID %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlSecondlySynchronyPrimaryID" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyIssuedState %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlSecondlyIssuedState" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyExpiretionDate %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlSecondlyExpiretionMonth" runat="server" Enabled="false" ClientIDMode="Static" />
                                        <asp:DropDownList CssClass="form-control" ID="ddlSecondlyExpiretionYear" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                if ($("#ddlSecondlyPaymentMethod").find('option:selected').text() == 'Synchrony') {
                                        $("#SecondlySynchronyForm").fadeIn(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                } else if ($("#ddlSecondlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'CreditCard') {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeIn(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                } else if ($("#ddlSecondlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'PurchaseOrder') {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeIn(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                } else if ($("#ddlSecondlyPaymentMethod").find('option:selected').text().replace(/\s/g, '').replace('�', '') == 'SamsClubSynchrony') {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeIn(500);
                                } else {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                }
                                $("#ddlSecondlyPaymentMethod").change(function () {
                                    if ($("#ddlSecondlyPaymentMethod").find('option:selected').text() == 'Synchrony') {
                                        $("#SecondlySynchronyForm").fadeIn(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlSecondlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'CreditCard') {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeIn(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlSecondlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'PurchaseOrder') {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeIn(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlSecondlyPaymentMethod").find('option:selected').text().replace(/\s/g, '').replace('�', '') == 'SamsClubSynchrony') {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeIn(500);
                                    } else {
                                        $("#SecondlySynchronyForm").fadeOut(500);
                                        $("#SecondlyCreditCardForm").fadeOut(500);
                                        $("#SecondlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#SecondlySamsSynchronyForm").fadeOut(500);
                                    }
                                    });
                            </script>
						</div>
                        <br />
                        <div class="row">
                            <div class="col-md-4">
								<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.ThirdlyPaymentMethod %>" />
							</div>
                            <div class="col-md-8">
                                <asp:DropDownList CssClass="form-control" ID="ddlThirdlyPaymentMethod" runat="server" Enabled="false" ClientIDMode="Static" />								    
                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">              
                                </div>
                                <div class="col-md-4 positionAdjustment">
                                    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PaymentBalance %>" />
                                </div>
                                <div class="col-md-4">
                                    <asp:TextBox CssClass="form-control" runat="server" ID="txtThirdlyPaymentBalance" onchange="checkNumDigit(this,0)"/>
                                    <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.PaymentBalanceErrMessage %>" CssClass="pm-errormsg" />
                                </div>
                            </div>
                            <div class="row">
                                 <div id="ThirdlyCreditCardForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.AuthorizeNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtThirdlyAuthorizeNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="ThirdlyPurchaseOrderNumberForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.PurchaseOrderNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtThirdlyPurchaseOrderNumber" />                                    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div id="ThirdlySamsSynchronyForm">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SamsSynchronyNumber %>" />
                                    </div>
                                    <div class="col-md-4">                                
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtThirdlySamsSynchronyNumber" />                                    
                                    </div>
                                </div>
                            </div>
							<div id="ThirdlySynchronyForm">
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumber %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtThirdlySynchronyAccountNumber" onchange="checkNumDigit(this,16)"/>
                                        <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumberErrMessage %>" CssClass="pm-errormsg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKey %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:TextBox CssClass="form-control" runat="server" ID="txtThirdlySynchronyApplicationKey" onchange="checkNumDigit(this,8)"/>
                                        <asp:Label runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKeyErrMessage %>" CssClass="pm-errormsg" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountInfo %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlThirdlySynchronyAccountInfo" runat="server" Enabled="false" ClientIDMode="Static" />	
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountTerm %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlThirdlySynchronyTerm" runat="server" Enabled="false" ClientIDMode="Static" />	
                                    </div>
                                </div>
                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyPrimaryID %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlThirdlySynchronyPrimaryID" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyIssuedState %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlThirdlyIssuedState" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">              
                                    </div>
                                    <div class="col-md-4 positionAdjustment">
                                        <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyExpiretionDate %>" />
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList CssClass="form-control" ID="ddlThirdlyExpiretionMonth" runat="server" Enabled="false" ClientIDMode="Static" />
                                        <asp:DropDownList CssClass="form-control" ID="ddlThirdlyExpiretionYear" runat="server" Enabled="false" ClientIDMode="Static" />
                                    </div>
                                </div>
                            </div>

                            <script type="text/javascript">
                                if ($("#ddlThirdlyPaymentMethod").find('option:selected').text() == 'Synchrony') {
                                        $("#ThirdlySynchronyForm").fadeIn(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                } else if ($("#ddlThirdlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'CreditCard') {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeIn(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                } else if ($("#ddlThirdlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'PurchaseOrder') {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeIn(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                } else if ($("#ddlThirdlyPaymentMethod").find('option:selected').text().replace(/\s/g, '').replace('�', '') == 'SamsClubSynchrony') {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeIn(500);
                                } else {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                }
                                $("#ddlThirdlyPaymentMethod").change(function () {
                                    if ($("#ddlThirdlyPaymentMethod").find('option:selected').text() == 'Synchrony') {
                                        $("#ThirdlySynchronyForm").fadeIn(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlThirdlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'CreditCard') {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeIn(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlThirdlyPaymentMethod").find('option:selected').text().replace(/\s/g, '') == 'PurchaseOrder') {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeIn(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                    } else if ($("#ddlThirdlyPaymentMethod").find('option:selected').text().replace(/\s/g, '').replace('�', '') == 'SamsClubSynchrony') {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeIn(500);
                                    } else {
                                        $("#ThirdlySynchronyForm").fadeOut(500);
                                        $("#ThirdlyCreditCardForm").fadeOut(500);
                                        $("#ThirdlyPurchaseOrderNumberForm").fadeOut(500);
                                        $("#ThirdlySamsSynchronyForm").fadeOut(500);
                                    }
                                });
                            </script>                               
							
                            <br>
                            <div class="row">
								<div class="col-md-4">
						        </div>
                                <div class="col-md-8">
                                    <asp:Button ID="btnSubmitPaymentForm" CssClass="btn btn-primary btnSubmitPaymentForm" runat="server" OnClick="btnUpdatePaymentForm_Click" Text="<%$Tokens:StringResource, admin.common.UpdatePaymentDetail %>" />
                                </div>
							</div>
                            <!----- End of Custom payment ----->
                            <hr>
							
                            <div class="row" ID="AreaAuthorizeNumber" visible="false" runat="server">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.AuthorizeNumber %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litAuthorizeNumber" />
								</div>
							</div>

                            <div ID="AreaSynchrony" visible="false" runat="server">
                                <div class="row">
                                    <div class="col-md-4">
									    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountNumber %>" />
								    </div>
								    <div class="col-md-8">
									    <asp:Literal runat="server" ID="litSynchronyAccountNumber" />
								    </div>
							    </div>

                                <div class="row">
                                    <div class="col-md-4">
									    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyApplicationKey %>" />
								    </div>
								    <div class="col-md-8">
									    <asp:Literal runat="server" ID="litSynchronyApplicationKey" />
								    </div>
							    </div>
                                <div class="row">
                                    <div class="col-md-4">
									    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyAccountInfo %>" />
								    </div>
								    <div class="col-md-8">
									    <asp:Literal runat="server" ID="litSynchronyAccountInfo" />
								    </div>
							    </div>
                                <div class="row">
                                    <div class="col-md-4">
									    <asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.SynchronyTerm %>" />
								    </div>
								    <div class="col-md-8">
									    <asp:Literal runat="server" ID="litSynchronyTerm" />
								    </div>
							    </div>
                            </div>

							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.default.PaymentGateway %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litPaymentGateway" />
								</div>
							</div>-->
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.AVSResult %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litAVSResult" />
								</div>
							</div>-->
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.order.TransactionState %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litTransactionState" />
									<asp:Button ID="btnCapture" OnClientClick="return confirm('Are you sure you want to capture the order?');" Enabled="false" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderframe.Capture %>" runat="server" OnClick="btnCapture_Click" />
								</div>
							</div>-->
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.AuthorizedOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litAuthorizedOn" />
								</div>
							</div>-->
							<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ReceiptEmailSentOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litReceiptSentOn" /><br>
									<asp:Button ID="btnReceiptEmail" Enabled="false" CssClass="btn btn-primary" runat="server" OnClick="btnReceiptEmail_Click" /><br />
								</div>
							</div>
                            <hr>
                            <div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.GenerateRecipt %>" />
								</div>
								<div class="col-md-8">
                                    <asp:Button ID="Button2" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderdetails.RegenerateReceipt %>" runat="server" OnClick="btnRegenerateReceipt_Click" />
								    <asp:Button ID="Button3" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderdetails.RegenerateDriverInstruction %>" runat="server" OnClick="btnRegenerateDriverInstruction_Click" />
                                </div>
							</div>
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CapturedOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litCapturedOn" />
								</div>
							</div>-->
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RefundedOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litRefundedOn" />
								</div>
							</div>-->
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.VoidedOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litVoidedOn" />
								</div>
							</div>-->
							<!--<div class="row">
								<div class="col-md-4">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.FraudedOn %>" />
								</div>
								<div class="col-md-8">
									<asp:Literal runat="server" ID="litFraudedOn" />
								</div>
							</div>-->
							<div id="divCCInfo" visible="false" runat="server">
								<div class="row">
									<div class="col-md-4">
										<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardType %>" />
									</div>
									<div class="col-md-8">
										<asp:Literal runat="server" ID="litCCType" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardNumber %>" />
									</div>
									<div class="col-md-8">
										<asp:Literal runat="server" ID="litCCNumber" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.LastFour %>" />
									</div>
									<div class="col-md-8">
										<asp:Literal runat="server" ID="litCCLastFour" />
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardExpiration %>" />
									</div>
									<div class="col-md-8">
										<asp:Literal runat="server" ID="litCCExpirationDate" />
									</div>
								</div>
								<div id="divCCIssueInfo" runat="server" visible="false">
									<div class="row">
										<div class="col-md-4">
											<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardIssueNumber %>" />
										</div>
										<div class="col-md-8">
											<asp:Literal runat="server" ID="litCCIssueNumber" />
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardStartDate %>" />
										</div>
										<div class="col-md-8">
											<asp:Literal runat="server" ID="litCCStartDate" />
										</div>
									</div>
								</div>
							</div>
							<div id="divPayPalInfo" runat="server" visible="false">
								<div class="form-group">
									<asp:Button ID="btnPayPalReauth" CssClass="btn btn-primary" runat="server" Text="<%$Tokens:StringResource, admin.orderframe.Reauthorize %>" OnClick="btnPayPalReauth_Click" />
								</div>
							</div>
						
					</div>

					<div id="divPromoInfo" class="white-ui-box">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.PromotionUsage %>" />
						</div>
						<div>
							<div>
								<div class="table-row-inner">
									<asp:GridView ID="grdPromotions" CssClass="table table-detail" ShowHeader="true" PageIndex="0" AllowPaging="true" PageSize="5"
										runat="server" GridLines="None" CellSpacing="-1" OnPageIndexChanging="grdPromotions_OnPageIndexChanging" AutoGenerateColumns="false">
										<Columns>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.PromotionCode %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("Code") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.ShippingDiscount %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("ShippingDiscount") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.LineItemDiscount %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("LineItemDiscount") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.OrderLevelDiscount %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("OrderDiscount") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.GiftWPurchase %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:CheckBox runat="server" Enabled="false" Checked='<%# (bool)Eval("GiftWithPurchase") %>' />
												</ItemTemplate>
											</asp:TemplateField>
											<asp:TemplateField>
												<HeaderTemplate>
													<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.TotalDiscount %>" />
												</HeaderTemplate>
												<ItemTemplate>
													<asp:Label runat="server" Text='<%# Eval("TotalDiscount") %>' />
												</ItemTemplate>
											</asp:TemplateField>
										</Columns>
									</asp:GridView>
								</div>
							</div>
						</div>
					</div>

					<div id="divDebugInfo" class="white-ui-box">
						<div class="white-box-heading">
							<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.DebugInfo %>" />
						</div>
						<div id="divTransactionInfoWrap" runat="server" visible="false">
							<div>
								<a href="javascript:void(0);" class="btn btn-primary" onclick="toggleTransactions();">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.TransactionCommand %>"></asp:Literal></a>
							</div>
							<div id="divTransactionInfo" style="display: none;">
								<div id="divTransactionCommand" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.TransactionCommand %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtTransactionCommand" runat="server" />
								</div>
								<div id="divAuthorizationResult" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.AuthorizationResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtAuthorizationResult" runat="server" />
								</div>
								<div id="divAuthorizationCode" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.AuthorizationCode %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtAuthorizationCode" runat="server" />
								</div>
								<div id="divCaptureCommand" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CaptureTXCommand %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtCaptureCommand" runat="server" />
								</div>
								<div id="divCaptureResult" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CaptureTXResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtCaptureResult" runat="server" />
								</div>
								<div id="divVoidCommand" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.VoidTXCommand %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtVoidCommand" runat="server" />
								</div>
								<div id="divVoidResult" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.VoidTXResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtVoidResult" runat="server" />
								</div>
								<div id="divRefundCommand" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RefundTXCommand %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtRefundCommand" runat="server" />
								</div>
								<div id="divRefundResult" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RefundTXResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtRefundResult" runat="server" />
								</div>
								<div id="divCardinalLookup" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardinalLookupResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtCardinalLookup" runat="server" />
								</div>
								<div id="divCardinalAuthenticate" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.CardinalAuthenticateResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtCardinalAuthenticate" runat="server" />
								</div>
								<div id="div3dSecure" runat="server" visible="false">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.3DSecureLookupResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="4" ID="txtThreedSecure" runat="server" />
								</div>
							</div>
						</div>

						<div id="divXmlInfoWrap" runat="server" visible="false">
							<div>
								<a href="javascript:void(0);" class="btn btn-primary" onclick="toggleXml();">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.title.orderXML %>"></asp:Literal></a>
							</div>
							<div id="divXmlInfo" style="display: none;">
								<div>
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="20" ID="litOrderXml" runat="server" />
								</div>
							</div>
						</div>

						<div id="divShippingInfoWrap" runat="server">
							<div>
								<a href="javascript:void(0);" class="btn btn-primary" onclick="toggleShipping();">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RTShippingInfo %>"></asp:Literal></a>
							</div>
							<div id="divShippingInfo" style="display: none;">
								<div>
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.RTShippingRequest %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="10" ID="txtRTShippingRequest" runat="server" />
								</div>
								<div>
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderdetails.RTShippingResponse %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="10" ID="txtRTShippingResponse" runat="server" />
								</div>
							</div>
						</div>

						<div id="divRecurringInfoWrap" runat="server" visible="false">
							<div>
								<a href="javascript:void(0);" class="btn btn-primary" onclick="toggleRecurring();">
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.title.recurringgatewaydetails %>"></asp:Literal></a>
							</div>
							<div id="divRecurringInfo" style="display: none;">
								<div>
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.GatewayAutoBillSubscriptionID %>" />
									<asp:Literal runat="server" ID="litRecurringSubscriptionId" Visible="false" />
									<asp:HyperLink ID="lnkRecurringStatus" runat="server" Visible="false" Target="_blank" />
								</div>
								<div>
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RecurringBillingSubscriptionCreateCommand %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="10" ID="txtRecurringCommand" runat="server" />
								</div>
								<div>
									<asp:Literal runat="server" Text="<%$Tokens:StringResource, admin.orderframe.RecurringBillingSubscriptionCreateResult %>" />
									<asp:TextBox TextMode="MultiLine" CssClass="form-control disabled" Rows="10" ID="txtRecurringResult" runat="server" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="item-action-bar">
				<asp:HyperLink runat="server" ID="btnClose" CssClass="btn btn-default" NavigateUrl="<%# ReturnUrlTracker.GetHyperlinkReturnUrl() %>" Text="<%$Tokens:StringResource, admin.common.close %>" />
				<asp:Button ID="btnViewReceiptBottom" CssClass="btn btn-primary" Text="<%$Tokens:StringResource, admin.orderframe.OpenNewReceiptWindowHere %>" runat="server" OnClick="btnViewReceipt_Click" />
			</div>
		</asp:Panel>
	</div>

	<script type="text/javascript">
		function toggleTransactions() {
			$("#divTransactionInfo").toggle();
		}
		function toggleXml() {
			$("#divXmlInfo").toggle();
		}
		function toggleShipping() {
			$("#divShippingInfo").toggle();
		}
		function toggleRecurring() {
			$("#divRecurringInfo").toggle();
		}
		function toggleDistributor() {
			$("#divDistributorInfo").toggle();
        }

        /**
         * Added by JBS 11/18/2017
         * @param obj
         * @param targetlength
         */
        function checkNumDigit(obj, targetlength) {
            if (targetlength == 0) {
                if (obj.value.trim() != '' && isNaN(obj.value)) {
                    $(obj).next(".pm-errormsg").show();
                } else {
                    $(obj).next(".pm-errormsg").hide();
                }
            } else {
                if (obj.value.trim() != '' && ((obj.value.length != targetlength) || (isNaN(obj.value)))) {
                    $(obj).next(".pm-errormsg").show();
                } else {
                    $(obj).next(".pm-errormsg").hide();
                }
            }

            handleUpdateButton();
        }

        function handleUpdateButton() {
            $(".btnSubmitPaymentForm").attr("disabled", false);
            $('.pm-errormsg').each(function (i, val) {
                if (val.style.display == "inline") {
                    $(".btnSubmitPaymentForm").attr("disabled", true);
                }
            });
        }
	</script>
</asp:Content>
