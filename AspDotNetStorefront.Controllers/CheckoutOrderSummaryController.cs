// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Web.Mvc;
using AspDotNetStorefront.Caching.ObjectCaching;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Checkout;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutOrderSummaryController : Controller
	{
		readonly ICachedShoppingCartProvider CachedShoppingCartProvider;
        readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;
        readonly IPaymentMethodInfoProvider PaymentMethodInfoProvider;

        public CheckoutOrderSummaryController(ICachedShoppingCartProvider cachedShoppingCartProvider,
            IPersistedCheckoutContextProvider persistedCheckoutContextProvider,
            IPaymentMethodInfoProvider paymentMethodInfoProvider)
		{
			CachedShoppingCartProvider = cachedShoppingCartProvider;
            PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
            PaymentMethodInfoProvider = paymentMethodInfoProvider;
        }

		//[ChildActionOnly]
		public ActionResult OrderSummary()
		{
			return PartialView(ViewNames.OrderSummaryPartial, BuildOrderSummaryViewModel());
		}

        OrderSummaryViewModel BuildOrderSummaryViewModel()
        {
            var customer = HttpContext.GetCustomer();
            var cart = CachedShoppingCartProvider.Get(customer, CartTypeEnum.ShoppingCart, AppLogic.StoreID());

            var subtotal = cart.SubTotal(
                includeDiscount: false,
                onlyIncludeTaxableItems: false,
                includeDownloadItems: true,
                includeFreeShippingItems: true,
                includeSystemItems: true,
                useCustomerCurrencySetting: true);

            var taxTotal = cart.TaxTotal();

            var shippingTotal = cart.ShippingTotal(
                includeDiscount: true,
                includeTax: true);

            // Calculate lineitem and order level coupon and promotional discounts.
            var discountTotal = cart.Total(
                includeDiscount: true,
                roundBeforeTotaling: false) - taxTotal - shippingTotal - subtotal;

            var orderTotal = cart.Total(includeDiscount: true);

            // Calculate the discount for gift card.
            var giftCardDiscount = Decimal.Zero;
            if (cart.Coupon.CouponType == CouponTypeEnum.GiftCard)
                giftCardDiscount = decimal.Round(Currency.Convert(cart.Coupon.DiscountAmount, Localization.StoreCurrency(), customer.CurrencySetting), 2, MidpointRounding.AwayFromZero);

            // Ensure gift card discounts do not create a negative order total.
            orderTotal -= giftCardDiscount;
            if (orderTotal < 0)
                orderTotal = 0;

            var vatEnabled = AppLogic.AppConfigBool("VAT.Enabled");
            var shippingVatCaption = string.Empty;
            if (vatEnabled)
            {
                shippingVatCaption = AppLogic.GetString("setvatsetting.aspx.7");
                if (cart.VatIsInclusive)
                    shippingVatCaption = AppLogic.GetString("setvatsetting.aspx.6");
            }

            var currencySetting = customer.CurrencySetting;

            //Custom for primary payment balance
            var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);
            Decimal PrmaryPaymentBalanceTotal = 0; 
            Decimal SecondlyPaymentBalanceTotal = 0;
            Decimal ThirdlyPaymentBalanceTotal = 0;
            Decimal RemainingBalance = 0;

            var selectedPaymentMethod = PaymentMethodInfoProvider.GetPaymentMethodInfo(
                    paymentMethod: customer.RequestedPaymentMethod,
                    gateway: AppLogic.ActivePaymentGatewayCleaned());

            if (selectedPaymentMethod != null) { 
                if (selectedPaymentMethod.Name == "CASH" && checkoutContext.Cash.PrimaryPaymentBalance != null) {
                    PrmaryPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.Cash.PrimaryPaymentBalance);
                }
                else if (selectedPaymentMethod.Name == "COD" && checkoutContext.COD.PrimaryPaymentBalance != null)
                {
                    PrmaryPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.COD.PrimaryPaymentBalance);
                }
                else if (selectedPaymentMethod.Name == "CHECKBYMAIL" && checkoutContext.CheckByMail.PrimaryPaymentBalance != null)
                {
                    PrmaryPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.CheckByMail.PrimaryPaymentBalance);
                }
                else if (selectedPaymentMethod.Name == "SYNCHRONY" && checkoutContext.Synchrony.PrimaryPaymentBalance != null)
                {
                    PrmaryPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.Synchrony.PrimaryPaymentBalance);
                }
                else if (selectedPaymentMethod.Name == "CREDITCARDWITHAUTHNUMBER" && checkoutContext.CCWA.PrimaryPaymentBalance != null)
                {
                    PrmaryPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.CCWA.PrimaryPaymentBalance);
                }
                else if (selectedPaymentMethod.Name == "CUSTOM")
                {
                    if (checkoutContext.CustomPayment.PrimaryPaymentBalance != null) { 
                        PrmaryPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.CustomPayment.PrimaryPaymentBalance);
                    }
                    if (checkoutContext.CustomPayment.SecondlyPaymentBalance != null)
                    {
                        SecondlyPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.CustomPayment.SecondlyPaymentBalance);
                    }
                    if (checkoutContext.CustomPayment.ThirdlyPaymentBalance != null)
                    {
                        ThirdlyPaymentBalanceTotal = Convert.ToDecimal(checkoutContext.CustomPayment.ThirdlyPaymentBalance);
                    }

                }

                RemainingBalance = orderTotal - PrmaryPaymentBalanceTotal - SecondlyPaymentBalanceTotal - ThirdlyPaymentBalanceTotal;
            }

            return new OrderSummaryViewModel
			{
				SubTotal = Localization.CurrencyStringForDisplayWithExchangeRate(subtotal, currencySetting),
				DiscountTotal = Localization.CurrencyStringForDisplayWithExchangeRate(discountTotal, currencySetting),
				ShippingTotal = Localization.CurrencyStringForDisplayWithExchangeRate(shippingTotal, currencySetting),
				ShippingVatCaption = string.Format("({0})", shippingVatCaption),
				TaxTotal = Localization.CurrencyStringForDisplayWithExchangeRate(taxTotal, currencySetting),
				HasGiftCardDiscountTotal = giftCardDiscount != 0,
				GiftCardDiscountTotal = Localization.CurrencyStringForDisplayWithExchangeRate(giftCardDiscount, currencySetting),
				Total = Localization.CurrencyStringForDisplayWithExchangeRate(orderTotal, currencySetting),
				HasDiscount = discountTotal != 0,
				ShowVatLabels = vatEnabled,
				ShowTax = !cart.VatIsInclusive,
                PrimaryPaymentBalance = Localization.CurrencyStringForDisplayWithExchangeRate(PrmaryPaymentBalanceTotal, currencySetting),
                SecondlyPaymentBalance = Localization.CurrencyStringForDisplayWithExchangeRate(SecondlyPaymentBalanceTotal, currencySetting),
                ThirdlyPaymentBalance = Localization.CurrencyStringForDisplayWithExchangeRate(ThirdlyPaymentBalanceTotal, currencySetting),
                RemainingBalance = Localization.CurrencyStringForDisplayWithExchangeRate(RemainingBalance, currencySetting),
                IsNoTax = customer.IsNoTax
            };
		}
	}
}
