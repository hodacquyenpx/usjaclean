// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Threading;
using AspDotNetStorefrontControls;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefrontAdmin
{
	public partial class WarrantyEditor : AspDotNetStorefront.Admin.AdminPageBase
	{
		readonly bool UseHtmlEditor;

		int RecordId
		{
			get { return (int?)ViewState["RecordId"] ?? CommonLogic.QueryStringNativeInt("WarrantyId"); }
			set { ViewState["RecordId"] = value; }
		}

		public WarrantyEditor()
		{
			UseHtmlEditor = !AppLogic.AppConfigBool("TurnOffHtmlEditorInAdminSite");
		}

		protected void Page_Load(object sender, EventArgs e)
		{

			if(Page.IsPostBack)
				return;

            LoadWarrantyItem();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			DataBind();
			btnClose.DataBind();
			btnCloseTop.DataBind();
		}

		protected void LocaleSelector_SelectedLocaleChanged(object sender, EventArgs e)
		{
            LoadWarrantyItem();
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				Page.Validate();
				if(!Page.IsValid)
					return;

				SaveWarrantyItem();
				ctlAlertMessage.PushAlertMessage("admin.orderdetails.UpdateSuccessful".StringResource(), AlertMessage.AlertType.Success);
			}
			catch(Exception exception)
			{
				ctlAlertMessage.PushAlertMessage(exception.Message, AlertMessage.AlertType.Error);
			}
		}

		protected void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			try
			{
				Page.Validate();
				if(!Page.IsValid)
					return;

				SaveWarrantyItem();
				Response.Redirect(ReturnUrlTracker.GetReturnUrl());
			}
			catch(ThreadAbortException)
			{
				throw;
			}
			catch(Exception exception)
			{
				ctlAlertMessage.PushAlertMessage(exception.Message, AlertMessage.AlertType.Error);
			}
		}

		void LoadWarrantyItem()
		{
			if(RecordId == 0)
			{
				Title = HeaderText.Text = AppLogic.GetString("admin.editwarranty.AddingWarranty", ThisCustomer.LocaleSetting);
			}
			else
			{
				Title = HeaderText.Text = AppLogic.GetString("admin.editwarranty.EditingWarranty", ThisCustomer.LocaleSetting);

				using(var connection = new SqlConnection(DB.GetDBConn()))
				using(var command = new SqlCommand())
				{
					command.Connection = connection;
					command.CommandText = "SELECT * FROM Warranty WITH (NOLOCK) WHERE WarrantyID = @WarrantyID";
					command.Parameters.AddRange(new[]
						{
							new SqlParameter("@WarrantyID", RecordId.ToString()) 
						});

					connection.Open();
					using(var reader = command.ExecuteReader())
						if(reader.Read())
						{
							var selectedLocale = LocaleSelector.GetSelectedLocale().Name;

							litWarrantyId.Text = reader.FieldInt("WarrantyID").ToString();
							txtWarrantyName.Text = reader.Field("WarrantyName");
                            txtWarrantyPrice.Text = reader.FieldDecimal("WarrantyPrice").ToString();
                            txtShowOrder.Text = reader.FieldInt("ShowOrder").ToString();

                            cbxPublished.Checked = reader.FieldBool("Published");
						}
				}
			}

			StoresMapping.ObjectID = RecordId;
			StoresMapping.DataBind();
			divStoreMapping.Visible = StoresMapping.StoreCount > 1;
		}

		void SaveWarrantyItem()
		{
			var editing = RecordId != 0;
			var selectedLocale = LocaleSelector.GetSelectedLocale().Name;

			var warrantyname = editing
				? AppLogic.FormLocaleXml("Warranty", txtWarrantyName.Text.Trim(), selectedLocale, "Warranty", RecordId)
				: AppLogic.FormLocaleXml(txtWarrantyName.Text.Trim(), selectedLocale);

            var warrantyprice = editing
                ? AppLogic.FormLocaleXml("Warranty", txtWarrantyPrice.Text.Trim(), selectedLocale, "Warranty", RecordId)
                : AppLogic.FormLocaleXml(txtWarrantyPrice.Text.Trim(), selectedLocale);

            var showorder = editing
                ? AppLogic.FormLocaleXml("Warranty", txtShowOrder.Text.Trim(), selectedLocale, "Warranty", RecordId)
                : AppLogic.FormLocaleXml(txtShowOrder.Text.Trim(), selectedLocale);



            var parameters = new[]
				{
					new SqlParameter("@warrantyId", RecordId),
					new SqlParameter("@warrantyname", warrantyname),
                    new SqlParameter("@warrantyprice", warrantyprice),
                    new SqlParameter("@showorder", showorder),
                    new SqlParameter("@published", cbxPublished.Checked)
				};

			var query = editing
				? "UPDATE Warranty SET WarrantyName = @warrantyname, Published = @published, UpdatedOn = getdate(), WarrantyPrice = @warrantyprice, ShowOrder = @showorder WHERE WarrantyID = @warrantyID"
                : "INSERT Warranty (WarrantyName, Published, CreatedOn, UpdatedOn, Deleted, WarrantyPrice, ShowOrder) VALUES (@warrantyname, @published, getdate(), getdate(),0, @warrantyprice, @showorder); select cast(SCOPE_IDENTITY() as int) N;";

			var identity = DB.GetSqlN(query, parameters);
			if(!editing)
				RecordId = identity;

			StoresMapping.ObjectID = RecordId;
			StoresMapping.Save();
		}
	}
}
