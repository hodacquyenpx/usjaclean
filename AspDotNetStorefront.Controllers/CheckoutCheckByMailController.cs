// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using System.Linq;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutCheckByMailController : Controller
	{
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;

		public CheckoutCheckByMailController(
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider)
		{
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
		}

		[PageTypeFilter(PageTypes.Checkout)]
		[HttpGet, ImportModelStateFromTempData]
		public ActionResult CheckByMail()
		{
			var customer = HttpContext.GetCustomer();

            SelectList listsecondlypayment = new SelectList(AppLogic.AppConfig("Checkout.secondlyPaymentListForCHKByMail").Split(',').ToList());


            if (!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMCheckByMail, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCheckByMailViewModel(checkoutContext.CheckByMail);

            model.ListsecondlyPayment = listsecondlypayment;

            return View(model);
		}

		[HttpPost, ExportModelStateToTempData]
		public ActionResult CheckByMail(CheckByMailViewModel model)
		{
			if(!ModelState.IsValid)
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: checkoutContext.CCWA,  
                synchrony: checkoutContext.Synchrony,
                cash: checkoutContext.Cash,
                cOD: checkoutContext.COD,
                checkByMail: new CheckByMailDetails(model.PrimaryPaymentBalance, model.SelectedsecondlyPayment),
                customPayment: checkoutContext.CustomPayment,
                braintree: checkoutContext.Braintree,
				amazonPayments: null,
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: null,
				offsiteRequiresShippingAddressId: null,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMCheckByMail);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}

		public ActionResult CheckByMailDetail()
		{
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCheckByMailViewModel(checkoutContext.CheckByMail);

			return PartialView(ViewNames.CheckByMailDetailPartial, model);
		}

        CheckByMailViewModel BuildCheckByMailViewModel(CheckByMailDetails checkByMail)
		{
            if (checkByMail != null)
            {
                return new CheckByMailViewModel
                {
                    PrimaryPaymentBalance = checkByMail.PrimaryPaymentBalance == null
                        ? string.Empty
                        : checkByMail.PrimaryPaymentBalance,
                    SelectedsecondlyPayment = checkByMail.SelectedsecondlyPayment == null
                        ? string.Empty
                        : checkByMail.SelectedsecondlyPayment
                };
            }
            else
            {
                return new CheckByMailViewModel
                {
                    PrimaryPaymentBalance = string.Empty,
                    SelectedsecondlyPayment = string.Empty,
                };
            }
            }
	}
}
