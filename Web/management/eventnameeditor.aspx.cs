// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Data.SqlClient;
using System.Threading;
using AspDotNetStorefrontControls;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefrontAdmin
{
	public partial class EventNameEditor : AspDotNetStorefront.Admin.AdminPageBase
	{
		readonly bool UseHtmlEditor;

		int RecordId
		{
			get { return (int?)ViewState["RecordId"] ?? CommonLogic.QueryStringNativeInt("EventId"); }
			set { ViewState["RecordId"] = value; }
		}

		public EventNameEditor()
		{
			UseHtmlEditor = !AppLogic.AppConfigBool("TurnOffHtmlEditorInAdminSite");
		}

		protected void Page_Load(object sender, EventArgs e)
		{

			if(Page.IsPostBack)
				return;

			LoadEventNameItem();
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);
			DataBind();
			btnClose.DataBind();
			btnCloseTop.DataBind();
		}

		protected void LocaleSelector_SelectedLocaleChanged(object sender, EventArgs e)
		{
			LoadEventNameItem();
		}

		protected void btnSubmit_Click(object sender, EventArgs e)
		{
			try
			{
				Page.Validate();
				if(!Page.IsValid)
					return;

				SaveEventNameItem();
				ctlAlertMessage.PushAlertMessage("admin.orderdetails.UpdateSuccessful".StringResource(), AlertMessage.AlertType.Success);
			}
			catch(Exception exception)
			{
				ctlAlertMessage.PushAlertMessage(exception.Message, AlertMessage.AlertType.Error);
			}
		}

		protected void btnSaveAndClose_Click(object sender, EventArgs e)
		{
			try
			{
				Page.Validate();
				if(!Page.IsValid)
					return;

				SaveEventNameItem();
				Response.Redirect(ReturnUrlTracker.GetReturnUrl());
			}
			catch(ThreadAbortException)
			{
				throw;
			}
			catch(Exception exception)
			{
				ctlAlertMessage.PushAlertMessage(exception.Message, AlertMessage.AlertType.Error);
			}
		}

		void LoadEventNameItem()
		{
			if(RecordId == 0)
			{
				Title = HeaderText.Text = AppLogic.GetString("admin.editeventname.AddingEventName", ThisCustomer.LocaleSetting);
			}
			else
			{
				Title = HeaderText.Text = AppLogic.GetString("admin.editeventname.EditingEventName", ThisCustomer.LocaleSetting);

				using(var connection = new SqlConnection(DB.GetDBConn()))
				using(var command = new SqlCommand())
				{
					command.Connection = connection;
					command.CommandText = "SELECT * FROM EventName WITH (NOLOCK) WHERE EventID = @EventID";
					command.Parameters.AddRange(new[]
						{
							new SqlParameter("@EventID", RecordId.ToString()) 
						});

					connection.Open();
					using(var reader = command.ExecuteReader())
						if(reader.Read())
						{
							var selectedLocale = LocaleSelector.GetSelectedLocale().Name;

							litEventId.Text = reader.FieldInt("EventID").ToString();
							txtEventName.Text = reader.FieldByLocale("EventName", selectedLocale);
                            txtEventNameTaxRate.Text = reader.FieldDecimal("TaxRate").ToString();
                            cbxPublished.Checked = reader.FieldBool("Published");
						}
				}
			}

			StoresMapping.ObjectID = RecordId;
			StoresMapping.DataBind();
			divStoreMapping.Visible = StoresMapping.StoreCount > 1;
		}

		void SaveEventNameItem()
		{
			var editing = RecordId != 0;
			var selectedLocale = LocaleSelector.GetSelectedLocale().Name;

			var headline = editing
				? AppLogic.FormLocaleXml("EventName", txtEventName.Text.Trim(), selectedLocale, "EventName", RecordId)
				: AppLogic.FormLocaleXml(txtEventName.Text.Trim(), selectedLocale);

            var headline2 = editing
                ? AppLogic.FormLocaleXml("TaxRate", txtEventNameTaxRate.Text.Trim(), selectedLocale, "TaxRate", RecordId)
                : AppLogic.FormLocaleXml(txtEventNameTaxRate.Text.Trim(), selectedLocale);


            var parameters = new[]
				{
					new SqlParameter("@eventId", RecordId),
					new SqlParameter("@eventname", headline),
					new SqlParameter("@published", cbxPublished.Checked),
                    new SqlParameter("@taxrate", headline2),
                };

			var query = editing
				? "UPDATE EventName SET EventName = @eventname, Published = @published, UpdatedOn = getdate(), TaxRate = @taxrate WHERE EventID = @eventID"
				: "INSERT EventName (EventName, Published, CreatedOn, UpdatedOn, Deleted, TaxRate) VALUES (@eventname, @published, getdate(), getdate(),0, @taxrate); select cast(SCOPE_IDENTITY() as int) N;";

			var identity = DB.GetSqlN(query, parameters);
			if(!editing)
				RecordId = identity;

			StoresMapping.ObjectID = RecordId;
			StoresMapping.Save();
		}
	}
}
