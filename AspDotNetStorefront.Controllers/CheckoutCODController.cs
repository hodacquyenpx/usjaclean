// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using System.Linq;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutCODController : Controller
	{
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;

		public CheckoutCODController(
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider)
		{
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
		}

		[PageTypeFilter(PageTypes.Checkout)]
		[HttpGet, ImportModelStateFromTempData]
		public ActionResult COD()
		{
			var customer = HttpContext.GetCustomer();

            SelectList listsecondlypayment = new SelectList(AppLogic.AppConfig("Checkout.secondlyPaymentListForCOD").Split(',').ToList());


            if (!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMCOD, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCODViewModel(checkoutContext.COD);

            model.ListsecondlyPayment = listsecondlypayment;

            return View(model);
		}

		[HttpPost, ExportModelStateToTempData]
		public ActionResult COD(CODViewModel model)
		{
			if(!ModelState.IsValid)
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: checkoutContext.CCWA,   
                synchrony: checkoutContext.Synchrony,
                cash: checkoutContext.Cash,
                cOD: new CODDetails(model.PrimaryPaymentBalance, model.SelectedsecondlyPayment),
                checkByMail: checkoutContext.CheckByMail,
                customPayment: checkoutContext.CustomPayment,
                braintree: checkoutContext.Braintree,
				amazonPayments: null,
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: null,
				offsiteRequiresShippingAddressId: null,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMCOD);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}

		public ActionResult CODDetail()
		{
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCODViewModel(checkoutContext.COD);

			return PartialView(ViewNames.CODDetailPartial, model);
		}

		CODViewModel BuildCODViewModel(CODDetails cOD)
		{
            if (cOD != null)
            {
                return new CODViewModel
                {
                    PrimaryPaymentBalance = cOD.PrimaryPaymentBalance == null
                        ? string.Empty
                        : cOD.PrimaryPaymentBalance,
                    SelectedsecondlyPayment = cOD.SelectedsecondlyPayment == null
                        ? string.Empty
                        : cOD.SelectedsecondlyPayment
                };
            }
            else
            {
                return new CODViewModel
                {
                    PrimaryPaymentBalance = string.Empty,
                    SelectedsecondlyPayment = string.Empty,
                };
            }
            }
	}
}
