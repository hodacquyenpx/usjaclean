// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using AspDotNetStorefront.Validation.DataAttribute;
using AspDotNetStorefrontCore;

namespace AspDotNetStorefront.Models
{
    [Bind(Exclude = "ListSecondaryPayment")]
    public class CODViewModel
	{
        [Display(Name = "checkout.PrimaryPaymentBalance.label")]
        [RegularExpression(@"\d{1,20}(\.\d{1,2})?", ErrorMessage = "Invalid Price. Please use the format of XXXX.XX.")]
        public string PrimaryPaymentBalance
        { get; set; }

        [Display(Name = "checkout.SecondaryPayment.label")]
        public SelectList ListSecondaryPayment;

        public string SelectedSecondaryPayment
        { get; set; }

    }
}
