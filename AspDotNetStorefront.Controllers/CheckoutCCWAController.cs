// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Checkout;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using System.Linq;

namespace AspDotNetStorefront.Controllers
{
	[SecureAccessFilter(forceHttps: true)]
	public class CheckoutCCWAController : Controller
	{
		readonly NoticeProvider NoticeProvider;
		readonly IPaymentOptionProvider PaymentOptionProvider;
		readonly IPersistedCheckoutContextProvider PersistedCheckoutContextProvider;

		public CheckoutCCWAController(
			NoticeProvider noticeProvider,
			IPaymentOptionProvider paymentOptionProvider,
			IPersistedCheckoutContextProvider persistedCheckoutContextProvider)
		{
			NoticeProvider = noticeProvider;
			PaymentOptionProvider = paymentOptionProvider;
			PersistedCheckoutContextProvider = persistedCheckoutContextProvider;
		}

		[PageTypeFilter(PageTypes.Checkout)]
		[HttpGet, ImportModelStateFromTempData]
		public ActionResult CCWA()
		{
			var customer = HttpContext.GetCustomer();

            SelectList listsecondlypayment = new SelectList(AppLogic.AppConfig("Checkout.secondlyPaymentListForCCWA").Split(',').ToList());


            if (!PaymentOptionProvider.PaymentMethodSelectionIsValid(AppLogic.ro_PMCreditCardWithAuthNumber, customer))
			{
				NoticeProvider.PushNotice(
					message: AppLogic.GetString("checkout.paymentmethodnotallowed"),
					type: NoticeType.Failure);
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			}

			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCCWAViewModel(checkoutContext.CCWA);

            model.ListsecondlyPayment = listsecondlypayment;

            return View(model);
		}

		[HttpPost, ExportModelStateToTempData]
		public ActionResult CCWA(CCWAViewModel model)
		{
			if(!ModelState.IsValid)
				return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
			
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var updatedCheckoutContext = new PersistedCheckoutContext(
				creditCard: checkoutContext.CreditCard,
				payPalExpress: checkoutContext.PayPalExpress,
				purchaseOrder: checkoutContext.PurchaseOrder,
                cCWA: new CCWADetails(model.CCWA, model.PrimaryPaymentBalance, model.SelectedsecondlyPayment),
                synchrony: checkoutContext.Synchrony,
                cash: checkoutContext.Cash,
                cOD: checkoutContext.COD,
                checkByMail: checkoutContext.CheckByMail,
                customPayment: checkoutContext.CustomPayment,
                braintree: checkoutContext.Braintree,
				amazonPayments: null,
				termsAndConditionsAccepted: checkoutContext.TermsAndConditionsAccepted,
				over13Checked: checkoutContext.Over13Checked,
				shippingEstimateDetails: checkoutContext.ShippingEstimateDetails,
				offsiteRequiresBillingAddressId: null,
				offsiteRequiresShippingAddressId: null,
				email: checkoutContext.Email,
				selectedShippingMethodId: checkoutContext.SelectedShippingMethodId,
                isHoldOrder: checkoutContext.IsHoldOrder);

			PersistedCheckoutContextProvider.SaveCheckoutContext(customer, updatedCheckoutContext);
			customer.UpdateCustomer(requestedPaymentMethod: AppLogic.ro_PMCreditCardWithAuthNumber);

			return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);
		}

		public ActionResult CCWADetail()
		{
			var customer = HttpContext.GetCustomer();
			var checkoutContext = PersistedCheckoutContextProvider.LoadCheckoutContext(customer);

			var model = BuildCCWAViewModel(checkoutContext.CCWA);

			return PartialView(ViewNames.CCWADetailPartial, model);
		}

		CCWAViewModel BuildCCWAViewModel(CCWADetails cCWA)
		{
            if (cCWA != null)
            {
                return new CCWAViewModel
                {
                    CCWA = cCWA.Number == null
                        ? string.Empty
                        : cCWA.Number,
                    PrimaryPaymentBalance = cCWA.PrimaryPaymentBalance == null
                        ? string.Empty
                        : cCWA.PrimaryPaymentBalance,
                    SelectedsecondlyPayment = cCWA.SelectedsecondlyPayment == null
                        ? string.Empty
                        : cCWA.SelectedsecondlyPayment
                };
            }
            else
            {
                return new CCWAViewModel
                {
                    CCWA = string.Empty,
                    PrimaryPaymentBalance = string.Empty,
                    SelectedsecondlyPayment = string.Empty,
                };
            }
            }
	}
}
