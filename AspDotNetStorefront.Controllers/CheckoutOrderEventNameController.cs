// --------------------------------------------------------------------------------
// Copyright AspDotNetStorefront.com. All Rights Reserved.
// http://www.aspdotnetstorefront.com
// For details on this license please visit the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT. 
// --------------------------------------------------------------------------------
using System.Web.Mvc;
using AspDotNetStorefront.Caching.ObjectCaching;
using AspDotNetStorefront.Models;
using AspDotNetStorefrontCore;
using AspDotNetStorefront.Routing;
using AspDotNetStorefront.Filters;
using AspDotNetStorefront.Classes;
using System.Data.SqlClient;

namespace AspDotNetStorefront.Controllers
{
    [SecureAccessFilter(forceHttps: true)]
    public class CheckoutOrderEventNameController : Controller
    {
        readonly ICachedShoppingCartProvider CachedShoppingCartProvider;

        public CheckoutOrderEventNameController(
            ICachedShoppingCartProvider cachedShoppingCartProvider
         )
        {
            CachedShoppingCartProvider = cachedShoppingCartProvider;
        }


        [ChildActionOnly]
        public ActionResult OrderEventName()
        {
            var customer = HttpContext.GetCustomer();
            var cart = CachedShoppingCartProvider.Get(customer, CartTypeEnum.ShoppingCart, AppLogic.StoreID());
            var SelectListBuilder = new EventNameSelectListBuilder();

            SelectList eventnames = SelectListBuilder.BuildEventNameSelectList();

            var model = new OrderEventNameViewModel();
            model.ListOrderEventNames = new SelectList(eventnames, "Value","Text", cart.OrderEventName);
 
            return PartialView(ViewNames.OrderEventNamePartial, model);
        }

        [HttpPost]
        public ActionResult OrderEventName(OrderEventNameViewModel model)
        {
            //Get TaxRate from EventName
            decimal tmpS = 0;

            if(model.SelectedOrderEventName != null) { 
                using (var con = new SqlConnection(DB.GetDBConn()))
                {
                    con.Open();
                    using (var rs = DB.GetRS("select TaxRate from EventName where EventName = @eventName", con, new SqlParameter("@eventName", model.SelectedOrderEventName.Substring(0, model.SelectedOrderEventName.IndexOf("::")))))
                    {
                        if (rs.Read())
                        {
                            tmpS = DB.RSFieldDecimal(rs, "TaxRate");
                        }
                    }
                }
            }

            var customer = HttpContext.GetCustomer();

            if (model.SelectedOrderEventName == null)
                model.SelectedOrderEventName = string.Empty;

            if (model.SelectedOrderEventName != null)
                customer.UpdateCustomer(orderEventName: model.SelectedOrderEventName.Trim(),appliedTaxRate: tmpS, submittedAppliedTaxRate: true, isNoTax: customer.IsNoTax, submittedIsNoTax: true);

            if (customer.IsNoTax)
            {
                customer.UpdateCustomer(appliedTaxRate: 0, submittedAppliedTaxRate: true, isNoTax: true, submittedIsNoTax: true);
            }

            //customer.UpdateCustomer(orderEventName: model.SelectedOrderEventName.Trim());

            //return RedirectToAction(ActionNames.Index, ControllerNames.Checkout);

            return RedirectToAction(ActionNames.OrderSummary, ControllerNames.CheckoutOrderSummary);
        }
    }
}
